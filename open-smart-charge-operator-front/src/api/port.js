import request from '@/utils/request'
// port/addPort
export function addPort(data) {
    return request({
        url: '/port/addPort',
        method: 'post',
        data

    })
}

export function delPort(data) {
    return request({
        url: '/port/delPort',
        method: 'delete',
        params: data

    })
}

///port/editPort


export function editPort(data) {
    return request({
        url: '/port/editPort',
        method: 'post',
        data
    })
}
export function switchPort(data) {
    return request({
        url: '/port/switchPort',
        method: 'post',
        params: data
    })
}
// port/getPortList

export function getPortList(data) {
    return request({
        url: '/port/getPortList',
        method: 'get',
        params: data
    })

}export function allPort(data) {
    return request({
        url: '/port/allPort',
        method: 'post',
        params: data
    })
}