import request from '@/utils/request'
//Statistics/getUsersAndSales
export function getUsersAndSales(data) {
    return request({
        url: '/Statistics/getUsersAndSales',
        method: 'get',
        params: data

    })
}

// Statistics/getSalesDetail
export function getSalesDetail(data) {
    return request({
        url: '/Statistics/getSalesDetail',
        method: 'get',
        params: data

    })
}

///Statistics/getSalesData
export function getSalesData(data) {
    return request({
        url: '/Statistics/getSalesData',
        method: 'get',
        params: data

    })
}

///Statistics/getNewUserCount
export function getNewUserCount(data) {
    return request({
        url: '/Statistics/getNewUserCount',
        method: 'get',
        params: data

    })
}

// Statistics/getPilesInfo
export function getPilesInfo(data) {
    return request({
        url: '/Statistics/getPilesInfo',
        method: 'get',
        params: data
    })
}

// Statistics/getRechargeDetail
export function getRechargeDetail(data) {
    return request({
        url: '/Statistics/getRechargeDetail',
        method: 'get',
        params: data
    })
}

// Statistics/getDayRecharge
export function getDayRecharge(data) {
    return request({
        url: '/Statistics/getDayRecharge',
        method: 'get',
        params: data
    })
}