// registerUser
import request from '@/utils/request'

export function getRegisterUser(data) {
    return request({
        url: '/registerUser/getRegisterUser',
        method: 'get',
        params: data

    })
}

export function getChongzhiRecods(data) {
    return request({
        url: '/registerUser/getRechargeRecods',
        method: 'get',
        params: data
    })
}

export function bindMobile(data) {
    return request({
        url: '/registerUser/bindMobile',
        method: 'post',
        params: data
    })
}
export function refundOrder(data) {
    return request({
        url: '/registerUser/refundAmount',
        method: 'post',
        params: data
    })
}