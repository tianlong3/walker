import request from '@/utils/request'
// addAdvert
export function addAdvert(data) {
    return request({
        url: '/advert/addAdvert',
        method: 'post',
        data
    })
}

// advert/delAdvert
export function delAdvert(data) {
    return request({
        url: '/advert/delAdvert',
        method: 'delete',
        params: data
    })
}

// advert/getAdvertList
export function getAdvertList(data) {
    return request({
        url: '/advert/getAdvertList',
        method: 'get',
        params: data
    })
}

// advert/useAdvert
export function useAdvert(data) {
    return request({
        url: '/advert/useAdvert',
        method: 'get',
        params: data
    })
}