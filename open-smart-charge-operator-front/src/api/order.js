import request from '@/utils/request'
// GET /sysCard/getCardList
export function getOrderList(data) {
    return request({
        url: '/sysOrder/getOrderList',
        method: 'get',
        params: data

    })
}
//GET /sysOrder/getOrderList
//GET /sysOrder/getOrderData
export function getOrderData(data) {
    return request({
        url: '/sysOrder/getOrderData',
        method: 'get',
        params: data

    })
}

export function  getdd(dd) {
    return request({

        url: '/sysOrder/getOrderData',
        method: 'get',
        params: dd

    })
}
export function refundOrder(data) {
    return request({
        url: '/sysOrder/refundOrder',
        method: 'get',
        params: data

    })
}

export function ChangePort(data) {
    return request({
        url: '/sysOrder/portChange',
        method: 'get',
        params: data

    })
}
