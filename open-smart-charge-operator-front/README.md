# 前端介绍
采用vue开发，使用vue-admin-template模板,模板克隆地址及命令如下
```bash
# clone the project（克隆项目）
git clone https://github.com/PanJiaChen/vue-admin-template.git
```
# 开发工具
vscode(Visual Studio Code)

# 配置服务器地址
.env.development 
```bash
VUE_APP_BASE_API ='http://localhost:9099'
```
.env.production  
```bash
VUE_APP_BASE_API ='http://localhost:9099'
```

# 本地开发
```bash
# 进入目录
cd open-smart-charge-operator-front
# 卸载webpack  先卸载，如果安装的版本是下面的版本则跳过
npm uninstall webpack
# 安装webpack 如果安装的版本是下面的版本则跳过
npm install webpack@^4.0.0 --save-dev
# install dependency
npm install --registry=https://registry.npm.taobao.org

# develop 
npm run dev
```

This will automatically open http://localhost:9528

## Build

```bash
# build for test environment
npm run build:stage

# build for production environment（
npm run build:prod
```