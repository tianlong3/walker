package com.dkm.commons;

import com.dkm.constant.Constant;
import com.dkm.modules.sys.user.mapper.UserMapper;
import com.dkm.modules.sys.user.model.User;
import com.dkm.util.HttpContext;
import com.dkm.util.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * @ClassName BaseController
 * @Description: 基础Controller
 * @Author 杨膑
 * @Date 2019/9/5
 * @Version V1.0
 **/
public class BaseController {

    public BaseController() {
    }

    protected HttpServletRequest getHttpServletRequest() {
        return HttpContext.getRequest();
    }

    protected HttpServletResponse getHttpServletResponse() {
        return HttpContext.getResponse();
    }

    protected HttpSession getSession() {
        return ((HttpServletRequest) Objects.requireNonNull(HttpContext.getRequest())).getSession();
    }

    protected HttpSession getSession(Boolean flag) {
        return ((HttpServletRequest)Objects.requireNonNull(HttpContext.getRequest())).getSession(flag);
    }

    protected String getPara(String name) {
        return ((HttpServletRequest)Objects.requireNonNull(HttpContext.getRequest())).getParameter(name);
    }

    protected void setAttr(String name, Object value) {
        ((HttpServletRequest)Objects.requireNonNull(HttpContext.getRequest())).setAttribute(name, value);
    }

    /**
     * 获取当前登录人用户名
     * @return 用户名
     */
    protected String  getUserNm(){
        //session中获取用户名
        return (String) getSession().getAttribute(Constant.USER_SESSION_KEY);
    }

    /**
     * 获取session中用户信息
     * @return 用户信息
     */
    protected User getUserInfo(){
        return (User) getSession().getAttribute(Constant.USERINFO_SESSION_KEY);
    }

    /**
     * 获取用户ID
     * @return 用户id
     */
    protected Integer getUserId(){

        return getUserInfo().getId();
    }

}
