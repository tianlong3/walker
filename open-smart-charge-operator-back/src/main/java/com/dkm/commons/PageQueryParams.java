package com.dkm.commons;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @描述 分页查询参数
 * @创建时间 2019/10/17 22:05
 * @创建人 yangbin
 */
@Getter
@Setter
public class PageQueryParams {
    @ApiModelProperty("页数")
    private Integer pageNo;

    @ApiModelProperty("每页显示数")
    private Integer pageSize;
}
