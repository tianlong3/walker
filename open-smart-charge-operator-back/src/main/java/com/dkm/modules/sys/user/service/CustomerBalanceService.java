package com.dkm.modules.sys.user.service;

import com.dkm.modules.sys.user.model.CustomerBalance;
import com.dkm.modules.sys.user.model.User;

import java.math.BigDecimal;

/**
 * DESCRIPTION : 注册用户余额表
 *
 * @author ducf
 * @create 2019-12-20 22:08
 */
public interface CustomerBalanceService {

    /**
     * 更新注册账户余额,默认做加法,传负数表示扣减/转账,正数标识充值
     * 并记录一天记录
     *
     * @param openId   账户的微信openid
     * @param amount   变更的金额,不能为0
     * @param batchNo  批次号,充值批次号,或转账批次号
     * @param  changeType 充值类型 @see Constant.RECHARGE_RECOD_TYPE_TRANSFER_ACCOUNTS
     * @param user     操作人
     */
    void updateUserBalanceAndRecord(String openId,
                                    BigDecimal amount,
                                    String batchNo,
                                    Integer changeType,
                                    User user
    );


    /**
     * 根据openid查询账户
     *
     * @param openId
     * @return
     */
    CustomerBalance selectByOpenId(String openId);

}
