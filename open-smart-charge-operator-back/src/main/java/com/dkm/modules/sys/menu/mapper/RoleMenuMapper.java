
package com.dkm.modules.sys.menu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.sys.role.model.RoleMenuPO;
import org.springframework.stereotype.Repository;


/**
 * @ClassName RoleMenuMapper
 * @Description:
 * @Author yangbin
 * @Date 2019-09-17 23:47
 * @Version V1.0
 **/
@Repository
public interface RoleMenuMapper extends BaseMapper<RoleMenuPO> {

}

