package com.dkm.modules.sys.statistics.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.statistics.model.*;
import com.dkm.modules.sys.statistics.service.StatisticsService;
import com.dkm.modules.sys.user.mapper.UserMapper;
import com.dkm.modules.wx.order.mapper.OrderMapper;
import com.dkm.modules.wx.pile.mapper.PileMapper;
import com.dkm.util.excel.ExportExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @描述 统计接口实现类
 * @创建时间 2019/10/24 14:37
 * @创建人 yangbin
 */
@Service
public class StatisticsServiceImpl implements StatisticsService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    PileMapper pileMapper;

    @Override
    /**
     *@描述 获取首页的新增用户数和销售总额
     *@参数 []
     *@返回值 java.util.Map<java.lang.String, java.lang.Object>
     *@创建人 yangbin
     *@创建时间 2019/10/24
     */
    public Map<String, Object> getUsersAndSales() {
        return orderMapper.getUsersAndSales();
    }

    @Override
    /**
     *@描述 获取新增用户数
     *@参数 []
     *@返回值 java.util.Map<java.lang.String, java.lang.Object>
     *@创建人 yangbin
     *@创建时间 2019/10/24
     */
    public List<Map<String, Object>> getNewUserCount(Map<String, Object> params) {
        return userMapper.getNewUserCount(params);
    }
    public List<SalesDetail> getSalesDetailParams(Map<String, Object> params) {
        return orderMapper.getSalesDetailParams(params);
    }

    @Override
    /**
     *@描述 获取销售数据
     *@参数 []
     *@返回值 com.dkm.modules.sys.statistics.model.SalesData
     *@创建人 yangbin
     *@创建时间 2019/10/24
     */
    public SalesData getSalesData() {
        return orderMapper.getSalesData();
    }

    @Override
    /**
     *@描述 获取销售详情
     *@参数 []
     *@返回值 java.util.List<com.dkm.modules.sys.statistics.model.SalesDetail>
     *@创建人 yangbin
     *@创建时间 2019/10/24
     */
    public IPage<SalesDetail> getSalesDetail(Page<SalesDetail> page) {
        List<SalesDetail> salesDetails = orderMapper.getSalesDetail(page);
        page.setRecords(salesDetails);
        return page;
    }

    /**
     * 获取设备统计详情
     *
     * @param page
     */
    @Override
    public IPage<PilesDetail> getPilesDetail(Page<PilesDetail> page) {
        List<PilesDetail> salesDetails = pileMapper.selectPilesInfo(page);
        page.setRecords(salesDetails);
        return page;
    }

    @Override
    public void exportPilesInfo(HttpServletResponse res) {
        /*模板名称*/
        String templateFileName = "pilesInfoTemp.xls";
        /*生成的文件名*/
        String destFileName = "pilesDetails.xls";
        List<PilesDetail> pilesDetails = pileMapper.selectPilesInfo(null);

        Map<String, Object> data = new HashMap<>();
        data.put("pilesDetails", pilesDetails);
        /*生成新文件*/
        ExportExcelUtil.exportExcel(templateFileName, destFileName, data, res);
    }

    /**
     * 功能描述：分页按天查询充值
     * @param page 分页信息
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.dkm.modules.sys.statistics.model.RechargeDetail> 充值
     * @Author: guoliangbo
     * @Date: 2019/12/26 11:32
     */
    @Override
    public IPage<RechargeDetail> getRechargeDetail(Page<RechargeDetail> page) {
        List<RechargeDetail> rechargeDetails = userMapper.getRechargeDetail(page);
        page.setRecords(rechargeDetails);
        return page;
    }

    /**
     * 功能描述：查询每日充值明细
     * @param page 分页信息
     * @param params 查询参数 日期 2019-12-24格式
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.dkm.modules.sys.statistics.model.DayRecharge> 结果
     * @Author: guoliangbo
     * @Date: 2019/12/26 15:53
     */
    @Override
    public IPage<DayRecharge> getDayRecharge(Page<DayRecharge> page, Map<String, String> params) {
        List<DayRecharge> dayRecharges = userMapper.getDayRecharge(page,params);
        page.setRecords(dayRecharges);
        return page;
    }

    @Override
    /**
     *@描述 下载表格
     *@参数 []
     *@返回值 void
     *@创建人 yangbin
     *@创建时间 2019/10/24
     */
    public void exportTable(HttpServletResponse response) {
        /*模板名称*/
        String templateFileName = "salesDetailTemp.xls";
        /*生成的文件名*/
        String destFileName = "salesDetail.xls";

        List<SalesDetail> salesDetails = orderMapper.getSalesDetail(null);
        Map<String, Object> data = new HashMap<>();
        data.put("salesDetails", salesDetails);
        /*生成新文件*/
        ExportExcelUtil.exportExcel(templateFileName, destFileName, data, response);

    }

}
