
package com.dkm.modules.sys.role.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.PageQueryParams;
import com.dkm.commons.Response;
import com.dkm.modules.sys.role.model.RolePO;
import com.dkm.modules.sys.role.model.request.AddRoleRequest;
import com.dkm.modules.sys.role.model.request.EditRoleRequest;
import com.dkm.modules.sys.role.model.request.RoleMenuRequest;
import com.dkm.modules.sys.role.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @ClassName RoleController
 * @Description: 角色权限管理
 * @Author yangbin
 * @Date 2019-09-17 23:56
 * @Version V1.0
 **/
@Api(tags = "A9 运营者管理 --角色管理")
@Slf4j
@RestController
@RequestMapping("/role")
public class RoleController extends BaseController {

    @Autowired
    private RoleService roleService;

    @ApiOperation("新增角色")
    @PostMapping("/addRole")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response addRole(@Valid @RequestBody AddRoleRequest addRole) {

        roleService.addRole(addRole);

        return Response.ok();
    }

    @ApiOperation("修改角色")
    @PostMapping("/editRole")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response editRole(@Valid @RequestBody EditRoleRequest editRoleRequest) {

        roleService.editRole(editRoleRequest);

        return Response.ok();
    }

    @ApiOperation("删除角色")
    @DeleteMapping("/delRole")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response delRole(@ApiParam("角色ID") @RequestParam("id") Integer id) {

        roleService.delRole(id);

        return Response.ok();
    }


    @ApiOperation("配置角色菜单")
    @PostMapping("/setRoleMenu")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response setRoleMenu(@RequestBody RoleMenuRequest roleMenus) {

        roleService.saveRoleMenus(roleMenus);

        return Response.ok();
    }

    @ApiOperation("角色列表")
    @GetMapping("/getRoleList")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<IPage<RolePO>> getRoleList(@ApiParam("搜索关键字") @RequestParam(value = "keyWord", required = false) String keyWord,
    PageQueryParams p) {

        Page<RolePO> page = new Page<>(p.getPageNo(), p.getPageSize());

        IPage<RolePO> userResultIPage  = roleService.getRoleList(keyWord,page);

        return Response.ok(userResultIPage);

    }

    @ApiOperation("获取用户的角色信息")
    @GetMapping("/getRoleSelect")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<List<RolePO>> getRoleSlect(@ApiParam("用户id") @RequestParam(value = "id", required = false) Integer id) {

        List<RolePO> roleResults = roleService.getRoleSelect(id);

        return Response.ok(roleResults);
    }


}
