package com.dkm.modules.sys.role.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2020-01-02 22:14
 */
@Data
@ApiModel(description = "角色菜单权限实体")
public class RoleMenuRequest implements Serializable {

    @NotBlank(message = "角色code必填")
    @ApiModelProperty(value = "角色code", required = true)
    private Integer roleCode;

    @Size(min = 1, message = "菜单code至少选择一条")
    @NotBlank(message = "菜单code必填")
    @ApiModelProperty(value = "菜单code", required = true)
    private List<Integer> menuCodes;

}
