
package com.dkm.modules.sys.menu.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.menu.model.MenuPO;
import com.dkm.modules.sys.menu.model.request.AddMenuRequest;

import java.util.List;


/**
 * @ClassName Menu
 * @Description:
 * @Author yangbin
 * @Date 2019-09-17 23:42
 * @Version V1.0
 **/
public interface MenuService {

    /**
     * 获取用户的所有菜单和按钮权限
     *
     * @param userId
     * @return
     */
    List getAllPermissionMenu(Integer userId);

    void addMenu(AddMenuRequest menu);

    void updateMenu(AddMenuRequest menu);

    void delMenu(Integer id);

    IPage<MenuPO> getMenuList(String keyWord, Page<MenuPO> page);

    /**
     * 获取所有菜单和按钮
     *
     * @return
     */
    List<MenuPO> getMenuTree();

    /**
     * 获取所有菜单,不区分菜单和按钮
     *
     * @return
     */
    List<MenuPO> getAllMenuAndBtn();

    /**
     * 获取角色拥有的菜单Code
     *
     * @return
     */
    List<Integer> getMenuAndBtnByRoleCode(Integer roleCode);

    /**
     * 获取所有菜单
     *
     * @return
     */
    List<MenuPO> getAllMenus();

    /**
     * 获取角色的所有菜单
     *
     * @param roleCodes
     * @return
     */
    List<MenuPO> getMenuAndBtnByRoleCodes(List<Integer> roleCodes);

    /**
     * 获取用户所有的菜单权限
     *
     * @param id
     * @return
     */
    List<MenuPO> getAllPermissionButton(Integer id);
}