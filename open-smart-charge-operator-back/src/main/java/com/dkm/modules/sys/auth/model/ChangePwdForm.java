package com.dkm.modules.sys.auth.model;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @描述 密码修改接收类
 * @创建时间 2019/10/29 11:13
 * @创建人 yangbin
 */
@Data
public class ChangePwdForm {

    @ApiModelProperty(value = "原密码",required = true)
    @NotBlank(message = "原密码不能为空")
    private String password;

    @ApiModelProperty(value = "新密码",required = true)
    @NotBlank(message = "新密码不能为空")
    private String newPassword;
}
