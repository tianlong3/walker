package com.dkm.modules.sys.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.sys.user.model.CustomerBalance;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * DESCRIPTION : 注册用户余额
 *
 * @author ducf
 * @create 2019-12-20 19:35
 */
@Repository
public interface CustomerBalanceMapper extends BaseMapper<CustomerBalance> {

    /**
     * 根据注册用户openId,更新账户余额,默认做加法
     *
     * @param openId
     * @param amount
     * @return
     */
    int updateAmountByOpenId(@Param("openId") String openId, @Param("amount") BigDecimal amount);

}
