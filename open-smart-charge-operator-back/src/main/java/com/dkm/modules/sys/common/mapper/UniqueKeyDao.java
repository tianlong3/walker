package com.dkm.modules.sys.common.mapper;

import com.dkm.modules.sys.common.model.UniqueKeyPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2020-01-02 21:47
 */
@Component
public class UniqueKeyDao {

    @Autowired
    UniqueKeyMapper uniqueKeyMapper;

    /**
     * 获取一个全局唯一的code码
     *
     * @return
     */
    public Integer getUniqueCode() {
        UniqueKeyPO uniqueKeyPO = new UniqueKeyPO();
        uniqueKeyMapper.insertOne(uniqueKeyPO);
        return uniqueKeyPO.getId();
    }
    

}
