package com.dkm.modules.sys.finance.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.finance.model.FinanceResult;
import com.dkm.modules.sys.finance.service.FinanceService;
import com.dkm.modules.sys.statistics.model.SalesDetail;
import com.dkm.modules.wx.order.mapper.OrderMapper;
import com.dkm.util.excel.ExportExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @描述 财务管理接口实现类
 * @创建时间 2019/10/21 16:33
 * @创建人 yangbin
 */
@Service
public class FinanceServiceImpl implements FinanceService {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    /**
     *@描述   获取财务列表数据
     *@参数  [params, page]
     *@返回值  com.baomidou.mybatisplus.core.metadata.IPage<com.dkm.modules.sys.finance.model.FinanceResult>
     *@创建人  yangbin
     *@创建时间  2019/10/20
     */
    public IPage<FinanceResult> getFinanceList(Map<String, Object> params, Page<FinanceResult> page) {

        List<FinanceResult> datas = orderMapper.getFinanceList(params,page);
        page.setRecords(datas);
        return page;
    }

    @Override
    /**
     *@描述  查询总销售额
     *@参数  []
     *@返回值  java.util.Map<java.lang.String,java.lang.Object>
     *@创建人  yangbin
     *@创建时间  2019/10/20
     */
    public Map<String, Object> getSumFinace() {
        return orderMapper.getSumFinace();
    }

    @Override
    /**
     *@描述   导出excel
     *@参数  []
     *@返回值  void
     *@创建人  yangbin
     *@创建时间  2019/10/24
     */
    public void ExportExcel(Map<String, Object> params, HttpServletResponse res) {
        /*模板名称*/
        String templateFileName= "financeTemp.xls";
        /*生成的文件名*/
        String destFileName= "finance.xls";

        List<FinanceResult> datas = orderMapper.getFinanceList(params,null);
        Map<String,Object> data =new HashMap<>();
        data.put("datas", datas);
        /*生成新文件*/
        ExportExcelUtil.exportExcel(templateFileName,destFileName,data,res);

    }
}
