package com.dkm.modules.sys.promotion.model;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @描述 促销修改实体
 * @创建时间 2019/10/20 13:50
 * @创建人 yangbin
 */
@Data
public class EditPromotion extends AddPromotion{


    @TableId
    @ApiModelProperty("促销广告id")
    private Integer id;
}
