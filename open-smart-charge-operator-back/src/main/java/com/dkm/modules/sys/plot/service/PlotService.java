package com.dkm.modules.sys.plot.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.Response;
import com.dkm.modules.sys.plot.model.PlotResult;
import com.dkm.modules.wx.city.model.PlotModel;

import java.util.Map;

/**
 * @描述 小区管理接口
 * @创建时间 2019/10/26 23:16
 * @创建人 yangbin
 */
public interface PlotService {
    /**
     * 返回小区id
     * @param plotModel
     * @return
     */
    Integer addPlot(PlotModel plotModel);

    void editPlot(PlotModel plotModel);

    void delPlot(Integer id);

    IPage<PlotResult> getPlotList(Map<String, Object> params, Page<PlotResult> page);
}
