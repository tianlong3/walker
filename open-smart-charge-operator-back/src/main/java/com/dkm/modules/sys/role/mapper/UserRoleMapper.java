        
package com.dkm.modules.sys.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.sys.role.model.UserRolePO;
import org.springframework.stereotype.Repository;


/**
 * @ClassName 用户 角色对应Mapper
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-17 23:56
 * @Version V1.0
 **/
@Repository
public interface UserRoleMapper extends BaseMapper<UserRolePO> {

}

