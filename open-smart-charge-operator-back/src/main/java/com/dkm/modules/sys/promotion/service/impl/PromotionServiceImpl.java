package com.dkm.modules.sys.promotion.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.promotion.mapper.PromotionMapper;
import com.dkm.modules.sys.promotion.model.Promotion;
import com.dkm.modules.sys.promotion.service.PromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @描述 促销管理实现类
 * @创建时间 2019/10/12 21:18
 * @创建人 yangbin
 */
@Service
public class PromotionServiceImpl implements PromotionService {
    @Autowired
    private PromotionMapper promotionMapper;

    @Override
    /**
     *@描述   新增促销
     *@参数  [promotion 促销数据]
     *@返回值  void
     *@创建人  yangbin
     *@创建时间  2019/10/12
     */
    public void addPromotion(Promotion promotion) {
        /*创建时间*/
        promotion.setCreateTime(new Date());
        promotionMapper.insert(promotion);
    }

    @Override
    /**
     *@描述   删除促销
     *@参数  [id 促销数据主键ID]
     *@返回值  void
     *@创建人  yangbin
     *@创建时间  2019/10/17
     */
    public void delPromotion(Integer id) {
        Promotion promotion = new Promotion();
        promotion.setId(id);
        promotion.setDeleteFlag(1);
        promotionMapper.updateDeleteStatus(promotion);
    }

    @Override
    /**
     *@描述   修改促销信息
     *@参数  [promotion 促销数据]
     *@返回值  void
     *@创建人  yangbin
     *@创建时间  2019/10/17
     */
    public void updatePromotion(Promotion promotion) {
        /*设置更新时间*/
        promotion.setUpdateTime(new Date());
        promotionMapper.updateById(promotion);
    }

    @Override
    /**
     *@描述   获取促销数据列表
     *@参数  [params, promotionPage]
     *@返回值  com.baomidou.mybatisplus.core.metadata.IPage<com.dkm.modules.sys.promotion.model.Promotion>
     *@创建人  yangbin
     *@创建时间  2019/10/17
     */
    public IPage<Promotion> getPromotionList(Map<String, Object> params, Page<Promotion> promotionPage) {
        List<Promotion> advertList = promotionMapper.getPromotionList(params,promotionPage);
        promotionPage.setRecords(advertList);
        return promotionPage;
    }


    @Override
    public boolean checkPromotionByTotalAmount(Promotion promotion1) {
        boolean flag = false;
        Double totalAmount = promotion1.getTotalAmount();
        Promotion promotionInDb = promotionMapper.selectByTotalAmount(totalAmount);
        // 如果是新增的，查询为空，就是可以新增
        if(null == promotionInDb){
            flag = true;
            // 如果是编辑，查询出的id相同，也可以编辑
        }else if(promotionInDb.getId().equals(promotion1.getId())){
            flag = true;
        }
        return flag;
    }
}
