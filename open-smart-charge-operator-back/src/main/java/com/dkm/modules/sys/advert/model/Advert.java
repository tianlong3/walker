package com.dkm.modules.sys.advert.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @描述 广告实体
 * @创建时间 2019/10/10 13:06
 * @创建人 yangbin
 */
@Data
@ApiModel("广告实体")
public class Advert {
    @ApiModelProperty(value = "广告Id",hidden = true)
    @TableId
    private Integer id;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("链接")
    private String url;

    @ApiModelProperty("广告图片Id")
    private String fileId;

    @ApiModelProperty(value = "广告图片URL",hidden = true)
    private String fileUrl;

    @ApiModelProperty("展示页面  公共代码parentCode=1")
    private String showPage;

    @ApiModelProperty(value = "状态",hidden = true)
    private String state;

    @ApiModelProperty(value ="创建时间",hidden = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "创建人",hidden = true)
    private String createBy;

}
