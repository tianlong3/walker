package com.dkm.modules.sys.operator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.sys.operator.model.UserDatascopePO;
import org.springframework.stereotype.Repository;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2020-01-17 15:24
 */
@Repository
public interface UserDataScopeMapper extends BaseMapper<UserDatascopePO> {
}
