
package com.dkm.modules.sys.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.PageQueryParams;
import com.dkm.commons.Response;
import com.dkm.modules.sys.user.model.*;
import com.dkm.modules.sys.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName UserController
 * @Description: 控制器
 * @Author yangbin
 * @Date 2019-09-18 00:01
 * @Version V1.0
 **/
@Api(tags = "A9 运营者管理 --运营人员管理", value = "用户管理")
@RestController
@RequestMapping("/sysUser")
public class SysUserController extends BaseController {

    private final static Logger logger = LoggerFactory.getLogger(SysUserController.class);

    @Autowired
    private UserService userService;

    @PostMapping(value = {"/add"})
    @ApiOperation(value = "添加运营者")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response add(@RequestBody @ApiParam(value = "用户数据") AddUser user) {
        logger.info("添加运营者");
        try {
            userService.addUser(user);
        } catch (Exception e) {
            return Response.error(-1, e.getMessage());
        }
        return Response.ok();
    }

    @DeleteMapping(value = {"/delUser"})
    @ApiOperation(value = "删除运营者")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response delUser(@ApiParam("运营者ID") @RequestParam("id") Integer id) {
        logger.info("删除运营者");
        userService.delUser(id);
        return Response.ok();
    }

    @PostMapping(value = {"/updateUser"})
    @ApiOperation(value = "修改运营者")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response updateUser(@RequestBody EditUser user) {
        logger.info("修改运营者：" + user.getRealName());
        userService.updateUser(user);
        return Response.ok();
    }

    @PostMapping(value = {"/getUserList"})
    @ApiOperation(value = "运营者列表数据")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<IPage<UserResult>> getUserList(@ApiParam("搜索关键字") @RequestParam(value = "keyWord", required = false) String keyWord,
                                                   PageQueryParams p) {
        Page<UserResult> page = new Page<>(p.getPageNo(), p.getPageSize());
        Map<String, Object> params = new HashMap<>();
        /*搜索关键字*/
        params.put("keyWord", keyWord);
        IPage<UserResult> userResultIPage = userService.getUserList(params, page);
        return Response.ok(userResultIPage);
    }

    @ApiOperation("配置角色")
    @PostMapping("/setUserRole")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response setUserRole(@ApiParam("角色数据 ") @RequestBody UserRoles roleResults) {

        logger.info("配置角色");
        userService.setUserRole(roleResults);
        return Response.ok();
    }

    @ApiOperation("分配代理商")
    @PostMapping("/setOperator")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response setOperator(@ApiParam("数据数据 ") @RequestBody OperatorSet operatorSet) {

        logger.info("分配代理商");
        userService.setOperator(operatorSet);
        return Response.ok();
    }

   /* @ApiOperation("获取用户的角色下拉")
    @GetMapping("/getUserRoleList")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<List<RoleSelect>> getUserRoleList(@ApiParam("用户ID")@RequestParam("id")Integer id){
        logger.info("获取用户的角色select："+id);
        List<RoleSelect> roleResults =userService.getUserRoleList(id);
        return Response.ok(roleResults);
    }*/

    @ApiOperation("运营端-重置密码")
    @PostMapping("/resetPassword")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response resetPassword(@ApiParam("用户id") @RequestParam("userId") Integer userId) {
        Integer operatorId = getUserId();
        return userService.resetPassword(userId, operatorId);
    }
}
