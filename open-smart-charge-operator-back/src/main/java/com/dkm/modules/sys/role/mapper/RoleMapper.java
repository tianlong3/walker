
package com.dkm.modules.sys.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.sys.role.model.RolePO;
import org.springframework.stereotype.Repository;


/**
 * @ClassName RoleMapper
 * @Description:
 * @Author yangbin
 * @Date 2019-09-17 23:56
 * @Version V1.0
 **/
@Repository
public interface RoleMapper extends BaseMapper<RolePO> {

}

