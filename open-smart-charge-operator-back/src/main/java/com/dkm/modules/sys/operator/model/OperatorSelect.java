package com.dkm.modules.sys.operator.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @描述 代理商select框数据
 * @创建时间 2019/10/27 11:26
 * @创建人 yangbin
 */
@ApiModel("代理商select 数据")
@Data
public class OperatorSelect {

    @ApiModelProperty("代理商id")
    private Integer operatorId;

    @ApiModelProperty("代理商名称")
    private String operatorNm;

}
