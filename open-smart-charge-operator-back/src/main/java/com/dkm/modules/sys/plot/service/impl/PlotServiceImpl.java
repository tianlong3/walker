package com.dkm.modules.sys.plot.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.exception.ServiceException;
import com.dkm.modules.sys.plot.model.PlotResult;
import com.dkm.modules.sys.plot.service.PlotService;
import com.dkm.modules.wx.city.mapper.CityMapper;
import com.dkm.modules.wx.city.mapper.PlotMapper;
import com.dkm.modules.wx.city.model.City;
import com.dkm.modules.wx.city.model.PlotModel;
import com.dkm.util.BaiDuGeoCodingUtil;
import com.dkm.util.BaiDuGeoCodingUtil.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @描述 小区管理接口实现类
 * @创建时间 2019/10/26 23:16
 * @创建人 yangbin
 */
@Service
public class PlotServiceImpl implements PlotService {

    @Autowired
    private PlotMapper plotMapper;
    @Autowired
    private CityMapper cityMapper;

    /**
     * @描述 添加小区
     * @参数 [plotModel 小区数据]
     * @返回值 com.dkm.commons.Response
     * @创建人 yangbin
     * @创建时间 2019/10/26
     */
    @Override
    public Integer addPlot(PlotModel plotModel) {
        plotModel.setCreateTime(new Date());
        //获取经纬度
        setLocation(plotModel);
        int id = plotMapper.insert(plotModel);
        return id;
    }

    private void setLocation(PlotModel plotModel) {
        City area = cityMapper.selectById(plotModel.getRegionCode());
        if (null == area) {
            throw new ServiceException("区域不存在,请检查区域id");
        }
        City city = cityMapper.selectById(area.getPid());
        Location location = BaiDuGeoCodingUtil
                .getLocation(city.getName().trim() + area.getName().trim() + plotModel.getAddress().trim());
        if (location != null) {
            plotModel.setLat(location.getLat());
            plotModel.setLng(location.getLng());
        }
    }


    /**
     * @描述 修改小区信息
     * @参数 [plotModel 小区数据]
     * @返回值 void
     * @创建人 yangbin
     * @创建时间 2019/10/26
     */
    @Override
    public void editPlot(PlotModel plotModel) {
        //获取经纬度
        setLocation(plotModel);
        plotMapper.updateById(plotModel);
    }


    /**
     * @描述 删除小区
     * @参数 [id 小区ID]
     * @返回值 void
     * @创建人 yangbin
     * @创建时间 2019/10/26
     */
    @Override
    public void delPlot(Integer id) {
        plotMapper.deleteById(id);
    }

    /**
     * @描述 获取小区列表数据
     * @参数 [params查询参数, page]
     * @返回值 com.baomidou.mybatisplus.core.metadata.IPage<com.dkm.modules.sys.plot.model.PlotResult>
     * @创建人 yangbin
     * @创建时间 2019/10/26
     */
    @Override
    public IPage<PlotResult> getPlotList(Map<String, Object> params, Page<PlotResult> page) {
        List<PlotResult> plotResultList = plotMapper.getPlotList(params, page);
        page.setRecords(plotResultList);
        return page;
    }
}
