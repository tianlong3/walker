package com.dkm.modules.sys.operator.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.Response;
import com.dkm.modules.sys.operator.model.OperatorResult;
import com.dkm.modules.sys.operator.model.OperatorSelect;
import com.dkm.modules.sys.operator.model.SetOperatorDTO;
import com.dkm.modules.sys.operator.model.UserOperator;
import com.dkm.modules.sys.user.model.AddUser;
import com.dkm.modules.sys.user.model.EditUser;
import com.dkm.modules.sys.user.model.User;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface OperatorService {
    Response addUser(AddUser operator);


    IPage<OperatorResult> getOperatorList(Page<OperatorResult> page, Map<String, String> params);

    void delOperator(Integer id);

    void editOperator(EditUser user);

    List<OperatorSelect> getOperatorSelect();

    /**
      * 功能描述：根据代理商id，获取累计分成金额
      * @param operatorId 代理商id
      * @return com.dkm.commons.Response 返回记录
      * @Author: guoliangbo
      * @Date: 2020/1/7 17:26
      */
    Response getAccumulatedAmount(int operatorId);

    /**
      * 功能描述：设置代理商分成比例
      * @param operatorId 代理商id
      * @param royaltyPercent 分成比例
      * @return com.dkm.commons.Response 响应结果
      * @Author: guoliangbo
      * @Date: 2020/1/7 17:38
      */
    Response setUpRoyaltyPercent(int operatorId, BigDecimal royaltyPercent);

    void setOperatorsForUser(SetOperatorDTO setOperatorDTO);

    /**
     * 获取所有运营者
     *
     * @return
     */
    List<User> getAllUser();

    /**
     * 分页查询运营者
     *
     * @param keyWord
     * @param p
     * @return
     */
    IPage<UserOperator> getUserOperatorsByPage(String keyWord, Page<UserOperator> p);

    /**
     * 删除运营者的某个代理商数据权限
     *
     * @param id
     */
    void deleteOpertaors(Integer id);
}
