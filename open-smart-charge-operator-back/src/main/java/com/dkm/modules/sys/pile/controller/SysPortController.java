
package com.dkm.modules.sys.pile.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.PageQueryParams;
import com.dkm.commons.Response;
import com.dkm.modules.sys.pile.model.AddPort;
import com.dkm.modules.sys.pile.model.EditPort;
import com.dkm.modules.sys.user.model.User;
import com.dkm.modules.wx.port.model.ChargingPort;
import com.dkm.modules.wx.port.service.ChargingPortService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName ChargingPortController
 * @Description: 控制器
 * @Author yangbin
 * @Date 2019-09-05 13:52
 * @Version V1.0
 **/
@Api(tags="A3 充电柱管理 ---端口管理")
@RestController
@RequestMapping("/port")
public class SysPortController extends BaseController {

    private final static Logger logger = LoggerFactory.getLogger(SysPortController.class);
    @Autowired
    private ChargingPortService chargingPortService;

    @ApiOperation("添加端口")
    @PostMapping("/addPort")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response addPort(@ApiParam("端口数据")@RequestBody AddPort port) {
        logger.info("添加端口");
        /*类型转换*/
        String portStr= JSONObject.toJSONString(port);
        ChargingPort chargingPort = JSONObject.parseObject(portStr,ChargingPort.class);
        chargingPortService.addPort(chargingPort);
        return Response.ok();
    }


    @ApiOperation("编辑端口")
    @PostMapping("/editPort")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response editPort(@ApiParam("端口数据")@RequestBody EditPort port) {
        logger.info("编辑端口");
        /*类型转换*/
        String portStr= JSONObject.toJSONString(port);
        ChargingPort port1 = JSONObject.parseObject(portStr,ChargingPort.class);
        chargingPortService.editPort(port1);
        return Response.ok();
    }


    @ApiOperation("获取端口列表")
    @GetMapping("/getPortList")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response getPortList(PageQueryParams p,
                                @ApiParam("充电桩ID") @RequestParam("id")String id,
                                @ApiParam("端口编号") @RequestParam(value = "deviceid",required = false)String deviceid) {

        logger.info("编辑端口");
        /*组装查询参数*/
        Map<String, String> params = new HashMap<>();
        /*充电桩编号*/
        params.put("id", id);
        /*投放地址*/
        params.put("deviceid", deviceid);
        //获取端口分页后列表
        Page<ChargingPort> page = new Page<>(p.getPageNo(),p.getPageSize());
        IPage<ChargingPort> chargingPiles = chargingPortService.getPortList(page, params);
        return Response.ok(chargingPiles);
    }



    @ApiOperation("删除端口")
    @DeleteMapping("/delPort")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response delPort(@ApiParam("端口id")@RequestParam Integer id){
        logger.info("删除端口："+id);
        try {
            chargingPortService.delPort(id);
        }catch (Exception e){
            return Response.error(-1, e.getMessage());
        }
        return Response.ok();
    }

    @ApiOperation("端口开启或关闭")
    @PostMapping("/switchPort")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response switchPort(@ApiParam("端口id")@RequestParam Integer id,@ApiParam("开启1，关闭0")@RequestParam Integer type){
        logger.info("端口："+id + " 开关：" + type);
        return chargingPortService.switchPort(id,type);
    }


    /****
     * 按照不同充电桩开启20个端口
     */
    @ApiOperation("开启不同充电桩开始")
    @PostMapping("/allPort")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response allPort(@ApiParam("充电桩桩号")@RequestParam String code,@ApiParam("开启1，关闭0")@RequestParam Integer type){
        logger.info("充电桩编号："+code + " 开关：" + type);
        return chargingPortService.allPort(code,type);
    }
}
