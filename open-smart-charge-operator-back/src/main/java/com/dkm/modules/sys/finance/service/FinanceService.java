package com.dkm.modules.sys.finance.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.finance.model.FinanceResult;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @描述
 * @创建时间 2019/10/20 16:32
 * @author yangbin
 */
public interface FinanceService {
    /**
     * 获取财务信息
     * @param params type 统计方式 1：分销商 2：充电桩和 keyword 设备编号或分销商手机号
     * @param page 分页信息
     * @return  分页查询结果
     */
    IPage<FinanceResult> getFinanceList(Map<String, Object> params, Page<FinanceResult> page);

    Map<String, Object> getSumFinace();

    void ExportExcel(Map<String, Object> params, HttpServletResponse res);
}
