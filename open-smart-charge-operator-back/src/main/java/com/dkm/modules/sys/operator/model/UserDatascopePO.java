package com.dkm.modules.sys.operator.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DESCRIPTION : 运营者代理商权限关联关系
 *
 * @author ducf
 * @create 2020-01-17 15:22
 */
@Data
@TableName("c_user_datascope")
@NoArgsConstructor
public class UserDatascopePO {

    @TableId
    private Integer id;

    /**
     * 运营者id
     */
    private Integer userId;

    /**
     * 代理商id
     */
    private Integer operatorId;

    public UserDatascopePO(Integer userId, Integer operatorId) {
        this.userId = userId;
        this.operatorId = operatorId;
    }
}
