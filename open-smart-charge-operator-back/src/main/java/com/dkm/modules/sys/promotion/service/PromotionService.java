package com.dkm.modules.sys.promotion.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.promotion.model.Promotion;

import java.util.Map;

/**
 * @描述 促销管理sevice
 * @创建时间 2019/10/12 21:18
 * @创建人 yangbin
 */
public interface PromotionService {
    void addPromotion(Promotion promotion);

    void delPromotion(Integer id);

    void updatePromotion(Promotion promotion);

    IPage<Promotion> getPromotionList(Map<String, Object> params, Page<Promotion> promotionPage);

    /**
      * 功能描述：检测是否存在相同的促销规则（满足金额相同）
      * @param promotion1 促销信息
      * @Return:boolean 结果
      * @Author: Guo Liangbo
      * @Date:2019/11/10 16:41
      */
    boolean checkPromotionByTotalAmount(Promotion promotion1);
}
