package com.dkm.modules.sys.advert.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.advert.mapper.AdvertMapper;
import com.dkm.modules.sys.advert.model.Advert;
import com.dkm.modules.sys.advert.service.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @描述 广告管理实现类
 * @创建时间 2019/10/10 17:16
 * @创建人 yangbin
 */
@Service
public class AdvertServiceImpl implements AdvertService {

    @Autowired
    private AdvertMapper advertMapper;

    @Override
    /**
     *@描述   获取广告列表
     *@参数  [params 查询参数, page]
     *@返回值  com.baomidou.mybatisplus.core.metadata.IPage<com.dkm.modules.sys.Advert.model.Advert>
     *@创建人  yangbin
     *@创建时间  2019/10/10
     */
    public IPage<Advert> getAdvertList(Map<String, Object> params, Page<Advert> page) {
        List<Advert> advertList = advertMapper.getAdvertList(params,page);
        page.setRecords(advertList);
        return page;
    }

    @Override
    /**
     *@描述   添加广告
     *@参数  [advert 广告数据]
     *@返回值  void
     *@创建人  yangbin
     *@创建时间  2019/10/10
     */
    public void addAdvert(Advert advert) {
        advert.setCreateTime(new Date());
        advertMapper.insert(advert);
    }

    @Override
    /**
     *@描述   根据广告ID删除广告
     *@参数  [id 广告ID]
     *@返回值  void
     *@创建人  yangbin
     *@创建时间  2019/10/10
     */
    public void delAdvert(Integer id) {
        advertMapper.deleteById(id);
    }

    @Override
    /**
     *@描述   修改广告数据
     *@参数  [advert 广告数据]
     *@返回值  void
     *@创建人  yangbin
     *@创建时间  2019/10/16
     */
    public void updateAdvert(Advert advert) {
        advertMapper.updateById(advert);
    }
}
