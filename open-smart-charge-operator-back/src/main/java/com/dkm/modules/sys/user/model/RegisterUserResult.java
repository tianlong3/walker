package com.dkm.modules.sys.user.model;

import com.dkm.util.StringUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @描述 注册用户返回结果
 * @创建时间 2019/11/9 12:55
 * @创建人 yangbin
 */
@Data
public class RegisterUserResult {
    @ApiModelProperty("用户ID")
    private Integer id;

    @ApiModelProperty("头像")
    private String headImage;

    @ApiModelProperty("昵称")
    private String userName;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("注册时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createtime;

    @ApiModelProperty("使用次数")
    private Integer useCount;

    @ApiModelProperty("余额")
    private Float amount;

    @ApiModelProperty("卡余额")
    private Float cardAmount;

    @ApiModelProperty("总余额")
    private Float sumAmount;

    @ApiModelProperty("状态 Y：正常 N 禁用")
    private String isvalid;

    public String getAmount() {
        return StringUtils.formatMoney(amount);
    }

    public String getCardAmount() {
        return StringUtils.formatMoney(cardAmount);
    }

    public String getSumAmount() {
        return StringUtils.formatMoney(sumAmount);
    }

}
