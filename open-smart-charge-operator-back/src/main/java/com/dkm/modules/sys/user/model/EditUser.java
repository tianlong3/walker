package com.dkm.modules.sys.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @描述 修改用户数据
 * @创建时间 2019/10/12 11:28
 * @创建人 yangbin
 */
@Data
@ApiModel("修改数据")
public class EditUser extends AddUser{

    @ApiModelProperty(value = "id")
    private Integer id;
}
