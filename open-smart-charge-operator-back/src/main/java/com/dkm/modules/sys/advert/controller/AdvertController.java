package com.dkm.modules.sys.advert.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.modules.sys.advert.model.Advert;
import com.dkm.modules.sys.advert.service.AdvertService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @描述 广告管理控制器
 * @创建时间 2019/10/10 17:18
 * @创建人 yangbin
 */
@Api(tags = "A11 运营端-广告管理")
@RestController
@RequestMapping("/advert")
public class AdvertController extends BaseController {

    private final static Logger logger = LoggerFactory.getLogger(AdvertController.class);

    @Autowired
    private AdvertService advertService;

    @ApiOperation("广告列表")
    @GetMapping("getAdvertList")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<IPage<Advert>> getAdvertList(@ApiParam("广告标题")@RequestParam(value ="title",required = false)String title,
                                                 @ApiParam("页数") @RequestParam("pageNo")int pageNo,
                                                 @ApiParam("每页显示数") @RequestParam("pageSize")int pageSize){
        logger.info("获取广告列表");
        Page<Advert> page = new Page<>(pageNo,pageSize);
        Map<String,Object> params = new HashMap<>();
        params.put("title",title);
        IPage<Advert> advertIPage = advertService.getAdvertList(params,page);
        return Response.ok(advertIPage);
    }

    @ApiOperation("新增广告")
    @PostMapping("addAdvert")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response addAdvert(@ApiParam("广告")@RequestBody Advert advert){
        logger.info("新增广告");
        /*设置添加人为当前用户*/
        advert.setCreateBy(getUserId().toString());
        /*状态默认为开启*/
        advert.setState("1");
        advertService.addAdvert(advert);
        return Response.ok();
    }

    @ApiOperation("删除广告")
    @DeleteMapping("delAdvert")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response delAdvert(@ApiParam("广告ID")@RequestParam("id") Integer id){
        logger.info("删除广告:"+id);
        advertService.delAdvert(id);
        return Response.ok();
    }

    @ApiOperation("启用/停用--广告")
    @GetMapping("useAdvert")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response useAdvert(@ApiParam("广告ID")@RequestParam("id") Integer id,
                              @ApiParam("状态（1:启用 0：停用）")@RequestParam("state") String state){
        logger.info("启用/停用--广告:"+id);
        Advert advert = new Advert();
        advert.setId(id);
        advert.setState(state);
        advertService.updateAdvert(advert);
        return Response.ok();
    }


}
