package com.dkm.modules.sys.finance.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @描述 设备编号返回数据
 * @创建时间 2019/10/20 16:26
 * @创建人 yangbin
 */
@Data
@ApiModel("财务数据")
public class FinanceResult {

    @ApiModelProperty("充电桩编号")
    private String pileId;

    @ApiModelProperty("分销商姓名")
    private String userNm;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("日销售额")
    private Float daySales;

    @ApiModelProperty("月销售额")
    private Float monSales;

    @ApiModelProperty("年销售额")
    private Float ySales;
}
