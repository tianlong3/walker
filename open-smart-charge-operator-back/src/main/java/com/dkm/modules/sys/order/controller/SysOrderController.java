
package com.dkm.modules.sys.order.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.modules.wx.order.model.OrderQueryForm;
import com.dkm.modules.wx.order.model.OrderResult;
import com.dkm.modules.wx.order.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName ChargingOrderController
 * @Description: 订单控制器
 * @Author yangbin
 * @Date 2019-09-09 23:38
 * @Version V1.0
 **/
@Api(tags = "A4 订单管理")
@RestController
@RequestMapping("/sysOrder")
public class SysOrderController extends BaseController{

    private final static Logger logger = LoggerFactory.getLogger(SysOrderController.class);
    @Autowired
    private OrderService chargingOrderService;


    @ApiOperation("获取订单列表")
    @GetMapping("/getOrderList")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<IPage<OrderResult>> getOrderList(@ApiParam("订单状态") @RequestParam(value = "orderState",required = false)String orderState,
                                 @ApiParam("分销商Id") @RequestParam(value = "userId",required = false)String userId,
                                 @ApiParam("用户名或订单号") @RequestParam(value = "userOrNum",required = false)String userOrNum,
                                 @ApiParam("端口号") @RequestParam(value = "portCode",required = false)String portCode,
                                 @ApiParam("页数") @RequestParam(value ="pageNo",defaultValue = "1")int pageNo,
                                 @ApiParam("开始时间") @RequestParam(value ="startTime" ,required = false)String startTime,
                                 @ApiParam("结束时间") @RequestParam(value ="endTime" ,required = false)String endTime,
                                 @ApiParam("每页显示数") @RequestParam(value = "pageSize",defaultValue = "10") int pageSize){
        logger.info("获取订单列表");
        Map<String,Object> params = new HashMap<>();
        params.put("orderState",orderState);
        params.put("userId",userId);
        params.put("userOrNum",userOrNum);
        // 端口编号  88-16
        params.put("portCode",portCode);
        params.put("order",new OrderQueryForm());
        params.put("startTime1",startTime);
        params.put("endTime",endTime);
        /*分页查询订单列表*/
        Page<OrderResult> page = new Page<>(pageNo,pageSize);
        IPage<OrderResult>orderList =chargingOrderService.getOrderList(params,page);
        return Response.ok(orderList);
    }

    @ApiOperation("订单详情")
    @GetMapping("/getOrderData")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<OrderResult> getOrderDetail(@ApiParam("订单编号") @RequestParam(value = "orderNumber",required = false)String orderNumber){
        OrderResult orderResult = chargingOrderService.getOrderDetail(orderNumber);
        return Response.ok(orderResult);
    }

    @ApiOperation("订单-退款")
    @GetMapping("/refundOrder")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<String> refundOrder(@ApiParam("订单id") @RequestParam(value = "orderId")Integer orderId,@ApiParam("退款金额") @RequestParam(value = "refundAmount") BigDecimal refundAmount){
        return chargingOrderService.refundOrder(orderId,getUserInfo(),refundAmount);
    }


    /***
     * 订单记录可以更改这条记录对应的端口号和充电桩号
     * 场景是当某个充电桩扫描以后发现该充电桩出现故障呢，此时运营人员手动切换端口和桩号，有一定风险指数
     * 1.更改订单 端口和桩号
     * 2.将原订单的端口改成故障状态F
     * 3.将下发表数据更新成重新下发状态，更新新的桩号和端口
     */
    @ApiOperation("订单-端口变更")
    @GetMapping("/portChange")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<String> portChange(@ApiParam("订单id") @RequestParam(value = "orderId")String orderId,@ApiParam("桩号") @RequestParam(value = "parkId") String parkId,@ApiParam("端口编号") @RequestParam(value = "portCode") String portCode){
        return chargingOrderService.portChange(orderId,getUserInfo(),portCode,parkId);
    }

}
