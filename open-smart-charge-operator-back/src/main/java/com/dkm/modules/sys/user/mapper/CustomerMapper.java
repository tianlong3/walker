package com.dkm.modules.sys.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.sys.user.model.Customer;
import org.springframework.stereotype.Repository;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2019-12-17 10:01
 */
@Repository
public interface CustomerMapper extends BaseMapper<Customer> {
}
