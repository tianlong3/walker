package com.dkm.modules.sys.operator.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description 代理商分成记录表
 * @author guoliangbo;
 * @Date 2020/1/7 17:12
 * @Version 1.0
 */
@Data
public class OperatorRoyaltyRecord {

    private Integer id;

    private String orderId;

    private Integer operatorId;

    private String chargePileId;

    private BigDecimal orderAmount;

    private BigDecimal royaltyPercent;

    private BigDecimal royaltyAmount;

    private String createTime;
}
