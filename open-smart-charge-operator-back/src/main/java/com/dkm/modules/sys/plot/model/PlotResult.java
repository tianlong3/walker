package com.dkm.modules.sys.plot.model;

import com.dkm.modules.wx.city.model.Plot;
import com.dkm.modules.wx.city.model.PlotModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @描述 小区返回前台数据
 * @创建时间 2019/10/26 23:33
 * @创建人 yangbin
 */
@Data
@ApiModel("小区数据")
public class PlotResult extends PlotModel {

    @ApiModelProperty("所属地区名称")
    private String regionName;

    @ApiModelProperty("所属省份")
    private String province;

    @ApiModelProperty("所属省份代码")
    private String provinceCode;

    @ApiModelProperty("所属城市")
    private String city;

    @ApiModelProperty("所属城市代码")
    private String cityCode;

}
