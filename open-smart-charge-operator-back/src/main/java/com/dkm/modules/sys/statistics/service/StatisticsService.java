package com.dkm.modules.sys.statistics.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.statistics.model.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @描述 统计接口
 * @创建时间 2019/10/24 14:37
 * @创建人 yangbin
 */
public interface StatisticsService {
    Map<String, Object> getUsersAndSales();

    List<Map<String,Object>> getNewUserCount(Map<String,Object> params);

    List<SalesDetail> getSalesDetailParams(Map<String,Object> params);

    SalesData getSalesData();

    IPage<SalesDetail> getSalesDetail(Page<SalesDetail> page);

    void exportTable(HttpServletResponse httpServletResponse);

    IPage<PilesDetail> getPilesDetail(Page<PilesDetail> page);

    /**
     * 导出设备销售统计
     * @param res
     */
    void exportPilesInfo(HttpServletResponse res);

    /**
      * 功能描述：分页按天查询充值
      * @param page 分页信息
      * @return com.baomidou.mybatisplus.core.metadata.IPage<com.dkm.modules.sys.statistics.model.RechargeDetail> 充值
      * @Author: guoliangbo
      * @Date: 2019/12/26 11:32
      */
    IPage<RechargeDetail> getRechargeDetail(Page<RechargeDetail> page);

    /**
      * 功能描述：查询每日充值明细
      * @param page 分页信息
      * @param params 查询参数
      * @return com.baomidou.mybatisplus.core.metadata.IPage<com.dkm.modules.sys.statistics.model.DayRecharge> 结果
      * @Author: guoliangbo
      * @Date: 2019/12/26 15:53
      */
    IPage<DayRecharge> getDayRecharge(Page<DayRecharge> page, Map<String, String> params);
}
