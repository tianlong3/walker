package com.dkm.modules.sys.advert.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.advert.model.Advert;

import java.util.Map;

/**
 * @描述
 * @创建时间 2019/10/10 17:16
 * @创建人 yangbin
 */
public interface AdvertService {
    IPage<Advert> getAdvertList(Map<String, Object> params, Page<Advert> page);

    void addAdvert(Advert advert);

    void delAdvert(Integer id);

    void updateAdvert(Advert advert);
}
