package com.dkm.modules.sys.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.sys.operator.model.UserDatascopePO;
import org.springframework.stereotype.Repository;

/**
 * @描述
 * @创建时间 2019/10/28 20:10
 * @创建人 yangbin
 */
@Repository
public interface UserDatascopeMapper extends BaseMapper<UserDatascopePO> {
}
