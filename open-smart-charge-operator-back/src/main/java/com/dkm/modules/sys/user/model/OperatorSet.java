package com.dkm.modules.sys.user.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @描述  用户分配代理商数据
 * @创建时间 2019/10/31 16:11
 * @创建人 yangbin
 */
@Data
public class OperatorSet {

    @ApiModelProperty(value = "运营人员ID",required = true)
    @NotBlank(message = "运营人员ID不能为空")
    private Integer userId;

    @ApiModelProperty(value = "代理商数据  id 数组")
    private Integer [] operatorsId;
}
