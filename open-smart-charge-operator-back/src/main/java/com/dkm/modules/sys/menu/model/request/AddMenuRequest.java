package com.dkm.modules.sys.menu.model.request;

import com.dkm.modules.sys.menu.model.MenuPO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2020-01-03 11:03
 */
@Data
@ApiModel("新增菜单入参")
public class AddMenuRequest implements Serializable {

    @ApiModelProperty(value = "主键")
    private Integer id;

    @NotBlank(message = "菜单名称必填")
    @ApiModelProperty(value = "菜单名称", required = true)
    private String menuName;

    @ApiModelProperty(value = "父级菜单code", required = true)
    private Integer parentCode;

    @NotNull(message = "序号必填")
    @ApiModelProperty(value = "序号", required = true)
    private Integer sequence;

    @NotNull(message = "菜单类型必填")
    @ApiModelProperty(value = "菜单类型", required = true)
    private Integer menuType;

    @ApiModelProperty(value = "后台请求地址", required = false)
    private String requestUrl;

    @ApiModelProperty(value = "前台路由地址", required = false)
    private String frontendUrl;

    public MenuPO convertPO() {
        return MenuPO.builder()
                .id(this.id)
                .menuName(this.menuName)
                .parentCode(this.parentCode)
                .sequence(this.sequence)
                .menuType(this.menuType)
                .requestUrl(this.requestUrl)
                .frontendUrl(this.frontendUrl)
                .build();
    }
}
