package com.dkm.modules.sys.finance.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.PageQueryParams;
import com.dkm.commons.Response;
import com.dkm.modules.sys.finance.model.FinanceResult;
import com.dkm.modules.sys.finance.service.FinanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @描述 财务管理控制器
 * @创建时间 2019/10/20 16:20
 * @创建人 yangbin
 */
@Api(tags = "A12 财务管理")
@RequestMapping("/finance")
@Controller
public class FinanceController extends BaseController {
    private Logger logger = LoggerFactory.getLogger(FinanceController.class);

    @Autowired
    private FinanceService financeService;

    @ApiOperation("财务数据列表")
    @GetMapping("getFinanceList")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    @ResponseBody
    public Response<IPage<FinanceResult>> getFinanceList(PageQueryParams p,
                                                         @ApiParam("设备编号或分销商手机号")@RequestParam(value = "keyWord",required = false)String keyWord,
                                                         @ApiParam("统计方式 1：分销商 2：充电桩")@RequestParam(value = "type",defaultValue = "1") Integer type){
        logger.info("财务数据列表");
        Page<FinanceResult> page = new Page<>(p.getPageNo(),p.getPageSize());
        Map<String,Object> params = new HashMap<>();
        params.put("keyWord",keyWord);
        params.put("type",type);
        IPage<FinanceResult> page1 = financeService.getFinanceList(params,page);
        return Response.ok(page1);
    }

    @ApiOperation(value = "查看总销售额",notes = "返回数据   daySales：日销售额，monSales:月销售额，ySales:年销售额")
    @GetMapping("/getSumFinace")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    @ResponseBody
    public Response getSumFinace(){
        logger.info("查看总销售额");
        Map<String,Object> data = financeService.getSumFinace();
        return Response.ok(data);
    }

    @ApiOperation("导出Excel")
    @GetMapping("export")
    @ResponseBody
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public void ExportExcel(@ApiParam("设备编号或分销商手机号")@RequestParam(value = "keyWord",required = false)String keyWord,
                            @ApiParam("统计方式 1：分销商 2：充电桩")@RequestParam(value = "type",defaultValue = "1") Integer type){
        Map<String,Object> params = new HashMap<>();
        params.put("keyWord",keyWord);
        params.put("type",type);
        financeService.ExportExcel(params,getHttpServletResponse());
    }
}
