package com.dkm.modules.sys.operator.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2020-01-17 16:07
 */
@Data
@ApiModel("运营人员数据权限")
public class UserOperator {

    @ApiModelProperty("关联关系id")
    private String id;

    @ApiModelProperty("运营者姓名")
    private String userName;

    @ApiModelProperty("运营者手机号")
    private String userMobile;

    @ApiModelProperty("代理商姓名")
    private String operatorName;

    @ApiModelProperty("代理商手机号")
    private String operatorMobile;

}
