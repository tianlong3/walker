package com.dkm.modules.sys.map.controller;

import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.modules.sys.map.model.PlotPile;
import com.dkm.modules.wx.pile.service.PileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @描述 首页地图控制器
 * @创建时间 2019/10/20 23:10
 * @创建人 yangbin
 */
@Controller
@RequestMapping("index")
public class MapController extends BaseController {
    @Autowired
    private PileService pileService;


    @GetMapping("")
    public String index(){
        return "map/index.html";
    }


    /**
     *@描述   获取小区充电桩数据
     *@参数  []
     *@返回值  com.dkm.commons.Response<java.util.List<com.dkm.modules.sys.map.model.PlotPile>>
     *@创建人  yangbin
     *@创建时间  2019/10/20
     */
    @GetMapping("getPlotPile")
    @ResponseBody
    public Response<List<PlotPile>> getPlotPile(){
        List<PlotPile> plotPiles =pileService.getPlotPile();
        return Response.ok(plotPiles);
    }

}
