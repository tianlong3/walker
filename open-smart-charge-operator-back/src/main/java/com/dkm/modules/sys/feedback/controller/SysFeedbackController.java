        
package com.dkm.modules.sys.feedback.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.modules.wx.feedback.model.FeedbackResult;
import com.dkm.modules.wx.feedback.model.QueryFeedBack;
import com.dkm.modules.wx.feedback.service.FeedbackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName FeedbackController
 * @Description: 问题反馈 控制器
 * @Author yangbin
 * @Date 2019-09-13 19:40
 * @Version V1.0
 **/
@Api(tags = "A10 问题反馈")
@RestController
@RequestMapping("/sysFeedback")
public class SysFeedbackController extends BaseController{

    private final static Logger logger = LoggerFactory.getLogger(SysFeedbackController.class);
    @Autowired
    private FeedbackService feedbackService;


    @ApiOperation(value="获取问题反馈列表")
    @GetMapping("/getFeedBackList")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<IPage<FeedbackResult>> getFeedBackList(@ApiParam("分销商ID")@RequestParam(value = "userId",required = false)String userId,
                                                           @ApiParam("搜索关键字")@RequestParam(value = "keyWord",required = false)String keyWord,
                                                           @ApiParam("页数") @RequestParam("pageNo")int pageNo,
                                                           @ApiParam("每页显示数") @RequestParam("pageSize")int pageSize){
        logger.info("获取问题反馈列表");
        Page<FeedbackResult> page = new Page<>(pageNo,pageSize);
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        params.put("keyWord",keyWord);
        params.put("FeedBack",new QueryFeedBack());
        IPage<FeedbackResult> FeedBackList = feedbackService.getFeedBackList(params,page);
        return Response.ok(FeedBackList);

    }
    @ApiOperation("处理问题")
    @GetMapping("/doFeedBack")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response doFeedBack(@ApiParam("问题ID") @RequestParam("id") String id,
                               @ApiParam("批注") @RequestParam("comment") String comment,
                               @ApiParam("处理状态 0：待处理，1：处理完成 2: 忽略") @RequestParam("state") int state){
        logger.info("运营端-处理问题反馈");
        return feedbackService.doFeedBack(id,comment,getUserId(),state);
    }

    @ApiOperation("获取问题反馈信息")
    @PostMapping("/getFeedBack")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response getFeedBack(@ApiParam("问题ID") @RequestParam("id") String id){
        logger.info("获取问题反馈信息");
        Map<String,Object> data = feedbackService.getFeedBack(id);
        return Response.ok(data);
    }

}
