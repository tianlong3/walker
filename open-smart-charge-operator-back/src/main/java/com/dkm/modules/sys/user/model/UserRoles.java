package com.dkm.modules.sys.user.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @描述 配置角色数据
 * @创建时间 2019/10/23 8:53
 * @创建人 yangbin
 */
@Data
public class UserRoles {

    @ApiModelProperty("用户ID")
    private Integer id;

    @ApiModelProperty(value = "配置的角色code")
    private List<Integer> roleCodes;
}
