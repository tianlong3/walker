
package com.dkm.modules.sys.role.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.role.model.RolePO;
import com.dkm.modules.sys.role.model.request.AddRoleRequest;
import com.dkm.modules.sys.role.model.request.EditRoleRequest;
import com.dkm.modules.sys.role.model.request.RoleMenuRequest;
import com.dkm.modules.sys.user.model.UserRoles;

import java.util.List;


public interface RoleService {


    void addRole(AddRoleRequest addRole);

    void editRole(EditRoleRequest editRole);

    void delRole(Integer id);

    IPage<RolePO> getRoleList(String keyWord, Page<RolePO> page);

    /**
     * 获取用户的角色信息
     *
     * @param userId
     * @return
     */
    List<RolePO> getRoleSelect(Integer userId);

    /**
     * 保存角色菜单(更新或新增)
     *
     * @param roleMenus
     */
    void saveRoleMenus(RoleMenuRequest roleMenus);

    /**
     * 获取所有角色
     *
     * @return
     */
    List<RolePO> getAllRole();

    /**
     * 获取用户的所有角色
     *
     * @return
     */
    List<RolePO> getRoleByUserId(Integer userId);

    /**
     * 配置用户的角色
     *
     * @param roleResults
     */
    void updateUserRole(UserRoles roleResults);


}