package com.dkm.modules.sys.user.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author guoliangbo;
 * @Description 注册用户余额表
 * @Date 2019/12/12 16:19
 * @Version 1.0
 */
@Data
@TableName("c_user_balance")
public class CustomerBalance {

    @ApiModelProperty(value = "表id")
    @TableId(value = "id")
    private Integer id;

    @ApiModelProperty(value = "微信id")
    @TableField(value = "open_id")
    private String openId;

    @ApiModelProperty(value = "金额")
    @TableField(value = "amount")
    private BigDecimal amount;

    @ApiModelProperty(value = "版本")
    @TableField(value = "version")
    private Integer version;

    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time")
    private String updateTime;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time")
    private String createTime;

}
