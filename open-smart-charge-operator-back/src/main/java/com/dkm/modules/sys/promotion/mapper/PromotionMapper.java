package com.dkm.modules.sys.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.promotion.model.Promotion;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @描述 促销管理Mapper
 * @创建时间 2019/10/12 21:19
 * @创建人 yangbin
 */
@Repository
public interface PromotionMapper extends BaseMapper<Promotion> {
    List<Promotion> getPromotionList(@Param("p") Map<String, Object> params, Page<Promotion> promotionPage);

    /**
      * 功能描述：根据满足金额查询促销规则
      * @param totalAmount 满足金额
      * @Return:com.dkm.modules.sys.promotion.model.Promotion 促销
      * @Author: Guo Liangbo
      * @Date:2019/11/10 16:54
      */
    Promotion selectByTotalAmount(Double totalAmount);

    Integer updateDeleteStatus(Promotion promotion);
}
