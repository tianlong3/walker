
package com.dkm.modules.sys.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.Response;
import com.dkm.modules.sys.operator.model.OperatorSelect;
import com.dkm.modules.sys.user.model.*;
import com.dkm.modules.wx.card.model.RechargeRecodPO;
import com.dkm.modules.wx.login.model.LoginForm;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * @ClassName User
 * @Description:
 * @Author yangbin
 * @Date 2019-09-18 00:01
 * @Version V1.0
 **/
public interface UserService {


    /**
     * @MethodName:
     * @Description: 系统用户登录操作
     * @Param:
     * @Return:
     * @Author: yangbin
     * @Date: 2019/9/19
     **/
    Response signIn(LoginForm form, HttpSession session, Integer type);

    /**
     * 添加用户
     *
     * @param user 账户信息
     * @return 返回影响行数
     */
    int addUser(AddUser user);

    /**
     * 修改密码
     *
     * @param password    原密码
     * @param newPassword 新密码
     * @param userId      用户id
     * @return 返回结果
     */
    Response changePwd(String password, String newPassword, String userId);

    /**
     * 通过用户账号查用户
     *
     * @param userNm 用户账号
     * @return 用户信息
     */
    User getUserById(String userNm);

    List<OperatorSelect> getUserScope(String userNm);

    void delUser(Integer id);

    void updateUser(EditUser user);

    IPage<UserResult> getUserList(Map<String, Object> params, Page<UserResult> page);

    void setUserRole(UserRoles roleResults);

    /**
     * @描述 绑定微信
     * @参数 [userId 用户ID, code 微信code]
     * @返回值 void
     * @创建人 yangbin
     * @创建时间 2019/10/25
     */
    Response bindWx(String userId, String code);

    void setOperator(OperatorSet operatorSet);

    void updateUserByNm(EditUser user);

    List getOperatorSelect();

    /**
     * 功能描述：根据用户id，重置密码
     *
     * @param userId     用户id
     * @param operatorId 操作人id
     * @Return:com.dkm.commons.Response
     * @Author: Guo Liangbo
     * @Date:2019/11/16 15:11
     */
    Response resetPassword(Integer userId, Integer operatorId);

    /**
     * 绑定注册用户的手机号
     *
     * @param userId
     * @param mobile
     */
    void bindMobile(Integer userId, String mobile);

    /**
     * 用户微信退款
     *
     * @param recodPO
     * @param amount  金额,传正数,单位为元
     * @param batchId
     * @param user
     */
    void wxRefundAmount(RechargeRecodPO recodPO, BigDecimal amount, String batchId, Integer changeType, User user);

    /**
     * 退款申请结果
     *
     * @param xmlData
     */
    void refundResult(String xmlData);

    /**
     * 赠送退款
     *
     * @param recodPO
     * @param refundAmount 单位元
     * @param batchId
     * @param changeType
     * @param userInfo
     */
    void giveRefundAmount(RechargeRecodPO recodPO, BigDecimal refundAmount, String batchId, Integer changeType, User userInfo);
}