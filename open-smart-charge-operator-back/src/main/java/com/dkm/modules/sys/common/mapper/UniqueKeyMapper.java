package com.dkm.modules.sys.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.sys.common.model.UniqueKeyPO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Repository;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2020-01-02 21:46
 */
@Repository
interface UniqueKeyMapper extends BaseMapper<UniqueKeyPO> {

    @Insert("insert into c_unique_key () values ()")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertOne(UniqueKeyPO uniqueKeyPO);

}
