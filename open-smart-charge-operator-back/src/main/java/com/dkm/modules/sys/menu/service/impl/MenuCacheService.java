package com.dkm.modules.sys.menu.service.impl;

import com.alibaba.fastjson.JSON;
import com.dkm.constant.CacheConstant;
import com.dkm.modules.sys.menu.model.MenuPO;
import com.dkm.util.StringUtils;
import com.dkm.util.redis.RedisUtils;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2020-01-10 21:56
 */
@Component
public class MenuCacheService {

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 根据角色code 获取所有角色的菜单和按钮
     *
     * @param roleCode
     * @return
     */
    public List<MenuPO> getMenuByRuleCodeFromCache(Integer roleCode) {
        String s = redisUtils.get(CacheConstant.Menu.PREFIX_ROLE + roleCode);
        if (StringUtils.isBlank(s)) {
            return Lists.newArrayList();
        }
        return JSON.parseArray(s, MenuPO.class).stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 添加角色 的按钮和菜单
     *
     * @param roleCode
     * @return
     */
    public void addMenuByRuleCodeFromCache(Integer roleCode, List<MenuPO> menus) {
        if (Objects.isNull(roleCode) || CollectionUtils.isEmpty(menus)) {
            return;
        }
        redisUtils.set(CacheConstant.Menu.PREFIX_ROLE + roleCode, JSON.toJSONString(menus));
    }

    /**
     * 缓存用户的菜单权限
     *
     * @param userId
     * @param menus
     */
    public void addMenuByUserIdFromCache(Integer userId, List<MenuPO> menus) {
        if (Objects.isNull(userId) || CollectionUtils.isEmpty(menus)) {
            return;
        }
        redisUtils.set(CacheConstant.Menu.PREFIX_MENU_USER + userId, JSON.toJSONString(menus));
    }

    /**
     * 获取用户的菜单权限
     *
     * @param userId
     * @return
     */
    public List<MenuPO> getMenuByUserIdFromCache(Integer userId) {
        if (Objects.isNull(userId)) {
            return Lists.newArrayList();
        }

        String s = redisUtils.get(CacheConstant.Menu.PREFIX_MENU_USER + userId);
        return StringUtils.isBlank(s) ? Lists.newArrayList() : JSON.parseArray(s, MenuPO.class).stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 缓存用户所有的按钮
     *
     * @param userId
     * @param menus
     */
    public void addBtnByUserIdFromCache(Integer userId, List<MenuPO> menus) {
        if (Objects.isNull(userId) || CollectionUtils.isEmpty(menus)) {
            return;
        }
        redisUtils.set(CacheConstant.Menu.PREFIX_BUTTON_USER + userId, JSON.toJSONString(menus));
    }

    /**
     * 获取用户的所有按钮权限
     *
     * @param userId
     * @return
     */
    public List<MenuPO> getBtnByUserIdFromCache(Integer userId) {
        if (Objects.isNull(userId)) {
            return Lists.newArrayList();
        }

        String s = redisUtils.get(CacheConstant.Menu.PREFIX_BUTTON_USER + userId);
        return StringUtils.isBlank(s) ? Lists.newArrayList() : JSON.parseArray(s, MenuPO.class).stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 清空所有的菜单和按钮缓存
     */
    public void delAllMenuAndBtnCache() {
        Set<String> keys = redisUtils.getKeys(CacheConstant.Menu.PREFIX);

        if (CollectionUtils.isNotEmpty(keys)) {
            redisUtils.delete(keys);
        }
    }

    /**
     * 缓存所有的菜单和按钮
     *
     * @param menuPOS
     */
    public void addAllMenuAndBtnCache(List<MenuPO> menuPOS) {
        if (CollectionUtils.isEmpty(menuPOS)) {
            return;
        }
        redisUtils.set(CacheConstant.Menu.ALL, JSON.toJSONString(menuPOS));
    }

    /**
     * 获取所有的菜单和按钮缓存
     *
     * @return
     */
    public List<MenuPO> getAllMenuAndBtnFromCahce() {
        String s = redisUtils.get(CacheConstant.Menu.ALL);
        if (StringUtils.isBlank(s)) {
            return Lists.newArrayList();
        }
        return JSON.parseArray(s, MenuPO.class).stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

}
