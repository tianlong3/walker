package com.dkm.modules.sys.role.model.request;

import com.dkm.modules.sys.role.model.RolePO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2020-01-02 21:26
 */
@Data
@ApiModel(description = "角色编辑实体")
public class EditRoleRequest implements Serializable {

    @NotNull(message = "角色主键必填")
    @ApiModelProperty(value = "主键必填", required = true)
    private Integer id;

    @NotBlank(message = "角色名称必传")
    @ApiModelProperty(value = "角色名称", required = true)
    private String roleName;

    @NotBlank(message = "角色描述必传")
    @ApiModelProperty(value = "角色名称", required = true)
    private String description;

    //@NotBlank(message = "角色类型毕传")
    //@ApiModelProperty(value = "角色类型 0=运营端角色  1=代理商端角色", required = true)
    private Integer roleType;

    //@ApiModelProperty(value = "代理商数据权限,角色类型为代理商角色时,必填", required = false)
    private String dataScope;

    public RolePO converToPO() {
        return RolePO.builder()
                .id(0 == this.id ? null : this.id)
                .roleName(this.roleName)
                .description(this.description)
                .roleType(roleType)
                .dataScope(this.dataScope == null ? "" : this.dataScope)
                .build();
    }
}
