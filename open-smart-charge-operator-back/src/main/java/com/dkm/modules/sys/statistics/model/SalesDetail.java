package com.dkm.modules.sys.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @描述 销售统计每日详情数据
 * @创建时间 2019/10/24 16:44
 * @创建人 yangbin
 */
@Data
public class SalesDetail extends SalesData{

    @ApiModelProperty("订单时间")
    private String createTime;

    @ApiModelProperty("有效订单数")
    private Integer validCount;
}
