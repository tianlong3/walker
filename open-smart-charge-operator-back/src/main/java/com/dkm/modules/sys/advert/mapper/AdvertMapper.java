package com.dkm.modules.sys.advert.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.advert.model.Advert;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @描述 广告Mapper
 * @创建时间 2019/10/10 17:11
 * @创建人 yangbin
 */
@Repository
public interface AdvertMapper extends BaseMapper<Advert> {
    List<Advert> getAdvertList(@Param("params") Map<String, Object> params, Page<Advert> page);
}
