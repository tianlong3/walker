package com.dkm.modules.sys.statistics.model;

import com.dkm.util.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @描述 销售数据
 * @创建时间 2019/10/24 16:23
 * @创建人 yangbin
 */
@Data
public class SalesData {

    @ApiModelProperty("订单数")
    private Integer sumCount;

    @ApiModelProperty("退款订单数")
    private Integer reCount;

    @ApiModelProperty("销售总额")
    private Float sumSales;

    @ApiModelProperty("退款总额")
    private Float reSales;

    @ApiModelProperty(value = "总耗电量", hidden = true)
    private String totalPowerConsumption;

    public String getTotalPowerConsumption() {
        if (StringUtils.isBlank(totalPowerConsumption)) {
            return StringUtils.formatMoney(0);
        } else {
            return StringUtils.formatMoney(Double.valueOf(totalPowerConsumption));
        }
    }
}
