package com.dkm.modules.sys.pile.model;

import lombok.Data;

/**
 * @Description 下发给设备的记录
 * @Author Guo Liangbo
 * @Date 2019/11/17 21:55
 * @Version 1.0
 */
@Data
public class DeviceState {

    private String deviceId;

    private String devicePortNum;

    private String switchS;

    private String num = "100";

    private String state = "1";

    private String orderId = "10000000";

    private String execTime;

    private String createTime;

}
