package com.dkm.modules.sys.promotion.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @描述 促销添加实体
 * @创建时间 2019/10/12 13:43
 * @创建人 yangbin
 */
@Data
public class AddPromotion {

    @ApiModelProperty("促销名称")
    private String name;

    @ApiModelProperty("满足金额")
    private Double totalAmount;


    @ApiModelProperty("赠送金额")
    private Double giftAmount;

    @ApiModelProperty("是否前端显示 默认0不显示，1显示")
    private String showStatus;

    @ApiModelProperty("数据状态 默认0  0:正常 1：禁用")
    private String dataStatus;

    @ApiModelProperty(value = "删除状态 默认0  0:正常 1：删除",hidden = true)
    private Integer deleteFlag;
}
