package com.dkm.modules.sys.operator.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2020-01-17 15:02
 */
@Data
@ApiModel("给代理商分配运营者")
public class SetOperatorDTO {

    @ApiModelProperty("代理商id")
    @NotNull(message = "代理商id必传")
    @Size(min = 1, message = "代理商id必传")
    List<Integer> operatorIds;

    @ApiModelProperty("运营人员id")
    @NotNull(message = "运营人员id必传")
    @Min(value = 0, message = "运营人员id不能为0")
    Integer userId;
}
