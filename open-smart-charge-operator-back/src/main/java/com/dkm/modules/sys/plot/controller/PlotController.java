package com.dkm.modules.sys.plot.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.PageQueryParams;
import com.dkm.commons.Response;
import com.dkm.modules.sys.pile.controller.SysPileController;
import com.dkm.modules.sys.plot.model.PlotResult;
import com.dkm.modules.sys.plot.service.PlotService;
import com.dkm.modules.wx.city.model.PlotModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @描述 小区管理--控制器
 * @创建时间 2019/10/26 23:14
 * @创建人 yangbin
 */
@Api(tags = "A14 小区管理")
@RequestMapping("/plot")
@RestController
public class PlotController extends BaseController {
    private final static Logger logger = LoggerFactory.getLogger(PlotController.class);

    @Autowired
    private PlotService plotService;

    @ApiOperation("添加小区")
    @PostMapping("/addPlot")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response addPlot(@ApiParam("小区数据：id,createTime 去除不传")@RequestBody PlotModel plotModel){
        logger.info("添加小区");
        plotService.addPlot(plotModel);
        return Response.ok();

    }

    @ApiOperation("修改小区")
    @PostMapping("/editPlot")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response editPlot(@ApiParam("小区数据：createTime 去除不传")@RequestBody  PlotModel plotModel){
        logger.info("修改小区");
        plotService.editPlot(plotModel);
        return Response.ok();
    }

    @ApiOperation(("删除小区"))
    @DeleteMapping("/delPlot")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response delPlot(@ApiParam("小区ID")@RequestParam("id") Integer id){
        logger.info("删除小区："+id);
        plotService.delPlot(id);
        return Response.ok();
    }

    @ApiOperation("获取小区列表")
    @GetMapping("/getPlotList")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<IPage<PlotResult>> getPlotList(PageQueryParams p,
                                                   @ApiParam("搜索关键字")@RequestParam(value = "keyWord",required = false)String keyWord){
        logger.info("获取小区列表数据");
        Page<PlotResult> page = new Page<>(p.getPageNo(),p.getPageSize());
        Map<String,Object>  params = new HashMap<>();
        params.put("keyWord",keyWord);
        IPage<PlotResult> plotList = plotService.getPlotList(params,page);
        return Response.ok(plotList);
    }
}
