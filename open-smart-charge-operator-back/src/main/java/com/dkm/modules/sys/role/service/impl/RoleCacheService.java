package com.dkm.modules.sys.role.service.impl;

import com.alibaba.fastjson.JSON;
import com.dkm.constant.CacheConstant;
import com.dkm.exception.ServiceException;
import com.dkm.modules.sys.role.model.RolePO;
import com.dkm.util.redis.RedisUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2020-01-10 22:05
 */
@Component
public class RoleCacheService {

    @Autowired
    RedisUtils redisUtils;


    /**
     * 获取所有角色数据
     *
     * @return
     */
    public List<RolePO> getAllRoleFromCache() {
        String json = redisUtils.get(CacheConstant.Role.ALL);
        return Strings.isBlank(json) ? null : JSON.parseArray(json, RolePO.class).stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 获取用户的角色数据
     *
     * @param userId
     * @return
     */
    public List<RolePO> getRoleByUserIdFromCache(Integer userId) {
        if (Objects.isNull(userId)) {
            throw new ServiceException("查询用户角色,用户id不能为空");
        }
        String json = redisUtils.get(CacheConstant.Role.PREFIX_USER + userId);
        return Strings.isBlank(json) ? null : JSON.parseArray(json, RolePO.class).stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 清除用户的角色缓存
     *
     * @param userId
     */
    public void delUserRoleCache(Integer userId) {
        if (Objects.isNull(userId)) {
            throw new ServiceException("删除用户角色,用户id不能为空");
        }
        redisUtils.delete(CacheConstant.Role.PREFIX_USER + userId);
    }

    /**
     * 缓存用户角色
     *
     * @param userId
     * @param roles
     */
    public void addUserRoleCache(Integer userId, List<RolePO> roles) {
        if (Objects.isNull(userId)) {
            throw new ServiceException("新增用户角色,用户id不能为空");
        }
        redisUtils.set(CacheConstant.Role.PREFIX_USER + userId, JSON.toJSONString(roles));
    }

    /**
     * 缓存所有角色
     *
     * @param rolePos
     */
    public void addAllRoleCache(List<RolePO> rolePos) {
        redisUtils.set(CacheConstant.Role.ALL, JSON.toJSONString(rolePos));
    }

    /**
     * 清空角色缓存
     */
    public void delAllRoleCache() {
        Set<String> keys = redisUtils.getKeys(CacheConstant.Role.PREFIX);
        if (CollectionUtils.isNotEmpty(keys)) {
            redisUtils.delete(keys);
        }
    }
}
