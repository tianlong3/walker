
package com.dkm.modules.sys.menu.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.sys.menu.model.MenuPO;
import org.springframework.stereotype.Repository;


/**
 * @ClassName MenuMapper
 * @Description:
 * @Author yangbin
 * @Date 2019-09-17 23:42
 * @Version V1.0
 **/
@Repository
public interface MenuMapper extends BaseMapper<MenuPO> {

}

