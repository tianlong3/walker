package com.dkm.modules.sys.pile.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.modules.sys.pile.model.SysPileResult;
import com.dkm.modules.wx.pile.model.ChargingPile;
import com.dkm.modules.wx.pile.service.PileService;
import com.dkm.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName ChargingPileController
 * @Description:
 * @Author 杨膑
 * @Date 2019/9/5
 * @Version V1.0
 **/


@RestController
@RequestMapping("/chargingpile")
@Api(value = "设备管理 ", tags = {"A3 充电桩管理"})
public class SysPileController extends BaseController {

    private final static Logger logger = LoggerFactory.getLogger(SysPileController.class);
    @Autowired
    private PileService chargingPileService;


    @ApiOperation(value = "获取充电桩分页列表")
    @GetMapping("/getChargingPileList")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<IPage<SysPileResult>> getChargingPileList(@ApiParam("搜索关键字") @RequestParam(value = "keyWord", required = false) String keyWord,
                                                              @ApiParam("分销商") @RequestParam(value = "userId", required = false) String userId,
                                                              @ApiParam("页数") @RequestParam("pageNo") int pageNo,
                                                              @ApiParam("每页显示数") @RequestParam("pageSize") int pageSize) {
        logger.info("获取充电桩分页列表");
        Map<String, String> params = new HashMap<>();
        //设备编号
        params.put("keyWord", keyWord);
        //分销商id
        params.put("userId", userId);
        Page<SysPileResult> page = new Page<>(pageNo, pageSize);
        IPage<SysPileResult> pileList = chargingPileService.getPage(params, page);
        return Response.ok(pileList);
    }

    @ApiOperation(value = "充电桩添加")
    @PostMapping("/addPile")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response add(@Valid @ApiParam("充电桩数据") @RequestBody ChargingPile pile) {
        logger.info("添加充电桩");

        if (!StringUtils.isNumeric(pile.getId()) || pile.getId().length() != 8) {
            return Response.error(-1, "充电桩编号需为8位数字，请检查！");
        }
        ChargingPile validPile = chargingPileService.getPile(Integer.parseInt(pile.getId()));
        if (validPile != null) {
            return Response.error(-1, "充电桩编号重复，请检查！");
        }
       /* Integer plotCode = pile.getPlot();
        if (null == plotCode) {
            return Response.error(-2, "小区信息未传递！");
        }*/

        chargingPileService.add(pile);
        return Response.ok();
    }

    @ApiOperation(value = "充电桩编辑")
    @PostMapping("/editPile")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response edit(@ApiParam("充电桩数据") @RequestBody ChargingPile pile) {
        logger.info("修改充电桩，充电桩编号:" + pile.getId());
        chargingPileService.edit(pile);
        return Response.ok();
    }

    @ApiOperation("统计")
    @GetMapping("/statisticalPile")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response statisticalPile(@ApiParam("充电桩ID") @RequestParam Integer id,
                                    @ApiParam("开始时间 Y-m-d") @RequestParam(value = "startTime", required = false) String startTime,
                                    @ApiParam("结束时间 Y-m-d") @RequestParam(value = "endTime", required = false) String endTime) {
        logger.info("充电桩统计，编号：" + id);
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("startTime", startTime);
        params.put("endTime", endTime);
        List<Map<String, Object>> datas = chargingPileService.statisticalPile(params);
        return Response.ok(datas);

    }

    @ApiOperation("excel导出")
    @GetMapping("/export")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public void ExportExcel(@ApiParam("搜索关键字") @RequestParam(value = "keyWord", required = false) String keyWord,
                            @ApiParam("分销商") @RequestParam(value = "userId", required = false) String userId) {

        logger.info("充电桩excel导出");
        Map<String, String> params = new HashMap<>();
        params.put("keyWord", keyWord);
        /*分销商*/
        params.put("userId", userId);
        chargingPileService.ExportExcel(params, getHttpServletResponse());
    }

    @ApiOperation("运营端-设置充电桩规则")
    @GetMapping("setPileRule")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response setPileRule(@ApiParam("充电桩编号") @RequestParam("Id") String Id,
                                @ApiParam("公众号规则ID") @RequestParam("wxRuleId") String wxRuleId,
                                @ApiParam("卡规则ID") @RequestParam("cardRuleId") String cardRuleId) {


        return chargingPileService.setPileRule(Id, wxRuleId, cardRuleId);

    }


}
