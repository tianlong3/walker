package com.dkm.modules.sys.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * DESCRIPTION : 充值统计
 * @author guolb
 * @create 2019-12-26 20:33
 * @version 1.0
 */
@ApiModel(value = "统计充值信息")
@Data
public class RechargeDetail {

    @ApiModelProperty(value = "充值日期")
    private String rechargeDate;

    @ApiModelProperty(value = "充值金额")
    private String rechargeAmount;

    @ApiModelProperty(value = "退款金额")
    private String refundAmount;

}
