package com.dkm.modules.sys.common.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2020-01-02 21:45
 */
@Data
@TableName("c_unique_key")
public class UniqueKeyPO implements Serializable {
    @TableId
    private Integer id;
}
