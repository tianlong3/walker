package com.dkm.modules.sys.promotion.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.PageQueryParams;
import com.dkm.commons.Response;
import com.dkm.modules.sys.order.controller.SysOrderController;
import com.dkm.modules.sys.promotion.model.AddPromotion;
import com.dkm.modules.sys.promotion.model.EditPromotion;
import com.dkm.modules.sys.promotion.model.Promotion;
import com.dkm.modules.sys.promotion.service.PromotionService;
import com.dkm.modules.sys.user.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @描述 促销管理控制器
 * @创建时间 2019/10/12 21:15
 * @创建人 yangbin
 */
@Api(tags="A13 促销管理")
@RestController
@RequestMapping("promotion")
public class PromotionController extends BaseController {

    private final static Logger logger = LoggerFactory.getLogger(PromotionController.class);

    @Autowired
    private PromotionService promotionService;


    @ApiOperation(value = "新增促销")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    @PostMapping("/addPromotion")
    public Response addPromotion(@ApiParam("促销数据")@RequestBody AddPromotion promotion){
        logger.info("新增促销");

        /*类型转换*/
        String promotionStr= JSONObject.toJSONString(promotion);
        Promotion promotion1 = JSONObject.parseObject(promotionStr,Promotion.class);
        boolean flag = promotionService.checkPromotionByTotalAmount(promotion1);
        if(!flag){
            return Response.error(-2,"已经存在相同金额的促销方案，请修改促销满足金额！");
        }
        /*创建人*/
        promotion1.setCreateBy(getUserId().toString());
        promotionService.addPromotion(promotion1);
        return Response.ok();
    }

    @ApiOperation("删除")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    @DeleteMapping("/delPromotion")
    public Response delPromotion(@ApiParam("促销id")@RequestParam("id") Integer id){
        logger.info("删除促销");
        promotionService.delPromotion(id);
        return Response.ok();
    }

    @ApiOperation(value = "更新")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    @PostMapping("/updatePromotion")
    public Response updatePromotion(@ApiParam("促销数据")@RequestBody EditPromotion promotion){
        logger.info("更新促销信息");

        /*类型转换*/
        String promotionStr= JSONObject.toJSONString(promotion);
        Promotion promotion1 = JSONObject.parseObject(promotionStr,Promotion.class);
        boolean flag = promotionService.checkPromotionByTotalAmount(promotion1);
        if(!flag){
            return Response.error(-2,"已经存在相同金额的促销方案，请修改促销满足金额！");
        }
        /*设置修改人*/
        promotion1.setUpdateBy(getUserId().toString());
        promotionService.updatePromotion(promotion1);
        return Response.ok();
    }


    @ApiOperation(value = "获取促销数据列表")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    @GetMapping("/getPromotionList")
    public Response<IPage<Promotion>> getPromotionList(@ApiParam("状态 0:正常 1：禁用")@RequestParam(value = "dataStatus",required = false) Integer dataStatus,
                                                       @ApiParam("是否展示 0不显示，1显示 ")@RequestParam(value = "showStatus",required = false) Integer showStatus,
                                                       PageQueryParams pageQueryParams){
        logger.info("查询促销信息");
        Map<String,Object> params = new HashMap<>();
        params.put("dataStatus",dataStatus);
        params.put("showStatus",showStatus);
        Page<Promotion> promotionPage = new Page<>(pageQueryParams.getPageNo(),pageQueryParams.getPageSize());
        IPage<Promotion> promotionList = promotionService.getPromotionList(params,promotionPage);
        return Response.ok(promotionList);
    }

}
