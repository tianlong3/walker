package com.dkm.modules.sys.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * DESCRIPTION : 每日充值记录
 * @author guolb
 * @create 2019-12-26 20:33
 * @version 1.0
 */
@ApiModel(value = "每日充值记录信息")
@Data
public class DayRecharge {

    @ApiModelProperty(value = "充值日期")
    private String rechargeDate;

    @ApiModelProperty(value = "用户昵称")
    private String userName;

    @ApiModelProperty(value = "用户手机号")
    private String telephone;

    @ApiModelProperty(value = "金额")
    private String quantity;

    @ApiModelProperty(value = "卡号")
    private String cardNo;

    @ApiModelProperty(value = "类型，充值0 退款4 退款失败5 ")
    private String type;

    @ApiModelProperty(value = "数据类型，1 卡  2 账户 ")
    private String dataType;

    @ApiModelProperty(value = "状态：0 未启用；1正常；2删除")
    private String status;

}
