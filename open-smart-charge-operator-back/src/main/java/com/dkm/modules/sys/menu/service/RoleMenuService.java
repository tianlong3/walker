
package com.dkm.modules.sys.menu.service;

import com.dkm.modules.sys.role.model.request.RoleMenuRequest;


/**
 * @ClassName
 * @Description: 权限菜单接口
 * @Author yangbin
 * @Date 2019-09-17 23:47
 * @Version V1.0
 **/
public interface RoleMenuService {

    void insertOrUpdate(RoleMenuRequest roleMenus);
}