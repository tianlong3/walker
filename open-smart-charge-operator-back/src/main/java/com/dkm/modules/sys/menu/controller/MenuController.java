
package com.dkm.modules.sys.menu.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.PageQueryParams;
import com.dkm.commons.Response;
import com.dkm.exception.ServiceException;
import com.dkm.modules.sys.menu.model.MenuPO;
import com.dkm.modules.sys.menu.model.request.AddMenuRequest;
import com.dkm.modules.sys.menu.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * @ClassName MenuController
 * @Description: 菜单控制器
 * @Author yangbin
 * @Date 2019-09-17 23:42
 * @Version V1.0
 **/
@Api(tags = "A9 运营者管理 --菜单管理")
@RestController
@RequestMapping("/menu")
public class MenuController extends BaseController {

    @Autowired
    private MenuService menuService;


    @ApiOperation("获取登录用户权限菜单(返回树形结构)")
    @GetMapping("/getUserMenu")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<List<MenuPO>> getRoleMenu() {
        List<MenuPO> menuList = menuService.getAllPermissionMenu(getUserId());
        return Response.ok(menuList);
    }

    @ApiOperation("添加菜单")
    @PostMapping("/addMenu")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response addMenu(@Valid @RequestBody AddMenuRequest menu) {
        menuService.addMenu(menu);
        return Response.ok();
    }

    @ApiOperation("修改菜单")
    @PostMapping("/updateMenu")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response updateMenu(@RequestBody AddMenuRequest menu) {
        menuService.updateMenu(menu);
        return Response.ok();
    }

    @ApiOperation("删除菜单")
    @DeleteMapping("/delMenu")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response delMenu(@ApiParam("菜单id") @RequestParam Integer id) {
        if (Objects.isNull(id)) {
            throw new ServiceException("菜单id必填");
        }
        menuService.delMenu(id);
        return Response.ok();
    }

    @ApiOperation("菜单列表")
    @GetMapping("/getMenuList")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<IPage<MenuPO>> getMenuList(@ApiParam("搜索关键字") @RequestParam(value = "keyWord", required = false) String keyWord,
                                              PageQueryParams p) {

        Page<MenuPO> page = new Page<>(p.getPageNo(), p.getPageSize());
        IPage<MenuPO> resultIPage = menuService.getMenuList(keyWord,page);

        return Response.ok(resultIPage);
    }

    @ApiOperation("所有菜单列表,只返回菜单类型,不返回按钮")
    @GetMapping("/getAllMenu")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<List<MenuPO>> getMenuList() {

        List<MenuPO> menuResults = menuService.getAllMenus();

        return Response.ok(menuResults);
    }


    @ApiOperation(value = "获取所有菜单(树形结构)")
    @GetMapping("/getMenuTree")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<List<MenuPO>> getMenuTree() {
        List<MenuPO> menuTree = menuService.getMenuTree();
        return Response.ok(menuTree);
    }

    @ApiOperation(value = "获取角色拥有的菜单,返回menuCode集合")
    @GetMapping("/getMenuCodeByRoleCode")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<List<Integer>> getMenuCodeByRoleCode(@ApiParam("角色code") @RequestParam("roleCode") Integer roleCode) {
        List<Integer> menuCodes = menuService.getMenuAndBtnByRoleCode(roleCode);
        return Response.ok(menuCodes);
    }


}
