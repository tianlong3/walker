
package com.dkm.modules.sys.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.operator.model.OperatorResult;
import com.dkm.modules.sys.operator.model.UserOperator;
import com.dkm.modules.sys.statistics.model.DayRecharge;
import com.dkm.modules.sys.statistics.model.RechargeDetail;
import com.dkm.modules.sys.user.model.User;
import com.dkm.modules.sys.user.model.UserResult;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * @ClassName UserMapper
 * @Description:
 * @Author yangbin
 * @Date 2019-09-18 00:01
 * @Version V1.0
 **/
@Repository
public interface UserMapper extends BaseMapper<User> {

    User getAgentByMobile(String mobile);

    List<OperatorResult> getOperatorList(Page<OperatorResult> page, @Param("p") Map<String, String> params);

    List<UserResult> getUserList(@Param("p") Map<String, Object> params, Page<UserResult> page);

    List<Map<String, Object>> getNewUserCount(Map<String, Object> params);

    /**
     * 功能描述：根据用户id，查询用户
     *
     * @param userId 用户id
     * @Return:com.dkm.modules.sys.user.model.User
     * @Author: Guo Liangbo
     * @Date:2019/11/16 15:28
     */
    User selectByUserId(@Param("userId") Integer userId);

    /**
     * 功能描述：根据电话号码查询登录
     *
     * @param telephone 电话号码
     * @Return:com.dkm.modules.sys.user.model.User
     * @Author: Guo Liangbo
     * @Date:2019/11/16 15:42
     */
    User selectByTelephone(@Param("telephone") String telephone);

    /**
     * 功能描述：分页按天汇总查询充值记录
     *
     * @param page 分页数据
     * @return java.util.List<com.dkm.modules.sys.statistics.model.RechargeDetail> 结果
     * @Author: guoliangbo
     * @Date: 2019/12/26 14:21
     */
    List<RechargeDetail> getRechargeDetail(Page<RechargeDetail> page);

    /**
     * 功能描述：分页按天查询充值记录
     *
     * @param page   分页信息
     * @param params 参数 日期 2019-12-24
     * @return java.util.List<com.dkm.modules.sys.statistics.model.DayRecharge>
     * @Author: guoliangbo
     * @Date: 2019/12/26 15:56
     */
    List<DayRecharge> getDayRecharge(Page<DayRecharge> page, @Param("p") Map<String, String> params);

    /**
     * 分页查询所有运营人员的数据权限
     *
     * @param keyWord
     * @param p
     * @return
     */
    List<UserOperator> selectByPage(@Param("keyWord") String keyWord, Page<UserOperator> p);

    /**
      * 功能描述：根据代理商获取分成金额
      * @param operatorId 代理商id
      * @return java.math.BigDecimal 分成金额
      * @Author: guoliangbo
      * @Date: 2020/1/7 17:30
      */
    BigDecimal getAccumulatedAmount(@Param("operatorId")int operatorId);

    /**
      * 功能描述：设置代理商分成
      * @param operatorId 代理商id
      * @param royaltyPercent 分成比例
      * @return java.lang.Integer 影响行数
      * @Author: guoliangbo
      * @Date: 2020/1/7 17:42
      */
    Integer setUpRoyaltyPercent(@Param("operatorId")int operatorId,@Param("royaltyPercent") BigDecimal royaltyPercent);
}

