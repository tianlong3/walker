package com.dkm.modules.sys.operator.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.dkm.modules.sys.user.model.EditUser;
import com.dkm.util.StringUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName Operator
 * @Description: 代理商信息返回类
 * @Author 杨膑
 * @Date 2019/9/3
 * @Version V1.0
 **/
@Getter
@Setter
@ApiModel(value = "代理商信息")
public class OperatorResult extends EditUser {

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "使用次数")
    private Integer useCount;

    @ApiModelProperty(value = "金额")
    private Float sum;

    @ApiModelProperty(value = "设备数量")
    private Integer pileCount;

    @ApiModelProperty(value = "分成比例")
    private BigDecimal royaltyPercent;

    @ApiModelProperty(value = "总耗电量", hidden = true)
    @TableField(exist = false)
    private String totalPowerConsumption;

    @ApiModelProperty(value = "角色集合")
    @TableField(exist = false)
    private String roleArr;

    public String getTotalPowerConsumption() {
        if(StringUtils.isBlank(totalPowerConsumption)){
            return StringUtils.formatMoney(Double.valueOf("0"));
        }
        return StringUtils.formatMoney(Double.valueOf(totalPowerConsumption));
    }
}
