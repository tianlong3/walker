package com.dkm.modules.sys.statistics.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.PageQueryParams;
import com.dkm.commons.Response;
import com.dkm.modules.sys.statistics.model.*;
import com.dkm.modules.sys.statistics.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @描述 统计--控制器
 * @创建时间 2019/10/24 14:17
 * @创建人 yangbin
 */
@Api(tags = "A2 统计管理")
@Controller
@RequestMapping("/Statistics")
public class StatisticsController extends BaseController {
    @Autowired
    private StatisticsService statisticsService;

    @ApiOperation(value = "首页数据",notes = "usreCount:新增用户数 ,sumSales:销售总额")
    @GetMapping("getUsersAndSales")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    @ResponseBody
    public Response getUsersAndSales(){
        Map<String,Object> data = statisticsService.getUsersAndSales();
        return Response.ok(data);
    }

    @ApiOperation(value = "用户数据--新增用户数",notes = "count:数量，createTime : 时间")
    @GetMapping("getNewUserCount")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    @ResponseBody
    public Response getNewUserCount(@ApiParam("近几天")@RequestParam(value = "day",required = false)Integer day,
                                    @ApiParam("开始时间（yyyy-mm-dd）")@RequestParam(value = "startTime",required = false)String startTime,
                                    @ApiParam("结束时间(yyyy-mm-dd)")@RequestParam(value = "endTime",required = false)String endTime){
        Map<String,Object> params = new HashMap<>();
        params.put("startTime",startTime);
        params.put("endTime",endTime);
        params.put("day",day);
        List<Map<String,Object>> data = statisticsService.getNewUserCount(params);
        return Response.ok(data);
    }

    @ApiOperation("销售统计")
    @GetMapping("getSalesData")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    @ResponseBody
    public Response<SalesData> getSalesData(){
        SalesData salesData = statisticsService.getSalesData();
        return Response.ok(salesData);
    }

    @ApiOperation("设备统计")
    @GetMapping("getPilesInfo")
    @ResponseBody
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<IPage<PilesDetail>> getPilesInfo(PageQueryParams p){
        Page<PilesDetail> page = new Page<>(p.getPageNo(),p.getPageSize());
        IPage<PilesDetail> pages = statisticsService.getPilesDetail(page);
        return Response.ok(pages);
    }

    @ApiOperation("导出设备统计")
    @GetMapping("exportPilesInfo")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    @ResponseBody
    public void getPilesInfo(HttpServletResponse res){
        statisticsService.exportPilesInfo(res);
    }


    @ApiOperation("销售详情")
    @GetMapping("getSalesDetail")
    @ResponseBody
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<IPage<SalesDetail>> getSalesDetail(PageQueryParams p){
        Page<SalesDetail> page = new Page<>(p.getPageNo(),p.getPageSize());
        IPage<SalesDetail> salesDetails = statisticsService.getSalesDetail(page);
        return Response.ok(salesDetails);
    }

    @ApiOperation("下载表格")
    @GetMapping("export")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    @ResponseBody
    public void exportTable(HttpServletResponse res){
        statisticsService.exportTable(res);
    }

    @ApiOperation("充值统计")
    @GetMapping("getRechargeDetail")
    @ResponseBody
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<IPage<RechargeDetail>> getRechargeDetail(PageQueryParams p){
        Page<RechargeDetail> page = new Page<>(p.getPageNo(),p.getPageSize());
        IPage<RechargeDetail> rechargeDetails = statisticsService.getRechargeDetail(page);
        return Response.ok(rechargeDetails);
    }

    @ApiOperation("按日期查询当日充值明细")
    @GetMapping("/getDayRecharge")
    @ResponseBody
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<IPage<DayRecharge>> getDayRecharge(PageQueryParams p,
                                @ApiParam("日期 2019-12-24") @RequestParam("date")String date,@ApiParam("用户昵称或者手机号") @RequestParam(value = "keyWord",required = false)String keyWord) {

        // 查询参数处理
        Map<String, String> params = new HashMap<>(16);
        // 日期
        params.put("date", date);
        params.put("keyWord", keyWord);
        //获取端口分页后列表
        Page<DayRecharge> page = new Page<>(p.getPageNo(),p.getPageSize());
        IPage<DayRecharge> dayRecharges = statisticsService.getDayRecharge(page, params);
        return Response.ok(dayRecharges);
    }

}
