package com.dkm.modules.sys.user.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * DESCRIPTION : 用户,消费群体
 *
 * @author ducf
 * @create 2019-12-17 9:43
 */
@Data
@TableName("t_user")
public class Customer implements Serializable {

    /**
     * 用户表主键id
     */
    @TableId(value = "ID")
    private Integer id;

    /**
     * 角色id
     */
    @TableField(value = "ROLE_ID")
    private BigDecimal roleId;

    /**
     * 用户头像
     */
    @TableField(value = "head_image")
    private String headImage;

    /**
     * 用户昵称
     */
    @TableField(value = "USER_NAME")
    private String userName;

    /**
     * 真实姓名
     */
    @TableField(value = "REAL_NAME")
    private String realName;

    /**
     * 卡号
     */
    @TableField(value = "CARD_ID")
    private String cardId;

    /**
     * 手机号
     */
    @TableField(value = "MOBILE")
    private String mobile;

    /**
     * 部门ID
     */
    @TableField(value = "DEPARTID")
    private String departId;

    /**
     * 用户账户
     */
    @TableField(value = "USERACCOUNT")
    private String userAccount;

    @TableField(value = "USERORDER")
    private BigDecimal userOrder;

    /**
     * 是否有效  默认Y有效  N无效'
     */
    @TableField(value = "ISVALID")
    private String isValid;

    @TableField(value = "SWRY_DM")
    private String swryDm;

    @TableField(value = "CZRY_DM")
    private String czryDm;

    @TableField(value = "CZRY_MC")
    private String czryMc;

    @TableField(value = "PROVINCECODE")
    private String provinceCode;

    @TableField(value = "REGIONCODE")
    private String regionCode;

    /**
     * 微信openID
     */
    @TableField(value = "weixin_openid")
    private String weiXinOpenId;

    @TableField(value = "zd")
    private String zd;

    /**
     * 是否管理员 N不是 Y 是
     */
    @TableField(value = "admin")
    private String admin;

    @TableField(value = "zdmobile")
    private String zdMobile;

    @TableField(value = "zdkssj")
    private String zdkssj;

    @TableField(value = "zdjssj")
    private String zdjssj;

    @TableField(value = "je")
    private String je;
    /**
     * Y审核通过，N审核未通过
     */
    @TableField(value = "shzt")
    private String shzt;

    @TableField(value = "createtime")
    private String createTime;

    @TableField(value = "parkid")
    private String parkId;

}
