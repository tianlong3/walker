package com.dkm.modules.sys.pile.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @描述 充电桩添加数据
 * @创建时间 2019/10/21 13:53
 * @创建人 yangbin
 */
@Data
public class AddPile {

    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value = "充电桩编号")
    @NotBlank(message = "不能为空")
    private String id;

    @ApiModelProperty(value = "充电桩名称")
    private String pkname;

    @ApiModelProperty(value="所属小区ID")
    private Integer plot;

    @ApiModelProperty(value="所属区域id")
    @TableField(exist = false)
    private Integer regionCode;

    @ApiModelProperty(value="小区名称")
    @TableField(exist = false)
    private String plotName;

    @ApiModelProperty(value = "端口数")
    @TableField(exist = false)
    private Integer portCount;
}
