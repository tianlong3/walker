        
package com.dkm.modules.wx.feedback.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.wx.feedback.model.FeedbackResult;
import com.dkm.modules.wx.feedback.model.QueryFeedBack;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.dkm.modules.wx.feedback.model.Feedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


/**
 * @ClassName FeedbackMapper
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-13 19:40
 * @Version V1.0
 **/
@Repository
public interface FeedbackMapper  extends BaseMapper<Feedback> {

    List<FeedbackResult> getFeedBackList(@Param("p")Map<String, Object> params, Page<FeedbackResult> page);

    Map<String,Object> getFeedBack(@Param("id")String id);
}

