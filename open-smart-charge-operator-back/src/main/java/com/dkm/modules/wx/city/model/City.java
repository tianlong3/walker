
package com.dkm.modules.wx.city.model;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName City
 * @Description:
 * @Author yangbin
 * @Date 2019-09-07 00:03
 * @Version V1.0
 **/

@Getter
@Setter
public class City {


    /**
     * 城市代码
     */
    @ApiModelProperty("代码")
    @TableId
    private Integer id;
    /**
     * 城市名称
     */
    @ApiModelProperty("城市名称")
    private String name;
    /**
     * 省级代码
     */
    @ApiModelProperty("上级代码")
    private Integer pid;
}
