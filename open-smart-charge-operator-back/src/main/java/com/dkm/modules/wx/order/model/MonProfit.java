package com.dkm.modules.wx.order.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @描述 本月收益数据
 * @创建时间 2019/10/23 22:34
 * @创建人 yangbin
 */
@Data
public class MonProfit {

    @ApiModelProperty("总收益")
    private Float sumOrdergold;

    @ApiModelProperty("卡收益")
    private Float cardOrdergold;

    @ApiModelProperty("线上收益")
    private Float lineOrdergold;

    @ApiModelProperty("日期数据列表")
    private List<Profit> profits;
}
