        
package com.dkm.modules.wx.cashrecord.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.wx.cashrecord.model.OperatorCashRecord;

import java.util.Map;

/**
 * @ClassName OperatorCashRecord
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-22 09:46
 * @Version V1.0
 **/
public interface OperatorCashRecordService {

    /**
     * 分页查询提现记录
     * @param page 分页信息
     * @param params 餐宿
     * @return 分页查询结果
     */
    IPage<Map<String, Object>> getCashRecords(Page<Map<String, Object>> page, Map<String, Object> params);

    void addCashRecord(OperatorCashRecord operatorCashRecord);
}