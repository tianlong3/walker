        
package com.dkm.modules.wx.code.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dkm.modules.wx.code.mapper.CodeMapper;
import com.dkm.modules.wx.code.model.Code;
import com.dkm.modules.wx.code.service.CodeService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @ClassName CodeServiceImpl
 * @Description: 实现类
 * @Author yangbin
 * @Date 2019-09-14 00:27
 * @Version V1.0
 **/
@Service
public class CodeServiceImpl implements CodeService {

        private static final Logger LOGGER = LoggerFactory.getLogger(CodeServiceImpl.class);

        @Autowired
        private CodeMapper codeMapper;

        @Override
        public List<Code> getCodesByParent(String parentCode) {
                QueryWrapper<Code> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("parent_code",parentCode);
                return codeMapper.selectList(queryWrapper);
        }
}