package com.dkm.modules.wx.login.controller;

import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.constant.Constant;
import com.dkm.modules.sys.operator.service.OperatorService;
import com.dkm.modules.sys.user.service.UserService;
import com.dkm.modules.wx.login.model.LoginForm;
import com.dkm.util.redis.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @ClassName LoginController
 * @Description: 登录认证
 * @Author 杨膑
 * @Date 2019/9/4
 * @Version V1.0
 **/
@Api(tags = {"1 代理商登录管理"})
@RestController
@RequestMapping("/wx")
public class LoginController extends BaseController {
    private final static Logger logger = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    private OperatorService operatorService;

    @Autowired
    private UserService userService;
    @Autowired
    private RedisUtils redisUtils;
    @Value("${remote.prefix}")
    private String tokenPrefix;

    /**
     * 登录
     */
    @PostMapping("login")
    @ApiOperation("登录")
    public Response login(@Valid @ApiParam(value = "登录数据",required = true) LoginForm form){
        return userService.signIn(form, getSession(),Constant.LOGIN_TYPE_OPERATOR);
    }



    @PostMapping("loginOut")
    @ApiOperation("退出登录")
    public Response loginout(HttpServletRequest request){
        //获取token
        String token = request.getHeader(Constant.USER_TOKEN);
        //从redis中删除token
        String key = tokenPrefix+token;
        redisUtils.delete(key);
        return Response.ok();
    }

    @PostMapping("changePwd")
    @ApiOperation("修改密码")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response changePwd(@ApiParam("原密码") @RequestParam("password") String password,
                              @ApiParam("新密码")@RequestParam("newPassword")  String newPassword){
        //获取用户名
        String userId = (String) getSession().getAttribute(Constant.USER_SESSION_KEY);
        //更新密码
        return userService.changePwd(password,newPassword,userId);
    }


}
