package com.dkm.modules.wx.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.PageQueryParams;
import com.dkm.commons.Response;
import com.dkm.modules.sys.user.controller.SysUserController;
import com.dkm.modules.wx.card.model.RechargeRecods;
import com.dkm.modules.wx.card.service.CardService;
import com.dkm.modules.wx.cashrecord.service.OperatorBalanceService;
import com.dkm.modules.wx.user.model.UserData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @描述 使用用户管理控制器
 * @创建时间 2019/10/8 22:48
 * @创建人 yangbin
 */
@Api(tags = "10 用户管理")
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    private final static Logger logger = LoggerFactory.getLogger(SysUserController.class);
    @Autowired
    private CardService cardService;

    @ApiOperation(value = "获取用户数据列表")
    @GetMapping("/getUserList")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<IPage<UserData>> getUserList(@ApiParam("用户昵称") @RequestParam(value = "userName",required = false)String userName,
                                                 @ApiParam("排序字段: sum--余额，rechargeSum--累计充值") @RequestParam(value = "orderKey",required = false)String orderKey,
                                                 @ApiParam("是否倒序") @RequestParam(value = "isDesc",required = false)Boolean isDesc,
                                                 PageQueryParams p){
        logger.info("获取用户数据列表");
        Page<UserData> page = new Page<>(p.getPageNo(),p.getPageSize());
        Map<String,Object> params = new HashMap<>();
        /*当前代理商*/
        params.put("userId",getUserId());
        /*用户昵称*/
        params.put("userName",userName);
        params.put("isDesc",isDesc);
        params.put("orderKey",orderKey);
        IPage<UserData> userData= cardService.getUserList(params,page);
        return Response.ok(userData);
    }
}
