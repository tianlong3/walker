package com.dkm.modules.wx.city.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * @Description 地址列表
 * @Author Guo Liangbo
 * @Date 2019/11/7 23:03
 * @Version 1.0
 */
@Getter
@Setter
@ApiModel("地址列表")
public class AllAddress {

    /**
     * 省份list
     */
    @ApiModelProperty("省份list")
    private Map<Integer,String> province_list;

    /**
     * 城市list
     */
    @ApiModelProperty("城市list")
    private Map<Integer,String>  city_list;


    /**
     * 乡村list
     */
    @ApiModelProperty("乡村list")
    private Map<Integer,String>  county_list;
}
