package com.dkm.modules.wx.card.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @ClassName RechargeRecods
 * @Description: 充值记录，作返回数据使用
 * @Author 杨膑
 * @Date 2019/9/23
 * @Version V1.0
 **/
@ApiModel("充值记录")
@Getter
@Setter
public class RechargeRecods {

    @ApiModelProperty("主键ID")
    private int id;

    @ApiModelProperty("金额")
    private Float amount;

    @ApiModelProperty("时间")
    private String time;

    @ApiModelProperty("用户微信号")
    private String userName;

    @ApiModelProperty("用户微信头像")
    private String headImage;

    @ApiModelProperty("状态")
    private String status;

    @ApiModelProperty("被充值人手机号")
    private String mobile;
    @ApiModelProperty("充值人手机号")
    private String chargeUserMobile;

    @ApiModelProperty("充值编号")
    private String rechargeNumber;

    @ApiModelProperty("卡号")
    private String cardNo;

    @ApiModelProperty("数据类型，1 卡  2 账户")
    private int  dataType;

    @ApiModelProperty("收支类型 0 充值 1 消费（支出）2 赠送")
    private int  type;
}
