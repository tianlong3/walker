
package com.dkm.modules.wx.card.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.Response;
import com.dkm.modules.sys.user.model.RegisterUserResult;
import com.dkm.modules.sys.user.model.User;
import com.dkm.modules.wx.card.model.Card;
import com.dkm.modules.wx.card.model.RechargeRecods;
import com.dkm.modules.wx.card.model.UseRecods;
import com.dkm.modules.wx.user.model.UserData;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * @ClassName Card
 * @Description:
 * @Author yangbin
 * @Date 2019-09-13 12:50
 * @Version V1.0
 **/
public interface CardService {


    /**
     * 充值卡片
     *
     * @param cardNo 卡号
     * @param amount 充值金额
     * @return
     */
    Response rechargeCard(String cardNo, BigDecimal amount, User user);

    /*
     * 新增卡片*/
    Response addCard(Card card);

    /*挂失卡片*/
    Response lossCard(String cardNo);

    /**
     * 卡列表
     *
     * @param params 参数
     * @param page   分页
     * @return 返回列表信息
     */
    IPage<Card> getCardList(Map<String, Object> params, Page<Card> page);

    /**
     * 功能描述：获取卡使用记录
     *
     * @param page   分页信息
     * @param cardNo 卡号
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.dkm.modules.wx.card.model.UseRecods> 分页查询使用记录
     * @Author: guoliangbo
     * @Date: 2019/11/7 15:48
     */
    IPage<UseRecods> getUseRecods(Page<UseRecods> page, String cardNo);

    /*更新卡*/
    void updateCard(Card card);

    IPage<RechargeRecods> geRechargeRecods(Map<String, Object> params, Page<RechargeRecods> page);


    IPage<UserData> getUserList(Map<String, Object> params, Page<UserData> page);

    IPage<RegisterUserResult> getRegisterUser(Map<String, Object> params, Page<RegisterUserResult> page);

    /**
     * 转账操作
     *
     * @param openId 注册用户openI
     *               d
     * @param cardNo 转入卡号
     * @param amount 转账金额
     * @param user   操作
     */
    void transferAccounts(String openId, String cardNo, BigDecimal amount, User user);

    /**
     * 更新卡的余额,默认做加法
     *
     * @param cardNo     卡号
     * @param amount     金额
     * @param batchNo    操作批次号/订单号
     * @param changeType 充值类型
     * @param user       操作人信息
     */
    void updateAmountAndRecord(String cardNo, BigDecimal amount, String batchNo, Integer changeType, User user);

    /**
     * 根据卡号查询
     *
     * @param cardNo
     * @return
     */
    Card selectByCardNo(String cardNo);

    /**
     * 根据id查询卡号
     *
     * @param id
     * @return
     */
    Card selectById(Integer id);

    /**
     * 获取所有的卡
     *
     * @param customerId
     * @return
     */
    List<Card> getCardsByCustomerId(Integer customerId);
}