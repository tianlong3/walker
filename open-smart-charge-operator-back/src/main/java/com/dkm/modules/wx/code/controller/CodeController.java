        
package com.dkm.modules.wx.code.controller;

import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.dkm.modules.wx.code.model.Code;
import com.dkm.modules.wx.code.service.CodeService;

import java.util.List;

/**
 * @ClassName CodeController
 * @Description: 公共代码 控制器
 * @Author yangbin
 * @Date 2019-09-14 00:27
 * @Version V1.0
 **/
@Api(tags = "（公共）公共代码")
@RestController
public class CodeController extends BaseController{

    private final static Logger logger = LoggerFactory.getLogger(CodeController.class);
    @Autowired
    private CodeService codeService;


    @ApiOperation("通过父级代码获取")
    @GetMapping("/getCodesByParent")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<List<Code>> getCodesByParent(@ApiParam("父级代码")@RequestParam("parentCode") String parentCode){
        logger.info("获取代码parentCode="+parentCode);
        List<Code> codeList = codeService.getCodesByParent(parentCode);
        return Response.ok(codeList);

    }

}
