
package com.dkm.modules.wx.order.model;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName ChargingOrder
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-09 23:38
 * @Version V1.0
 **/

@Getter
@Setter
public class ChargingOrder {



    /**
     * 订单Id
     * 
     */
    @TableId
    private Integer id;
    /**
     * 订单类型，8001 按小时计费（取决于用户选择
     * 订单类型，8001 按小时计费（取决于用户选择,默认） 8002 按度数计费（取决于 用户选择）
8003 免费订单（取决于充电桩设置）
     */
    private String orderType;
    /**
     * 耗电量 单位 kw
     * 
     */
    private BigDecimal consumePower;
    /**
     * 自定义价格ID
     * 
     */
    private String customPriceId;
    /**
     * 充电口ID
     * 
     */
    private String parklockid;
    /**
     * 充电时长
     * 
     */
    private String hour;
    /**
     * 发布金额
     * 
     */
    private String price;
    /**
     * 手机号
     * 
     */
    private String mobile;
    /**
     * 创建时间
     * 
     */
    private Date createtime;
    /**
     * 充电桩ID
     * 
     */
    private String parkid;
    /**
     * Code
     * 
     */
    private String code;
    /**
     * 订单号
     * 
     */
    private String ordernumber;
    /**
     * 1.下单 2.取消 3.已支付 4.退款 5.完成
     * 
     */
    private String orderstate;
    /**
     * 微信openid
     * 
     */
    private String weixinOpenid;
    /**
     * 支付金额
     * 
     */
    private String ordergold;
    /**
     * wx支付订单编写
     * 
     */
    private String outTradeNo;
    /**
     * 开始时间
     * 
     */
    private String starttime;
    /**
     * 结束时间
     * 
     */
    private String endtime;
}
