        
package com.dkm.modules.wx.city.controller;

import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.constant.Constant;
import com.dkm.modules.wx.city.model.AllAddress;
import com.dkm.modules.wx.city.model.Plot;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.dkm.modules.wx.city.model.City;
import com.dkm.modules.wx.city.service.CityService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName CityController
 * @Description: 省市管理控制器
 * @Author yangbin
 * @Date 2019-09-07 00:03
 * @Version V1.0
 **/
@Api(value="省市接口",tags = "(公共)省市地址接口")
@RestController
@RequestMapping("/city")
public class CityController extends BaseController {

        private final static Logger logger = LoggerFactory.getLogger(CityController.class);
        @Autowired
        private CityService cityService;

        @ApiOperation("获取省份列表")
        @GetMapping("/getProvinceList")
        @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
        public Response<List<City>> getProvinceList(){
                logger.info("获取省份列表");
                /*获取省份列表，pid 为0*/
                List<City> provinceList= cityService.getCityList(0);
                return Response.ok(provinceList);
        }

        @ApiOperation("根据省份ID获取城市(地区)列表")
        @GetMapping("/getCityList")
        @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
        public Response<List<City>> getCityList(@ApiParam(value = "省份ID、市ID")@RequestParam("pid") Integer pid){
                logger.info("根据省份ID获取城市列表");
                List<City> CityList = cityService.getCityList(pid);
                return Response.ok(CityList);
        }

        @ApiOperation("根据地区ID 获取小区列表")
        @GetMapping("/getPoltList")
        @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
        public Response<List<Plot>> getPoltList(@ApiParam(value = "地区ID")@RequestParam("regionCode") String regionCode){
                logger.info("根据地区ID 获取小区列表");
                Map<String,Object> params = new HashMap<>();
                /*地区代码*/
                params.put("regionCode",regionCode);
                /*代理商*/
                params.put("userId",getUserId());
                List<Plot> regionList = cityService.getPoltListByRegion(params);
                return Response.ok(regionList);
        }

    @ApiOperation("获取省市区三级地址list")
    @GetMapping("/getAllAddress")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<AllAddress> getAllAddress(){
        logger.info("获取省市区三级地址list");
        AllAddress allAddress = cityService.getAllAddress();
        return Response.ok(allAddress);
    }
}
