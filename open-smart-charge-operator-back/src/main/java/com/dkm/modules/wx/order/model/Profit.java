package com.dkm.modules.wx.order.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @描述 收益
 * @创建时间 2019/10/23 22:38
 * @创建人 yangbin
 */
@Data
public class Profit {

    @ApiModelProperty("卡收益")
    private Float cardOrdergold;

    @ApiModelProperty("线上收益")
    private Float lineOrdergold;

    @ApiModelProperty("时间")
    private String createTime;
}
