        
package com.dkm.modules.wx.cashrecord.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.dkm.modules.wx.cashrecord.model.OperatorCashRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;


/**
 * @ClassName OperatorCashRecordMapper
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-22 09:46
 * @Version V1.0
 **/
@Repository
public interface OperatorCashRecordMapper  extends BaseMapper<OperatorCashRecord> {

    /**
     * 查询某个代理商的提现信息
     * @param params 用户参数
     * @param page 分页信息
     * @return  返回分页结果
     */
    List<Map<String, Object>> getCashRecords(@Param("p") Map<String, Object> params, Page<Map<String, Object>> page);
}

