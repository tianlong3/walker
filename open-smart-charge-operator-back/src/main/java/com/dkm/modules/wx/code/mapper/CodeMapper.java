        
package com.dkm.modules.wx.code.mapper;

import org.springframework.stereotype.Repository;
import com.dkm.modules.wx.code.model.Code;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * @ClassName CodeMapper
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-14 00:27
 * @Version V1.0
 **/
@Repository
public interface CodeMapper  extends BaseMapper<Code> {

}

