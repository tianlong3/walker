        
package com.dkm.modules.wx.cashrecord.service.impl;

import com.dkm.modules.wx.cashrecord.mapper.OperatorBalanceMapper;
import com.dkm.modules.wx.cashrecord.mapper.OperatorCashRecordMapper;
import com.dkm.modules.wx.cashrecord.model.OperatorBalance;


import com.dkm.modules.wx.cashrecord.service.OperatorBalanceService;
import com.dkm.util.sms.CCPRestSDK;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @ClassName OperatorBalanceServiceImpl
 * @Description: 代理商钱包余额实现类
 * @Author yangbin
 * @Date 2019-09-22 17:58
 * @Version V1.0
 **/
@Service
public class OperatorBalanceServiceImpl implements OperatorBalanceService {

        private static final Logger logger = LoggerFactory.getLogger(OperatorBalanceServiceImpl.class);

        @Autowired
        private OperatorBalanceMapper operatorBalanceMapper;
        @Autowired
        private OperatorCashRecordMapper operatorCashRecordMapper;

        @Value("${sms.serverIP}")
        private String serverIP;
        @Value("${sms.serverPort}")
        private String serverPort;
        @Value("${sms.accountSid}")
        private String accountSid;
        @Value("${sms.accountToken}")
        private String accountToken;
        @Value("${sms.appId}")
        private String appId;

        @Override
         /**
          *@描述  通过代理商ID获取代理商余额信息
          *@参数  [userId 代理商ID]
          *@返回值  com.dkm.modules.wx.cashrecord.model.OperatorBalance
          *@创建人  yangbin
          *@创建时间  2019/9/24
          */
        public OperatorBalance getBalanceByUserId(Integer userId) {
                return operatorBalanceMapper.selectById(userId);
        }

        @Override
        /**
         *@描述   更新余额
         *@参数  [operatorBalance]
         *@返回值  void
         *@创建人  yangbin
         *@创建时间  2019/10/25
         */
        public void updateBlance(OperatorBalance operatorBalance) {
                /*更新余额表*/
                operatorBalance.setUpdateTime(new Date());
                operatorBalanceMapper.updateById(operatorBalance);
        }

        @Override
        /**
         *@描述   发送短信
         *@参数  [mobile 手机号, code验证码]
         *@返回值  void
         *@创建人  yangbin
         *@创建时间  2019/10/27
         */
        public void sendSms(String mobile, String code) {
                HashMap<String, Object> result = null;

                CCPRestSDK restAPI = new CCPRestSDK();
                restAPI.init(serverIP, serverPort);// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
                restAPI.setAccount(accountSid, accountToken);// 初始化主帐号和主帐号TOKEN
                restAPI.setAppId(appId);// 初始化应用ID
                result = restAPI.sendTemplateSMS(mobile,"478850", new String[]{code, "5分钟"});

                logger.info("短信发送结果=" + result);

                if("000000".equals(result.get("statusCode"))){
                        //正常返回输出data包体信息（map）
                        HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
                        Set<String> keySet = data.keySet();
                        for(String key:keySet){
                                Object object = data.get(key);
                                logger.info(key +" = "+object);
                        }
                }else{
                        //异常返回输出错误码和错误信息
                        logger.error("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
                }
        }

}