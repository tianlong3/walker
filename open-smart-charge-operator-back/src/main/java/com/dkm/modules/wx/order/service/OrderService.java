        
package com.dkm.modules.wx.order.service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;


import com.dkm.commons.Response;
import com.dkm.modules.sys.user.model.User;
import com.dkm.modules.wx.order.model.AddressReport;
import com.dkm.modules.wx.order.model.MonProfit;
import com.dkm.modules.wx.order.model.OrderQueryForm;
import com.dkm.modules.wx.order.model.OrderResult;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * @ClassName ChargingOrder
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-09 23:38
 * @Version V1.0
 **/
public interface OrderService {

    /**
     * 订单列表
     * @param params 参数
     * @param page 分页
     * @return 分页结果
     */
    IPage<OrderResult> getOrderList(Map<String,Object> params,Page<OrderResult> page);

    /*经营管理首页数据*/
    Map<String, Object> getOperateData(Integer userId);

    /*地址统计列表*/
    List<AddressReport> getAddressStatistics(Map<String,Object> params);

    /*今日收益*/
    Map<String, Object> getDayProfit(OrderQueryForm orderQueryForm);

    /*月收益*/
    MonProfit getMonProfit(Integer userId);

    OrderResult getOrderDetail(String orderNumber);

    /**
      * 功能描述：订单退款
      * @param orderId 订单表id
      * @param user 当前登录用户
      * @param refundAmount 退款金额
      * @return com.dkm.commons.Response<java.lang.String> 返回结果
      * @Author: guoliangbo
      * @Date: 2019/12/12 15:20
      */
    Response<String> refundOrder(Integer orderId, User user, BigDecimal refundAmount);

    /***
     * 端口变更
     * liwenhui
     */
    Response<String> portChange(String orderId, User userInfo, String portCode, String parkId);


}