package com.dkm.modules.wx.order.model;

import com.dkm.modules.wx.pile.model.PileResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @描述 时间统计返回结果
 * @创建时间 2019/10/3 15:25
 * @创建人 yangbin
 */
@Data
@ApiModel
public class AddressReport {

    @ApiModelProperty("小区编号")
    private int  plotCode;

    @ApiModelProperty("小区名称")
    private String plotName;

    @ApiModelProperty("小区地址")
    private String address;

    @ApiModelProperty("小区充电桩数量")
    private Integer pileCount;

    @ApiModelProperty("充电桩数据")
    private List<PileResult> pileData;

    @ApiModelProperty("收入总额")
    private Float sumOrdergold;
}
