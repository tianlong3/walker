
package com.dkm.modules.wx.rule.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dkm.commons.Response;
import com.dkm.constant.Constant;
import com.dkm.exception.ServiceException;
import com.dkm.modules.wx.rule.mapper.CustomPriceMapper;
import com.dkm.modules.wx.rule.mapper.RuleMapper;
import com.dkm.modules.wx.rule.model.CustomPrice;
import com.dkm.modules.wx.rule.service.CustomPriceService;
import com.dkm.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName CustomPriceServiceImpl
 * @Description: 规则价格管理实现类
 * @Author yangbin
 * @Date 2019-09-20 20:59
 * @Version V1.0
 **/
@Service
public class CustomPriceServiceImpl implements CustomPriceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomPriceServiceImpl.class);

    @Autowired
    private CustomPriceMapper customPriceMapper;
    @Autowired
    private RuleMapper ruleMapper;

    @Override
    public Response addCustomPrice(CustomPrice customPrice) {
        // 定义规则，新增的价格规则，只能是按小时 固定时间价格
        customPrice.setChargeType(Constant.CHARGE_TYPE_HOURS);
        customPrice.setType("1");
        /*插入规则价格*/
        customPrice.setCreatetime(new Date());
        customPriceMapper.insert(customPrice);
        return Response.ok(0, "添加成功");
    }


    /**
     * @描述 查询规则价格
     * @参数 [parasms]
     * @返回值 com.dkm.modules.wx.rule.model.CustomPrice
     * @创建人 yangbin
     * @创建时间 2019/10/25
     */
    @Override
    public CustomPrice getCustomPrice(Map<String, Object> parasms) {
        /*获取规则价格数据*/
        return ruleMapper.getCustomPrice(parasms);
    }

    @Override
    public Response editCustomPrice(CustomPrice customPrice) {
        String hours = customPrice.getHours();
        if (StringUtils.isEmpty(hours)) {
            throw new ServiceException("单价不能为空");
        }
        BigDecimal bigDecimal = new BigDecimal(hours);
        if (BigDecimal.ZERO.compareTo(bigDecimal) >= 0) {
            throw new ServiceException("单价不能为零或负数");
        }

        Integer id = customPrice.getId();
        if (null == id) {
            return Response.error(-1, "id不能为空！");
        }
        CustomPrice price = customPriceMapper.selectById(customPrice.getId());
        if (null == price) {
            return Response.error(-2, "该价格规则不存在！");
        }
        /*更新规则价格*/
        customPrice.setType(null);
        customPrice.setChargeType(null);
        customPriceMapper.updateById(customPrice);
        return Response.ok();
    }


    /**
     * @描述 删除规则价格
     * @参数 [id 规则价格ID]
     * @返回值 void
     * @创建人 yangbin
     * @创建时间 2019/10/25
     */
    @Override
    public Response delCustomPrice(Integer id) {
        CustomPrice price = customPriceMapper.selectById(id);
        if (price == null) {
            return Response.ok();
        } else {
            String chargeType = price.getChargeType();
            String type = price.getType();
            if (Constant.CHARGE_TYPE_KWH.equals(chargeType)) {
                return Response.error(-2, "按度数充电类型价格，只可编辑，不可删除！");
            } else if (Constant.CHARGE_TYPE_HOURS.equals(chargeType) && "2".equals(type)) {
                return Response.error(-3, "按小时充电类型自定义时长价格，只可编辑，不可删除！");
            } else {
                customPriceMapper.logicalDeletion(id);
                return Response.ok();
            }
        }
    }

    @Override
    /**
     *@描述 根据规则ID 查询规则价格数据
     *@参数 [ 规则 id]
     *@返回值 java.util.List<com.dkm.modules.wx.rule.model.CustomPrice>
     *@创建人 yangbin
     *@创建时间 2019/11/10
     */
    public List<CustomPrice> getCustomPriceByRuleId(Integer id) {
        QueryWrapper<CustomPrice> queryWrapper = new QueryWrapper();
        queryWrapper.eq("rule_id", id);
        return customPriceMapper.selectList(queryWrapper);
    }


}