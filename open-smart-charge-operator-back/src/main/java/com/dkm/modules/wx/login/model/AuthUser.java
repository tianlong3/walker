package com.dkm.modules.wx.login.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName AuthUser
 * @Description: 身份权限认证类 登陆身份认证
 * @Author 杨膑
 * @Date 2019/9/4
 * @Version V1.0
 **/
@Getter
@Setter
public class AuthUser {

    private String userName;
    private String realName;
    private String token;
}
