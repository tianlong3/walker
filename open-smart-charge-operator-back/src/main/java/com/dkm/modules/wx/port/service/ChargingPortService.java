        
package com.dkm.modules.wx.port.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.Map;

import com.dkm.commons.Response;
import com.dkm.modules.wx.pile.model.ChargingPile;
import com.dkm.modules.wx.port.model.ChargingPort;
/**
 * @ClassName ChargingPort
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-05 13:52
 * @Version V1.0
 **/
public interface ChargingPortService {


    void addPort(ChargingPort chargingPort);

    IPage<ChargingPort> getPortList(Page<ChargingPort> page, Map<String, String> params);

    void editPort(ChargingPort chargingPort);

    void delPort(Integer id);

    void addPortForPile(ChargingPile chargingPile);

    /**
      * 功能描述：开启或者关闭设备端口
      * @param id 端口id
      * @param type 开启1 关闭0
      * @Return: com.dkm.commons.Response 相应信息
      * @Author: Guo Liangbo
      * @Date:2019/11/17 21:42
      */
    Response switchPort(Integer id, Integer type);

    Response allPort(String code, Integer type);
}