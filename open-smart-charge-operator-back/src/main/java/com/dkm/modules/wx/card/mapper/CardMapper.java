
package com.dkm.modules.wx.card.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.user.model.RegisterUserResult;
import com.dkm.modules.wx.card.model.Card;
import com.dkm.modules.wx.card.model.RechargeRecods;
import com.dkm.modules.wx.card.model.UseRecods;
import com.dkm.modules.wx.user.model.UserData;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * @ClassName CardMapper
 * @Description:
 * @Author yangbin
 * @Date 2019-09-13 12:50
 * @Version V1.0
 **/
@Repository
public interface CardMapper extends BaseMapper<Card> {

    List<Card> getCardList(@Param("p") Map<String, Object> params, Page<Card> page);

    List<UseRecods> getUseRecods(Page<UseRecods> page, @Param("cardNo") String cardNo);

    List<RechargeRecods> geRechargeRecods(Page<RechargeRecods> page, @Param("params") Map<String, Object> params);

    List<UserData> getUserList(@Param("params") Map<String, Object> params, Page<UserData> page);

    List<RegisterUserResult> getRegisterUser(@Param("p") Map<String, Object> params, Page<RegisterUserResult> page);

    /**
     * 充值卡片,余额默认做加法
     *
     * @param cardNo 卡号
     * @param amount 金额
     * @return
     */
    int updateSumByCardNo(@Param("cardNo") String cardNo, @Param("amount") BigDecimal amount);

    /**
     * 功能描述：根据卡号查询卡信息
     *
     * @param cardNo 卡号
     * @return com.dkm.modules.wx.card.model.Card
     * @Author: guoliangbo
     * @Date: 2019/12/12 16:28
     */
    Card getCardInfo(@Param("cardNo") String cardNo);

    /**
     * 功能描述：更新卡余额
     *
     * @param card 卡信息
     * @return java.lang.Integer
     * @Author: guoliangbo
     * @Date: 2019/12/12 16:32
     */
    Integer updateCardBalance(Card card);

}

