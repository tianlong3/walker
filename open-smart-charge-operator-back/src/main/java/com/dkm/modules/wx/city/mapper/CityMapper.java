        
package com.dkm.modules.wx.city.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.wx.city.model.City;
import com.dkm.modules.wx.city.model.Plot;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @ClassName CityMapper
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-07 00:03
 * @Version V1.0
 **/
@Repository
public interface CityMapper  extends BaseMapper<City> {

    List<Plot> getPoltListByRegion(Map<String,Object> params);
}

