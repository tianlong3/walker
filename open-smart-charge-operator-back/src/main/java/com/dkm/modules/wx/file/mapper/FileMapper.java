        
package com.dkm.modules.wx.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.wx.file.model.FileModel;
import org.springframework.stereotype.Repository;


/**
 * @ClassName FileMapper
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-05 23:09
 * @Version V1.0
 **/
@Repository
public interface FileMapper  extends BaseMapper<FileModel> {

}

