
package com.dkm.modules.wx.rule.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.Response;
import com.dkm.constant.Constant;
import com.dkm.dataScope.DataScope;
import com.dkm.modules.wx.rule.mapper.CustomPriceMapper;
import com.dkm.modules.wx.rule.mapper.RuleMapper;
import com.dkm.modules.wx.rule.model.CustomPrice;
import com.dkm.modules.wx.rule.model.Rule;
import com.dkm.modules.wx.rule.service.CustomPriceService;
import com.dkm.modules.wx.rule.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName RuleServiceImpl
 * @Description: 规则管理实现方法
 * @Author yangbin
 * @Date 2019-09-20 15:29
 * @Version V1.0
 **/
@Service
public class RuleServiceImpl implements RuleService {

    private static final Logger log = LoggerFactory.getLogger(RuleServiceImpl.class);

    @Autowired
    private RuleMapper ruleMapper;
    @Autowired
    private CustomPriceService customPriceService;
    @Autowired
    private CustomPriceMapper customPriceMapper;


    @Override
    public Response addRule(Rule rule) {
        /*判断当前代理商规则名称是否重复*/
        Rule vailRule = ruleMapper.getRule(rule);
        /*如果重复 不做插入操作 并提示重复*/
        if (vailRule != null) {
            log.error("规则添加失败，规则名称重复!");
            return Response.error(-1, "添加失败，规则名称重复！");
        }
        /*设置操作时间*/
        rule.setCreatetime(new Date());
        /*插入规则*/
        ruleMapper.insert(rule);
        return Response.ok();

    }


    /**
     * @描述 查询规则列表
     * @参数 [params 查询参数]
     * @返回值 java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @创建人 yangbin
     * @创建时间 2019/10/55
     */
    @Override
    @DataScope(alias = "a.userId")
    public IPage<Rule> getRuleListByUserNm(Map<String, Object> params, Page<Rule> page) {

        /*通过用户名获取规则列表数据*/
        List<Rule> ruleList = ruleMapper.getRuleListByUserNm(params, page);
        for (Rule rule : ruleList) {
            String type = rule.getRuleType();
            String chargeName = rule.getChangeName();
            if (Constant.RULE_TYPE_CARD.equals(type)) {
                rule.setChangeName("卡-" + chargeName);
            } else {
                rule.setChangeName("公众号-" + chargeName);
            }
        }
        page.setRecords(ruleList);
        return page;
    }


    /**
     * @描述 删除规则
     * @参数 [规则 id]
     * @返回值 com.dkm.commons.Response
     * @创建人 yangbin
     * @创建时间 2019/10/9
     */
    @Override
    public Response deleteRule(int id) {
        /*获取当前规则下使用规则的是充电桩数量*/
        int useCount = ruleMapper.getRoleUseContById(id);
        /*如果使用该规则数量大于0  不做删除操作，并提示用户*/
        if (useCount > 0) {
            return Response.error(-1, "删除失败，有" + useCount + "个充电桩在使用该规则");
        }
        // 校验默认规则不可删除
        Rule rule = ruleMapper.selectById(id);
        if (null == rule) {
            return Response.error(-1, "该规则ID不存在！");
        } else {
            Integer isSysGive = rule.getIsSysGive();
            Integer isGive = rule.getIsGive();
            if (Constant.RULE_DEFAULT.equals(isSysGive) || Constant.RULE_DEFAULT.equals(isGive)) {
                return Response.error(-2, "默认规则不可删除！");
            }
            /*删除规则*/
            ruleMapper.deleteById(id);
            return Response.ok(0, "删除成功！");
        }

    }

    @Override
    public List<Map<String, Object>> getRulePriceListById(int id) {
        return ruleMapper.getRulePriceListById(id);
    }

    @Override
    public Response getRule(int id) {
        Rule rule = ruleMapper.selectById(id);
        if (null == rule) {
            return Response.error(-2, "规则不存在！");
        }
        String type = rule.getRuleType();
        String chargeName = rule.getChangeName();
        if (Constant.RULE_TYPE_CARD.equals(type)) {
            rule.setChangeName("卡-" + chargeName);
        } else {
            rule.setChangeName("公众号-" + chargeName);
        }
        return Response.ok(rule);
    }

    @Override
    public void editeRule(Rule rule) {
        ruleMapper.updateById(rule);
    }

    @Override
    /**
     *@描述 复制规则
     *@参数 [rule]
     *@返回值 com.dkm.commons.Response
     *@创建人 yangbin
     *@创建时间 2019/11/10
     */
    @Transactional
    public Response copyRule(Rule rule) {
        Rule oldRule = ruleMapper.selectById(rule.getId());
        oldRule.setChangeName(rule.getChangeName());
        oldRule.setId(null);
        oldRule.setUserId(rule.getUserId());
        /*复制规则*/
        Rule vailRule = ruleMapper.getRule(rule);
        /*如果重复 不做插入操作 并提示重复*/
        if (vailRule != null) {
            log.error("规则复制失败，规则名称重复!");
            return Response.error(-1, "复制失败，规则名称重复！");
        }
        /*设置操作时间*/
        oldRule.setCreatetime(new Date());
        //设置为非默认规则
        oldRule.setIsGive(0);
        oldRule.setIsSysGive(0);
        /*插入规则*/
        ruleMapper.insert(oldRule);

        /*复制规则价格*/
        List<CustomPrice> customPrices = customPriceService.getCustomPriceByRuleId(rule.getId());
        if (customPrices.size() > 0) {
            for (CustomPrice c : customPrices) {
                c.setId(null);
                c.setCreatetime(new Date());
                c.setRuleId(oldRule.getId());
                customPriceMapper.insert(c);
            }
        }

        return Response.ok(0, "复制成功");
    }

}