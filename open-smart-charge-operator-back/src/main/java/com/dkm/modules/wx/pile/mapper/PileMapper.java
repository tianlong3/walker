package com.dkm.modules.wx.pile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.map.model.PlotPile;
import com.dkm.modules.sys.pile.model.SysPileResult;
import com.dkm.modules.sys.statistics.model.PilesDetail;
import com.dkm.modules.sys.statistics.model.SalesDetail;
import com.dkm.modules.wx.pile.model.ChargingPile;
import com.dkm.modules.wx.pile.model.PileResult;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface PileMapper extends BaseMapper<ChargingPile> {

    List<PileResult> getPilesByPlot(Map<String, String>  params);

    /**
      * 功能描述：分页获取 充电桩列表
      * @param page 分页信息
      * @param params 参数
      * @return java.util.List<com.dkm.modules.sys.pile.model.SysPileResult>
      * @Author: guoliangbo
      * @Date: 2019/11/11 15:15
      */
    List<SysPileResult>  getPilePageList(Page page, @Param("params") Map<String, String> params);

    List<PlotPile> getPlotPile();

    List<Map<String, Object>> statisticalPile(Map<String, Object> params);

    /**
      * 功能描述： 根据小区id和用户id查询充电桩
      * @param params 小区编码和用户id
      * @Return:java.util.List<com.dkm.modules.wx.pile.model.PileResult> 充电桩列表
      * @Author: Guo Liangbo
      * @Date:2019/11/12 14:29
      */
    List<PileResult> getPileByPlotAndUserId(Map<String, String> params);

    List<PilesDetail> selectPilesInfo(Page page);
}
