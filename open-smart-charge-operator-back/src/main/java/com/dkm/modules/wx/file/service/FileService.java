        
package com.dkm.modules.wx.file.service;

import com.dkm.modules.wx.file.model.FileModel;
import org.springframework.web.multipart.MultipartFile;


/**
 * @ClassName File
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-05 23:09
 * @Version V1.0
 **/
public interface FileService {

    /*上传文件*/
    void uploadFile(MultipartFile file, FileModel fileModel);

    FileModel getFileById(String imgId);

    void insertFileInfo(FileModel fileModel);
}