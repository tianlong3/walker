        
package com.dkm.modules.wx.rule.service;

import com.dkm.commons.Response;
import com.dkm.modules.wx.rule.model.CustomPrice;
import com.dkm.modules.wx.rule.model.Rule;

import java.util.List;
import java.util.Map;


/**
 * @ClassName CustomPrice
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-20 20:59
 * @Version V1.0
 **/
public interface CustomPriceService {


    /**
     * 添加规则价格数据
     * @param customPrice
     * @return
     */
    Response addCustomPrice(CustomPrice customPrice);

    /**
     * 获取规则价格数据
     * @param id
     * @return
     */
    CustomPrice getCustomPrice(Map<String, Object> id);

    /**
      * 修改规则价格
      * @param customPrice
     */
    Response editCustomPrice(CustomPrice customPrice);

    /**
      * 功能描述：删除价格
      * @param id 价格id
      * @Return:void
      * @Author: Guo Liangbo
      * @Date:2019/11/10 21:24
      */
    Response delCustomPrice(Integer id);


    List<CustomPrice> getCustomPriceByRuleId(Integer id);
}