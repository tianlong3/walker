        
package com.dkm.modules.wx.city.service;


import com.dkm.modules.wx.city.model.AllAddress;
import com.dkm.modules.wx.city.model.City;
import com.dkm.modules.wx.city.model.Plot;

import java.util.List;
import java.util.Map;

/**
 * @ClassName City
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-07 00:03
 * @Version V1.0
 **/
public interface CityService {

     /**
      * @MethodName:
      * @Description: 通过省份ID获取城市列表
      * @Param:
      * @Return:
      * @Author: yangbin
      * @Date: 2019/9/7
     **/
    List<City> getCityList(Integer pid);

    List<Plot> getPoltListByRegion(Map<String,Object> params);

    /**
      * 功能描述：获取所有省市区地址
      * @param void
      * @Return:com.dkm.modules.wx.city.model.AllAddress 结果地址
      * @Author: Guo Liangbo
      * @Date: 2019/11/7 23:08
      */
    AllAddress getAllAddress();
}