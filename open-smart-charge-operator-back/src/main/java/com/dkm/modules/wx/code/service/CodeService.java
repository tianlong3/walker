        
package com.dkm.modules.wx.code.service;

import com.dkm.modules.wx.code.model.Code;

import java.util.List;


/**
 * @ClassName Code
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-14 00:27
 * @Version V1.0
 **/
public interface CodeService {


    List<Code> getCodesByParent(String parentCode);
}