        
package com.dkm.modules.wx.rule.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.dkm.modules.wx.rule.model.CustomPrice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * @ClassName CustomPriceMapper
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-20 20:59
 * @Version V1.0
 **/
@Repository
public interface CustomPriceMapper  extends BaseMapper<CustomPrice> {

    /**
      * 功能描述：逻辑删除价格
      * @param id 价格id
      * @return java.lang.Integer
      * @Author: guoliangbo
      * @Date: 2019/11/11 14:05
      */
    Integer logicalDeletion(@Param("id") Integer id);
}

