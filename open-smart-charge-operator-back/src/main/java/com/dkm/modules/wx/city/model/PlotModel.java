package com.dkm.modules.wx.city.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @描述 小区实体类
 * @创建时间 2019/10/23 21:02
 * @author yangbin
 */
@Data
@TableName("c_plot")
public class PlotModel extends Plot {

    @ApiModelProperty("地址")
    @NotBlank(message = "地址不能为空")
    private String Address;

    @ApiModelProperty("所属地区代码")
    @NotBlank(message = "所属地区不能为空")
    private Integer regionCode;

    @ApiModelProperty("创建时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty("经度")
    private String lat;

    @ApiModelProperty("纬度")
    private String lng;
}
