
package com.dkm.modules.wx.rule.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @ClassName CustomPrice
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-20 20:59
 * @Version V1.0
 **/

@Getter
@Setter
public class CustomPrice {


    @TableId
    private Integer id;
    /**
     * 计费类型 3001：按小时计费 3002：按度数计费（此类型，一个充电桩就一个）
     * 
     */
    @ApiModelProperty("计费类型 公共代码parentCode=3")
    private String chargeType;
    /**
     * 时长
     *
     */
    @ApiModelProperty("时长")
    private String time;
    /**
     * 费用
     *
     */
    @ApiModelProperty("费用")
    private String price;
    /**
     * 每小时-单价
     * 
     */
    @NotBlank(message = "每小时")
    @ApiModelProperty("每小时-单价")
    private String hours;
    /**
     * 1.固定 2.自定义时长
     * 
     */
    @ApiModelProperty("类型 1.固定 2.自定义时长")
    private String type;

    @ApiModelProperty("类型名称")
    @TableField(exist = false)
    private String typeName;
    /**
     * 规则大于4个小时，第五个小时0.5元
     * 
     */
    @ApiModelProperty("规则大于某值 例如：规则大于4个小时，第五个小时0.5元")
    private String granternum;
    /**
     * 规则大于4个小时，第五个小时0.5元
     * 
     */
    @ApiModelProperty("规则大于某值时价格 例如：规则大于4个小时，第五个小时0.5元")
    private String granterprice;
    /**
     * 充电桩ID
     * 
     */
    @ApiModelProperty(value = "充电桩ID",hidden = true)
    private String chargePileId;
    /**
     * 创建时间
     * 
     */
    @ApiModelProperty(value = "创建时间",hidden = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createtime;
    /**
     * 规则ID
     * 
     */
    @NotNull(message = "所属规则ID")
    @ApiModelProperty("所属规则ID")
    private Integer ruleId;

    @ApiModelProperty(value = "删除标记，默认0：未删除，1 删除",hidden = true)
    private Integer delFlag;
}
