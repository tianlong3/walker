        
package com.dkm.modules.wx.rule.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.Response;
import com.dkm.modules.wx.rule.model.CustomPrice;
import com.dkm.modules.wx.rule.model.Rule;

import java.util.List;
import java.util.Map;


/**
 * @ClassName Rule
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-20 15:29
 * @Version V1.0
 **/
public interface RuleService {

    /**
      * 功能描述：添加规则
      * @param rule 规则
      * @Return:com.dkm.commons.Response 结果
      * @Author: Guo Liangbo
      * @Date:2019/11/10 21:05
      */
    Response addRule(Rule rule);

    /**
     * 根据代理商用户名查询规则列表
     * @param params 参数
     * @return 分页结果
     */
    IPage<Rule> getRuleListByUserNm(Map<String,Object> params, Page<Rule> page);

    /**
     * 根据规则ID删除规则
     * @param id 规则id
     * @return 影响行数
     */
    Response deleteRule(int id);

    /**
     * 根据规则ID 获取价格
     * @param id 规则id
     * @return
     */
    List<Map<String,Object>> getRulePriceListById(int id);

    /**
      * 功能描述：根据规则ID获取规则数据
      * @param id 规则数据
      * @return com.dkm.commons.Response
      * @Author: guoliangbo
      * @Date: 2019/11/11 16:28
      */
    Response getRule(int id);

    /**
     * 修改规则
     * @param rule 规则
     */
    void editeRule(Rule rule);

    /**
      * 功能描述：复制规则
      * @param rule 复制规则id和新规则名称
      * @return com.dkm.commons.Response
      * @Author: guoliangbo
      * @Date: 2019/11/11 16:28
      */
    Response copyRule(Rule rule);
}