package com.dkm.modules.wx.card.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dkm.modules.wx.card.model.RechargeRecodPO;
import org.springframework.stereotype.Repository;

/**
 * DESCRIPTION :
 *
 * @author ducf
 * @create 2019-12-07 17:21
 */
@Repository
public interface RechargeRecodMapper extends BaseMapper<RechargeRecodPO> {
}
