
package com.dkm.modules.wx.cashrecord.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.omg.CORBA.IDLType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName OperatorBalance
 * @Description: 代理商余额
 * @Author yangbin
 * @Date 2019-09-22 17:58
 * @Version V1.0
 **/

@Getter
@Setter
public class OperatorBalance {


    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value="代理商ID",hidden = true)
    private Integer userId;

    @ApiModelProperty(value="账户余额")
    private Float amount;

    @ApiModelProperty(value="创建时间",hidden = true)
    private Date createTime;

    @ApiModelProperty(value="修改时间",hidden = true)
    private Date updateTime;

    @ApiModelProperty(value="乐观锁",hidden = true)
    private Integer version;
}
