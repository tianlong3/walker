package com.dkm.modules.wx.pile.controller;

import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.modules.sys.pile.model.SysPileResult;
import com.dkm.modules.wx.pile.mapper.PileMapper;
import com.dkm.modules.wx.pile.model.ChargingPile;
import com.dkm.modules.wx.pile.model.PileResult;
import com.dkm.modules.wx.pile.model.PlotPileTree;
import com.dkm.modules.wx.pile.service.PileService;
import com.dkm.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName ChargingPileController
 * @Description:
 * @Author 杨膑
 * @Date 2019/9/5
 * @Version V1.0
 **/


@RestController
@RequestMapping("/wx/chargingpile")
@Api(value = "设备管理 ontroller", tags = {"2 充电桩管理"})
public class PileController extends BaseController {

    private final static Logger logger = LoggerFactory.getLogger(PileController.class);
    @Autowired
    private PileService chargingPileService;

    @Autowired
    PileMapper pileMapper;

    /**
     * @MethodName:
     * @Description:设备列表
     * @Param:
     * @Return:
     * @Author: yangbin
     * @Date: 2019/9/5
     **/
    @ApiOperation(value = "获取充电桩列表")
    @GetMapping("/getChargingPileList")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<List<PlotPileTree>> getChargingPileList(@ApiParam("搜索关键字（设备编号或地址）") @RequestParam(value = "keWord", required = false) String keWord) {
        logger.info("获取充电桩分页列表");
        Map<String, String> params = new HashMap<>();
        params.put("userId", getUserId().toString());
        //设备编号
        params.put("keyWord", keWord);
        List<PlotPileTree> datas = chargingPileService.getPileTreeList(params);
        return Response.ok(datas);
    }

    @ApiOperation(value = "根据id获取充电桩信息")
    @GetMapping("/getPileById")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<SysPileResult> getPile(@ApiParam("充电桩id") @RequestParam(value = "id") Integer id) {

        Map<String, String> param = new HashMap();
        param.put("id", id + "");
        List<SysPileResult> pageList = pileMapper.getPilePageList(null, param);

        return Response.ok(CollectionUtils.isEmpty(pageList) ? null : pageList.get(0));
    }

    /* @MethodName:
     * @Description: 设备添加
     * @Param:
     * @Return:
     * @Author: yangbin
     * @Date: 2019/9/5
     **/
    @ApiOperation(value = "充电桩添加")
    @PostMapping("/addPile")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response add(ChargingPile chargingPile) {
        logger.info("添加充电桩");
        if (!StringUtils.isNumeric(chargingPile.getId()) || chargingPile.getId().length() != 8) {
            return Response.error(-1, "充电桩编号需为8位数字，请检查！");
        }
        /*session中获取用户名*/
        chargingPile.setUserId(getUserId().toString());
        ChargingPile validPile = chargingPileService.getPile(Integer.parseInt(chargingPile.getId()));
        if (validPile != null) {
            return Response.error(-1, "充电桩编号重复，请检查！");
        }
        chargingPileService.add(chargingPile);
        return Response.ok();
    }

    /* @MethodName:
     * @Description: 设备修改
     * @Param:
     * @Return:
     * @Author: yangbin
     * @Date: 2019/9/5
     **/
    @ApiOperation(value = "充电桩编辑", tags = {"2 充电桩管理"}, notes = "注意问题点")
    @PostMapping("/editPile")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response edit(ChargingPile chargingPile) {
        logger.info("修改充电桩，充电桩编号:" + chargingPile.getId());
        chargingPileService.edit(chargingPile);
        return Response.ok();
    }


    @ApiOperation(value = "根据小区代码获取充电桩", tags = {"2 充电桩管理"}, notes = "注意问题点")
    @GetMapping("/getPilesByPlot")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<List<PileResult>> getPilesByPlot(@ApiParam("小区Id") @RequestParam("plotId") int plotId) {
        logger.info("根据小区代码获取充电桩，小区代码：" + plotId);
        Integer userId = getUserId();
        List<PileResult> piles = chargingPileService.getPilesByPlotAndUserId(plotId, userId);
        return Response.ok(piles);
    }

    @ApiOperation("设置充电桩规则")
    @GetMapping("setPileRule")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response setPileRule(@ApiParam("充电桩编号") @RequestParam("Id") String Id,
                                @ApiParam("公众号规则ID") @RequestParam("wxRuleId") String wxRuleId,
                                @ApiParam("卡规则ID") @RequestParam("cardRuleId") String cardRuleId) {


        return chargingPileService.setPileRule(Id, wxRuleId, cardRuleId);

    }

}
