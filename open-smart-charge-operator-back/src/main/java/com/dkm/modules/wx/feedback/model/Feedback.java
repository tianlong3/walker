
package com.dkm.modules.wx.feedback.model;

import com.baomidou.mybatisplus.annotation.IdType;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @ClassName Feedback
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-13 19:40
 * @Version V1.0
 **/

@Getter
@Setter
public class Feedback {



    /**
     * Id
     * 
     */
    private Integer id;
    /**
     * 微信open_id
     * 
     */
    private String openId;
    /**
     * 用户手机号
     * 
     */
    private String mobile;
    /**
     * 问题类型
     * 
     */
    private String type;
    /**
     * 问题描述
     * 
     */
    private String rmk;
    /**
     * 设备编号
     * 
     */
    private String deviceNo;
    /**
     * 现场图片（路径)
     * 
     */
    private String imgUrl;
    /**
     * 批注
     * 
     */
    private String comment;
    /**
     * 处理人编号
     * 
     */
    private String handler;
    /**
     * 处理状态：0：待处理，1：处理完成
     * 
     */
    private int state;
    /**
     * 提交时间
     * 
     */
    private Date createTime;
    /**
     * 处理时间
     * 
     */
    private Date handleTime;
}
