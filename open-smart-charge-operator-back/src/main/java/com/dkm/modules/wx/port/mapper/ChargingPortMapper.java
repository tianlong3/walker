        
package com.dkm.modules.wx.port.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.pile.model.DeviceState;
import com.dkm.modules.wx.port.model.ChargingPort;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @ClassName ChargingPortMapper
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-05 13:52
 * @Version V1.0
 **/
@Repository
public interface ChargingPortMapper  extends BaseMapper<ChargingPort> {

    List<ChargingPort> listChargingPorts(@Param("p") Map<String,String> params, Page<ChargingPort> page);

    /**
     *  插入设备操作状态
     * @param deviceState
     * @return
     */
    Integer insertDeviceState(DeviceState deviceState);

    // 查询该充电桩ID所属的端口
    List<ChargingPort> queryPortByCode(@Param("deviceid") String deviceid);
}

