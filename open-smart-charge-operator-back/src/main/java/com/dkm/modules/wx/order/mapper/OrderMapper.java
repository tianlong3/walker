
package com.dkm.modules.wx.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.finance.model.FinanceResult;
import com.dkm.modules.sys.operator.model.OperatorRoyaltyRecord;
import com.dkm.modules.sys.statistics.model.SalesData;
import com.dkm.modules.sys.statistics.model.SalesDetail;
import com.dkm.modules.sys.user.model.CustomerBalance;
import com.dkm.modules.wx.cashrecord.model.OperatorBalance;
import com.dkm.modules.wx.order.model.*;
import com.dkm.modules.wx.port.model.ChargingPort;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


/**
 * @ClassName ChargingOrderMapper
 * @Description:
 * @Author yangbin
 * @Date 2019-09-09 23:38
 * @Version V1.0
 **/
@Repository
public interface OrderMapper extends BaseMapper<ChargingOrder> {

    List<OrderResult> getOrderList(@Param("params")Map<String,Object> params,Page<OrderResult> page);

    Map<String, Object> getOperateData( @Param("userId")Integer  userId);

    List<AddressReport> getAddressStatistics(Map<String,Object> params);

    Map<String, Object> getDayProfit(@Param("order") OrderQueryForm orderQueryForm);

    MonProfit getMonProfit(@Param("userId")Integer userId);

    OrderResult getOrderDetail(@Param("orderNumber")String orderNumber);

    List<FinanceResult> getFinanceList(@Param("p")Map<String, Object> params, Page<FinanceResult> page);

    Map<String, Object> getSumFinace();

    List<Profit> getProfits(@Param("userId")Integer userId);

    Map<String, Object> getUsersAndSales();

    SalesData getSalesData();

    List<SalesDetail> getSalesDetail(Page<SalesDetail> page);

    /**
     * Grain Rain 统计收益
     * @param params
     * @return
     */
    List<SalesDetail> getSalesDetailParams(@Param("pd")Map<String, Object> params);

    /**
      * 功能描述：根据订单id 查询订单信息
      * @param orderId 订单id
      * @return com.dkm.modules.wx.order.model.OrderResult 订单结果
      * @Author: guoliangbo
      * @Date: 2019/12/12 15:22
      */
    OrderResult getOrderDetailById(Integer orderId);

    /**
      * 功能描述：查询用户余额
      * @param openId 微信openId
      * @return com.dkm.modules.sys.user.model.CustomerBalance
      * @Author: guoliangbo
      * @Date: 2019/12/12 16:33
      */
    CustomerBalance queryUserBalance(@Param("openId")String openId);

    /**
      * 功能描述：插入用户余额数据
      * @param balance 用户余额
      * @return java.lang.Integer
      * @Author: guoliangbo
      * @Date: 2019/12/12 16:35
      */
    Integer insertUserBalance(CustomerBalance balance);

    /**
      * 功能描述：更新用户余额
      * @param balance 用户余额信息
      * @return java.lang.Integer 影响行数
      * @Author: guoliangbo
      * @Date: 2019/12/12 16:37
      */
    Integer updateUserBalance(CustomerBalance balance);

    /**
      * 功能描述：更新订单状态
      * @param order 订单信息
      * @return java.lang.Integer 影响行数
      * @Author: guoliangbo
      * @Date: 2019/12/12 16:39
      */
    Integer updateOrderRefundStatus(OrderResult order);

    /**
      * 功能描述：根据充电桩id查询代理商余额表
      * @param parkId 充电桩id
      * @Return:com.dkm.modules.wx.cashrecord.model.OperatorBalance
      * @Author: Guo Liangbo
      * @Date:2019/12/14 10:03
      */
    OperatorBalance queryOperatorBalance(@Param("parkId")String parkId);

    /**
      * 功能描述：更新代理商余额
      * @param operatorBalance 代理商余额
      * @Return:java.lang.Integer 影响行数
      * @Author: Guo Liangbo
      * @Date:2019/12/14 10:04
      */
    Integer updateOperatorBalance(OperatorBalance operatorBalance);


    ChargingPort queryPortInfo(@Param("params")Map<String,Object> params);


    void updateOrderParkCodeAndPortCode(@Param("params") Map<String,Object> params);

    void updatePortState(String parklockId);

    void updateDownload(@Param("params") Map<String,Object> params);

    /**
      * 功能描述：查询此订单的分成记录
      * @param operatorRoyaltyRecord 分成记录,用订单id查询
      * @return com.dkm.modules.sys.operator.model.OperatorRoyaltyRecord 分成记录
      * @Author: guoliangbo
      * @Date: 2020/1/8 14:26
      */
    OperatorRoyaltyRecord queryOperatorRoyaltyRecord(OperatorRoyaltyRecord operatorRoyaltyRecord);

    /**
      * 功能描述：插入分成记录
      * @param operatorRoyaltyRecord 插入分成记录
      * @return java.lang.Integer 影响行数
      * @Author: guoliangbo
      * @Date: 2020/1/8 14:28
      */
    Integer insertOperatorRoyaltyRecord(OperatorRoyaltyRecord operatorRoyaltyRecord);

    /**
     * 功能描述：将新的端口更改成使用状态
     * @Author: LIWENHUI
     * @Date: 2020/2/29 11:16
     */
    void portChangedToUse(@Param("params") Map<String,String> params);

    void updateOrderStatus(@Param("params") Map<String,Object> params);
    void updateChargingPortByOrderId(@Param("params") Map<String,Object> params);
}

