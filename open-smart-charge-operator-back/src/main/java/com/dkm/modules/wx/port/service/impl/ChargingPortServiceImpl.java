
package com.dkm.modules.wx.port.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.Response;
import com.dkm.constant.Constant;
import com.dkm.modules.sys.pile.model.DeviceState;
import com.dkm.modules.wx.pile.model.ChargingPile;
import com.dkm.modules.wx.port.mapper.ChargingPortMapper;
import com.dkm.modules.wx.port.model.ChargingPort;
import com.dkm.modules.wx.port.service.ChargingPortService;
import com.dkm.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName ChargingPortServiceImpl
 * @Description: 实现类
 * @Author yangbin
 * @Date 2019-09-05 13:52
 * @Version V1.0
 **/
@Service
public class ChargingPortServiceImpl implements ChargingPortService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChargingPortServiceImpl.class);

    @Autowired
    private ChargingPortMapper chargingPortMapper;

    @Override
    public void addPort(ChargingPort chargingPort) {
        chargingPort.setCreatetime(new Date());
        chargingPortMapper.insert(chargingPort);
    }

    @Override
    public IPage<ChargingPort> getPortList(Page<ChargingPort> page, Map<String, String> params) {
        List<ChargingPort> chargingPorts = chargingPortMapper.listChargingPorts(params, page);
        page.setRecords(chargingPorts);
        return page;
    }

    @Override
    public void editPort(ChargingPort chargingPort) {
        chargingPortMapper.updateById(chargingPort);
    }

    @Override
    /**
     *@描述 删除端口
     *@参数 [id 端口ID]
     *@返回值 void
     *@创建人 yangbin
     *@创建时间 2019/9/21
     */
    public void delPort(Integer id) {
        ChargingPort chargingPort = chargingPortMapper.selectById(id);
        if (StringUtils.equals("Y", chargingPort.getState())) {
            throw new IllegalStateException("端口已占用，不能删除");
        }
        chargingPortMapper.deleteById(id);
    }

    @Override
    /**
     *@描述 给充电桩添加端口，默认20个端口
     *@参数 [chargingPile]
     *@返回值 void
     *@创建人 yangbin
     *@创建时间 2019/10/29
     */
    public void addPortForPile(ChargingPile chargingPile) {
        Integer portCount = chargingPile.getPortCount();
        if (null == portCount) {
            portCount = 20;
        }
        for (int i = 1; i <= portCount; i++) {
            ChargingPort port = new ChargingPort();
            port.setParkid(Integer.parseInt(chargingPile.getId()));
            port.setState("N");
            port.setDeviceid("88-" + i);
            port.setName(i + "号端口");
            port.setCreatetime(new Date());
            chargingPortMapper.insert(port);
        }
    }


    @Override
    public Response switchPort(Integer id, Integer type) {

        if (!Constant.SWITCH_CLOSE.equals(type) && !Constant.SWITCH_OPEN.equals(type)) {
            return Response.error(-2, "端口操作状态值不对！");
        }
        ChargingPort chargingPort = chargingPortMapper.selectById(id);
        if (null == chargingPort) {
            return Response.error(-3, "端口不存在！");
        }
        DeviceState deviceState = new DeviceState();
        deviceState.setDeviceId(chargingPort.getParkid().toString());
        deviceState.setDevicePortNum(chargingPort.getDeviceid());
        deviceState.setSwitchS(type.toString());
        chargingPortMapper.insertDeviceState(deviceState);

        return Response.ok();
    }

    @Override
    public Response allPort(String code, Integer type) {
        // 查询该充电桩ID所属的端口 // iter for循环快捷方式
        List<ChargingPort> chargingPorts = chargingPortMapper.queryPortByCode(code);
        if(chargingPorts!=null && chargingPorts.size()>0){
            for (ChargingPort chargingPort : chargingPorts) {
                if (!Constant.SWITCH_CLOSE.equals(type) && !Constant.SWITCH_OPEN.equals(type)) {
                    return Response.error(-2, "提示：端口操作状态值不对");
                }
//            ChargingPort chargingPort = chargingPortMapper.selectById(id);
                if (null == chargingPort) {
                    return Response.error(-3, "提示：端口不存在");
                }
                DeviceState deviceState = new DeviceState();
                deviceState.setDeviceId(chargingPort.getParkid().toString());
                deviceState.setDevicePortNum(chargingPort.getDeviceid());
                deviceState.setSwitchS(type.toString());
                // 每次下发暂停一下
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                chargingPortMapper.insertDeviceState(deviceState);
            }
            return Response.ok();
        }else {
            return Response.error(-1, "提示：充电桩编号不存在");
        }

    }


}