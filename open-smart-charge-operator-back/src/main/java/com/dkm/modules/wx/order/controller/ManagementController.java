package com.dkm.modules.wx.order.controller;

import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.modules.sys.statistics.model.SalesDetail;
import com.dkm.modules.sys.statistics.service.StatisticsService;
import com.dkm.modules.wx.order.model.AddressReport;
import com.dkm.modules.wx.order.model.MonProfit;
import com.dkm.modules.wx.order.model.OrderQueryForm;
import com.dkm.modules.wx.order.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName ManagementController
 * @Description: 经营管理控制器
 * @Author 杨膑
 * @Date 2019/9/14
 * @Version V1.0
 **/
@Api(tags = "3 经营管理")
@RestController
@RequestMapping("/management")
public class ManagementController extends BaseController {

    private final static Logger logger = LoggerFactory.getLogger(ManagementController.class);
    @Autowired
    private OrderService chargingOrderService;
    @Autowired
    private StatisticsService statisticsService;

    //Grain Rain 2020年3月11日21:41:41 销售数据--统计充电桩销售数据
    @ApiOperation(value = "销售数据--统计充电桩销售数据",notes = "count:数量，days : 时间，totlePrice：销售额")
    @GetMapping("getSalesDetailByParams")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response getSalesDetailByParams(
            @ApiParam("充电桩ID")@RequestParam(value = "parkid",required = true) String parkid,
            @ApiParam("近几天")@RequestParam(value = "day",required = false)Integer day,
            @ApiParam("开始时间（yyyy-mm-dd）")@RequestParam(value = "startTime",required = false)String startTime,
            @ApiParam("结束时间(yyyy-mm-dd)")@RequestParam(value = "endTime",required = false)String endTime){
        logger.info("获取统计充电桩销售数据");
        Map<String,Object> params = new HashMap<>();
        params.put("startTime",startTime);
        params.put("endTime",endTime);
        params.put("day",day);
        params.put("parkid",parkid);
        List<SalesDetail> data = statisticsService.getSalesDetailParams(params);
        return Response.ok(data);
    }

    @ApiOperation("经营管理首页数据")
    @GetMapping("/getOperateData")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response getOperateData(){
        logger.info("获取经营管理首页数据");
        /*获取本月收益、今日收益、订单数数据*/
        Map<String,Object> operateData = chargingOrderService.getOperateData(getUserId());
        return Response.ok(operateData);

    }

    @ApiOperation("地址统计列表")
    @GetMapping("/addressStatistics")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<List<AddressReport>> addressStatistics(@ApiParam("开始时间，可为空")@RequestParam(value = "startTime",required = false) String startTime,
                                                           @ApiParam("开始时间，可为空")@RequestParam(value = "plotName",required = false) String plotName,
                                                           @ApiParam("结束时间，可为空")@RequestParam(value = "endTime",required = false) String endTime){
        logger.info("地址统计列表数据");
        Map<String,Object> params = new HashMap<>();
        /*代理商*/
        params.put("userId",getUserId());
        /*开始时间 结束时间*/
        params.put("startTime",startTime);
        params.put("endTime",endTime);
        params.put("plotName",plotName);
        /*获取地址统计列表数据*/
        List<AddressReport> addressStatisticsData = chargingOrderService.getAddressStatistics(params);
        return Response.ok(addressStatisticsData);


    }

    @ApiOperation("今日收益")
    @GetMapping("/getDayProfit")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response getDayProfit(@Valid OrderQueryForm orderQueryForm){
        logger.info("今日收益");
        /*设置当前代理商*/
        orderQueryForm.setUserId(getUserId().toString());
        /*获取数据*/
        Map<String,Object> DayProfit =chargingOrderService.getDayProfit(orderQueryForm);
        return Response.ok(DayProfit);
    }

    @ApiOperation("本月收益")
    @GetMapping("/getMonProfit")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<MonProfit> getMonProfit(){
        logger.info("本月收益");
        MonProfit monProfit =chargingOrderService.getMonProfit(getUserId());
        return Response.ok(monProfit);


    }

}
