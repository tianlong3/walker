
package com.dkm.modules.wx.feedback.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName 问题反馈返回结果
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-13 19:40
 * @Version V1.0
 **/

@Getter
@Setter
public class FeedbackResult {

    @ApiModelProperty("问题id")
    private Integer id;

    @ApiModelProperty("用户昵称")
    private String userName;

    @ApiModelProperty("用户手机号")
    private String mobile;

    @ApiModelProperty("问题类型")
    private String type;

    @ApiModelProperty("问题描述")
    private String rmk;

    @ApiModelProperty("设备编号")
    private String deviceNo;


    @ApiModelProperty("现场图片（路径)")
    private String imgUrl;

    @ApiModelProperty("批注")
    private String comment;

    @ApiModelProperty("处理人")
    private String handler;


    @ApiModelProperty("处理状态")
    private String state;

    @ApiModelProperty("提交时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty("处理时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date handleTime;

    @ApiModelProperty("设备名称")
    private String PKNAME;

    @ApiModelProperty("场地")
    private String plot;

    @ApiModelProperty("代理商名字")
    private String realName;

    @ApiModelProperty("代理商Id")
    private String operatorId;
}
