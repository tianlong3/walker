
package com.dkm.modules.wx.cashrecord.model;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName OperatorCashRecord
 * @Description: 提现记录
 * @Author yangbin
 * @Date 2019-09-22 09:46
 * @Version V1.0
 **/

@Getter
@Setter
public class OperatorCashRecord {


    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("代理商ID")
    private Integer operaterId;

    @ApiModelProperty("金额")
    private Float amount;

    @ApiModelProperty("状态")
    private Integer status;

    @ApiModelProperty("微信提现订单号")
    private String orderNumber;
    /**
     * 提现时间
     * 
     */
    @ApiModelProperty("提现时间")
    private Date createTime;

    @ApiModelProperty(value = "提现失败原因",hidden = true)
    private String errorReason;
}
