package com.dkm.modules.wx.user.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @描述  用户数据返回结果
 * @创建时间 2019/10/8
 * @创建人 yangbin
 */
@Data
@ApiModel("UserData：用户数据")
public class UserData {

    @TableId
    @ApiModelProperty("主键ID")
    private Integer id;

    @ApiModelProperty("用户头像")
    private String headImage;

    @ApiModelProperty("用户昵称")
    private String userName;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("性别")
    private String sex;

    @ApiModelProperty("卡余额")
    private Float cardBalance;

    @ApiModelProperty("虚拟余额")
    private Float lineBalance;

    @ApiModelProperty("累计充值金额")
    private Float rechargeSum;

}
