        
package com.dkm.modules.wx.feedback.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.constant.Constant;
import com.dkm.modules.wx.feedback.model.FeedbackResult;
import com.dkm.modules.wx.feedback.model.QueryFeedBack;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.dkm.modules.wx.feedback.model.Feedback;
import com.dkm.modules.wx.feedback.service.FeedbackService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName FeedbackController
 * @Description: 问题反馈 控制器
 * @Author yangbin
 * @Date 2019-09-13 19:40
 * @Version V1.0
 **/
@Api(tags = "8 问题反馈")
@RestController
@RequestMapping("/feedback")
public class FeedbackController extends BaseController{

    private final static Logger logger = LoggerFactory.getLogger(FeedbackController.class);
    @Autowired
    private FeedbackService feedbackService;


    @ApiOperation(value="获取问题反馈列表")
    @GetMapping("/getFeedBackList")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response<IPage<FeedbackResult>> getFeedBackList(@ApiParam("页数") @RequestParam("pageNo")int pageNo,
                                                           @ApiParam("每页显示数") @RequestParam("pageSize")int pageSize,
                                                           QueryFeedBack queryFeedBack){
        logger.info("获取问题反馈列表");
        queryFeedBack.setUserId(getUserId().toString());

        Page<FeedbackResult> page = new Page<>(pageNo,pageSize);
        Map<String,Object> params = new HashMap<>();
        params.put("FeedBack",queryFeedBack);
        IPage<FeedbackResult> FeedBackList = feedbackService.getFeedBackList(params,page);
        return Response.ok(FeedBackList);

    }

    @ApiOperation("处理问题")
    @PostMapping("/doFeedBack")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response doFeedBack(@ApiParam("问题ID") @RequestParam("id") String id,
                               @ApiParam("批注") @RequestParam("comment") String comment,
                               @ApiParam("处理状态") @RequestParam("state") int state){
        return feedbackService.doFeedBack(id,comment,getUserId(),state);
    }

    @ApiOperation("获取问题反馈信息")
    @PostMapping("/getFeedBack")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response getFeedBack(@ApiParam("问题ID") @RequestParam("id") String id){
        Map<String,Object> data = feedbackService.getFeedBack(id);
        return Response.ok(data);
    }

}
