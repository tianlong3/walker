        
package com.dkm.modules.wx.cashrecord.service;

import com.dkm.modules.wx.cashrecord.model.OperatorBalance;

import java.util.List;
import java.util.Map;


/**
 * @ClassName OperatorBalance
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-22 17:58
 * @Version V1.0
 **/
public interface OperatorBalanceService {


    /*通过用户账号查询余额数据*/
    OperatorBalance getBalanceByUserId(Integer userId);


    void updateBlance(OperatorBalance operatorBalance);

    void sendSms(String mobile, String code);
}