package com.dkm.modules.wx.card.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName CardUseRecods
 * @Description: 卡使用记录，作返回数据使用
 * @Author 杨膑
 * @Date 2019/9/23
 * @Version V1.0
 **/
@ApiModel("卡使用记录")
@Getter
@Setter
public class UseRecods {

    /**
     * 主键ID
     */
    @ApiModelProperty("主键ID")
    private int id;

    @ApiModelProperty(value = "姓名")
    private String userName;

    @ApiModelProperty("金额")
    private Float amount;

    @ApiModelProperty("开始时间")
    private String startTime;

    @ApiModelProperty("结束时间")
    private String endTime;

    @ApiModelProperty("预计时间")
    private Float hour;

    @ApiModelProperty("状态")
    private String status;

    @ApiModelProperty("实际时间")
    private String realHour;

    @ApiModelProperty("充电桩id")
    private String parkId;

    @ApiModelProperty("电卡标号")
    private String cardNo;

    @ApiModelProperty("订单号")
    private String orderNumber;

    @ApiModelProperty("订单金额")
    private String orderGold;

    @ApiModelProperty("订单状态")
    private String orderState;

    @ApiModelProperty("订单类型")
    private String orderType;
}
