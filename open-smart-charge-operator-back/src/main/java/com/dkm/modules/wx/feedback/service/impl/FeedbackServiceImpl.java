        
package com.dkm.modules.wx.feedback.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.Response;
import com.dkm.dataScope.DataScope;
import com.dkm.modules.wx.feedback.mapper.FeedbackMapper;
import com.dkm.modules.wx.feedback.model.Feedback;
import com.dkm.modules.wx.feedback.model.FeedbackResult;
import com.dkm.modules.wx.feedback.service.FeedbackService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName FeedbackServiceImpl
 * @Description: 实现类
 * @Author yangbin
 * @Date 2019-09-13 19:40
 * @Version V1.0
 **/
@Service
public class FeedbackServiceImpl implements FeedbackService {

        private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackServiceImpl.class);

        @Autowired
        private FeedbackMapper feedbackMapper;

        @Override
        @DataScope(alias = "a.operatorId")
        public IPage<FeedbackResult> getFeedBackList(Map<String, Object> params, Page<FeedbackResult> page) {
                /*查询问题反馈数据*/
                List<FeedbackResult> feedBackLists = feedbackMapper.getFeedBackList(params,page);
                /*组装数据成page类型*/
                page.setRecords(feedBackLists);
                return page;
        }

        @Override
        public Response doFeedBack(String id, String comment, Integer userId, int state) {
                if(0==state){
                    return Response.error(-4,"问题反馈不能修改为待处理！");
                }
                Feedback feedback =  feedbackMapper.selectById(id);
                if (feedback==null){
                    return Response.error(-5,"该id的问题反馈不存在！");
                }
                //*设置处理人*/
                feedback.setHandler(userId.toString());
                feedback.setComment(comment);
                /*设置状态*/
                feedback.setState(state);
                /*设置处理时间*/
                feedback.setHandleTime(new Date());
                /*存库*/
                Integer num =feedbackMapper.updateById(feedback);
                if(num > 0){
                    return Response.ok();
                }else {
                    return Response.error();
                }
        }

        @Override
        /**
         *@描述   获取问题反馈信息
         *@参数  [id 问题反馈编号]
         *@返回值  void
         *@创建人  yangbin
         *@创建时间  2019/10/13
         */
        public Map<String,Object>  getFeedBack(String id) {
                return feedbackMapper.getFeedBack(id);
                
        }
}