        
package com.dkm.modules.wx.feedback.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.Response;
import com.dkm.modules.wx.feedback.model.FeedbackResult;

import java.util.Map;


/**
 * @ClassName Feedback
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-13 19:40
 * @Version V1.0
 **/
public interface FeedbackService {

    Response doFeedBack(String id, String comment, Integer userId, int state);

    Map<String,Object>  getFeedBack(String id);

    IPage<FeedbackResult> getFeedBackList(Map<String, Object> params, Page<FeedbackResult> page);
}