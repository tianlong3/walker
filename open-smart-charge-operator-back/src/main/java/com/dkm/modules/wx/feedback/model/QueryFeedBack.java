package com.dkm.modules.wx.feedback.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName querFeedBack
 * @Description:
 * @Author 杨膑
 * @Date 2019/9/13
 * @Version V1.0
 **/
@Getter
@Setter
@ApiModel(value = "问题反馈")
public class QueryFeedBack {

    @ApiModelProperty(value = "处理状态 0：待处理，1：处理完成 2: 忽略")
    private String state;

    @ApiModelProperty(value = "开始时间")
    private String startTime;

    @ApiModelProperty(value = "结束时间")
    private String endTime;

    @ApiModelProperty(value = "问题类型,见公共代码parentCode=4")
    private String type;

    @ApiModelProperty(value = "设备编号")
    private String deviceNo;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    /*代理商ID*/
    @ApiModelProperty(hidden = true)
    private String userId;
}
