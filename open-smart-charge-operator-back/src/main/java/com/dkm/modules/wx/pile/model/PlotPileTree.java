package com.dkm.modules.wx.pile.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @描述 小区充电桩 树状数据
 * @创建时间 2019/10/23 19:51
 * @创建人 yangbin
 */
@Data
public class PlotPileTree {
    @ApiModelProperty("小区ID")
    private Integer poltId;

    @ApiModelProperty("小区名称")
    private String poltNm;

    @ApiModelProperty("小区名称")
    private List<PileResult> pileResults;
}
