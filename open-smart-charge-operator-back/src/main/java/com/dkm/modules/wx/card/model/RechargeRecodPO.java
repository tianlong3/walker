package com.dkm.modules.wx.card.model;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName RechargeRecods
 * @Description: 充值记录，作返回数据使用
 * @Author 杨膑
 * @Date 2019/9/23
 * @Version V1.0
 **/
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName("c_recharge_record")
public class RechargeRecodPO {

    //充值记录表主键ID
    private Integer id;

    // 微信OpenId
    private String openId;

    // 充值卡ID
    private Integer cardId;


    private String rechargeOpenId;


    // 收支类型 0 充值 1 消费（支出）2 赠送 3 系统充值
    private Integer type;

    // 数据类型，1 卡  2 账户
    private Integer dataType;

    // 本次操作金额
    private BigDecimal quantity;

    // 充值编号，全局唯一，当做充值订单号
    private String rechargeNumber;

    // 状态：0 未启用；1正常；2删除
    private Integer status;

    // 默认为空，用户操作，如存在数据，即为管理员ID
    private Integer createBy;

    // 创建时间
    private Date createTime;

}
