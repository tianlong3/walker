package com.dkm.modules.wx.city.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.sys.plot.model.PlotResult;
import com.dkm.modules.wx.city.model.PlotModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @描述 小区Mapper
 * @创建时间 2019/10/23 21:08
 * @创建人 yangbin
 */
@Repository
public interface PlotMapper extends BaseMapper<PlotModel> {
    List<PlotResult> getPlotList(@Param("p") Map<String, Object> params, Page<PlotResult> page);
}
