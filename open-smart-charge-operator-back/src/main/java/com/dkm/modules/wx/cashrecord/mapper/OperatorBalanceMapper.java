        
package com.dkm.modules.wx.cashrecord.mapper;

import org.springframework.stereotype.Repository;
import com.dkm.modules.wx.cashrecord.model.OperatorBalance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * @ClassName OperatorBalanceMapper
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-22 17:58
 * @Version V1.0
 **/
@Repository
public interface OperatorBalanceMapper  extends BaseMapper<OperatorBalance> {

}

