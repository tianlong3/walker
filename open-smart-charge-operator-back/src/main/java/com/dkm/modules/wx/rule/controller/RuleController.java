        
package com.dkm.modules.wx.rule.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.modules.wx.rule.model.CustomPrice;
import com.dkm.modules.wx.rule.model.Rule;
import com.dkm.modules.wx.rule.service.CustomPriceService;
import com.dkm.modules.wx.rule.service.RuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName RuleController
 * @Description: 规则管理控制器
 * @Author yangbin
 * @Date 2019-09-20 15:29
 * @Version V1.0
 **/
@RestController
@RequestMapping("/rule")
@Api(tags = {"7 规则管理"})
public class RuleController extends BaseController{

    private final static Logger logger = LoggerFactory.getLogger(RuleController.class);
    @Autowired
    private RuleService ruleService;
    @Autowired
    private CustomPriceService customPriceService;

    @PostMapping("/addRule")
    @ApiOperation(value = "新增规则")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response addRule(@Valid @ApiParam("规则数据") Rule rule){
        /*设置当前操作人员*/
        rule.setUserId(getUserId());
        /*添加规则*/
        return ruleService.addRule(rule);

    }

    @GetMapping("/copyRule")
    @ApiOperation(value = "复制规则")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response copyRule(@ApiParam("被复制的规则ID")@RequestParam("id") Integer id,
                             @ApiParam("新的规则名称")@RequestParam("changeName") String changeName){
        logger.info("代理商-复制规则");
        Rule rule =new Rule();
        rule.setId(id);
        rule.setChangeName(changeName);
        /*设置当前操作人员*/
        rule.setUserId(getUserId());
        /*添加规则*/
        return ruleService.copyRule(rule);

    }

    @GetMapping("/getRuleList")
    @ApiOperation(value = "规则列表数据（分页）")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response getRuleList(@ApiParam("页数") @RequestParam("pageNo")int pageNo,
                                @ApiParam("每页显示数") @RequestParam("pageSize")int pageSize,@ApiParam("规则类型(代码表parentCode=10)")@RequestParam(value = "ruleType",required = false)String ruleType){
        /*根据代理商用户名查询规则列表*/
        Map<String,Object> params = new HashMap<>(8);
        params.put("userId",getUserId());
        params.put("ruleType",ruleType);
        Page<Rule> page = new Page<>(pageNo,pageSize);
        IPage<Rule> ruleList = ruleService.getRuleListByUserNm(params,page);
        return Response.ok(ruleList);
    }

    @GetMapping("/getRule")
    @ApiOperation(value = "规则数据")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response getRule( @ApiParam("规则ID")@RequestParam("id") int id){
        /*根据规则ID获取规则数据*/
        return ruleService.getRule(id);
    }

    @PostMapping("/editeRule")
    @ApiOperation(value = "编辑规则")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response editeRule(@Valid @ApiParam("规则数据") Rule rule){
        /*修改规则*/
         ruleService.editeRule(rule);
        return Response.ok();
    }

    @DeleteMapping("/deleteRule")
    @ApiOperation(value = "删除规则")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response deleteRule(@ApiParam("规则ID") @RequestParam("id")int id){
        return ruleService.deleteRule(id);
    }


    @GetMapping("/getRulePriceList")
    @ApiOperation(value = "规则对应的价格列表数据")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response getRulePriceList(@ApiParam("规则ID")@RequestParam("id") int id){
        List<Map<String,Object>> rulePriceList = ruleService.getRulePriceListById(id);
        return  Response.ok(rulePriceList);
    }


    @PostMapping("/addCustomPrice")
    @ApiOperation(value = "添加规则价格")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response addCustomPrice( @Valid @ApiParam("规则价格数据")
                                               CustomPrice customPrice){

        /*添加规则价格*/
        return customPriceService.addCustomPrice(customPrice);
    }

    @GetMapping("/getCustomPrice")
    @ApiOperation(value = "规则价格数据")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response getCustomPrice( @ApiParam("规则价格ID")@RequestParam("id") int id,
                                    @ApiParam("搜索关键字")@RequestParam("keyWord") String keyWord){
        Map<String,Object> params = new HashMap<>();
        params.put("id",id);
        params.put("keyWord",keyWord);
        CustomPrice customPrice = customPriceService.getCustomPrice(params);
        return Response.ok(customPrice);
    }

    @PostMapping("/editCustomPrice")
    @ApiOperation(value = "修改规则价格")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response editCustomPrice(@Valid @ApiParam("规则价格数据")
                                                CustomPrice customPrice){
        return customPriceService.editCustomPrice(customPrice);

    }

    @DeleteMapping("/delCustomPrice")
    @ApiOperation(value = "删除规则价格")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response delCustomPrice(@ApiParam("规则价格ID") @RequestParam("id")Integer id){
        return customPriceService.delCustomPrice(id);
    }

}
