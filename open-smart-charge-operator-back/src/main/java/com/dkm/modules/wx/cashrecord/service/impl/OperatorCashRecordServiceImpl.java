        
package com.dkm.modules.wx.cashrecord.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.wx.cashrecord.mapper.OperatorCashRecordMapper;
import com.dkm.modules.wx.cashrecord.model.OperatorCashRecord;
import com.dkm.modules.wx.cashrecord.service.OperatorCashRecordService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @ClassName OperatorCashRecordServiceImpl
 * @Description: 提现记录实现类
 * @Author yangbin
 * @Date 2019-09-22 09:46
 * @Version V1.0
 **/
@Service
public class OperatorCashRecordServiceImpl implements OperatorCashRecordService {

        private static final Logger LOGGER = LoggerFactory.getLogger(OperatorCashRecordServiceImpl.class);

        @Autowired
        private OperatorCashRecordMapper operatorCashRecordMapper;

        @Override
        /**
         *@描述   获取提现记录
         *@参数  [page, params]
         *@返回值  com.baomidou.mybatisplus.core.metadata.IPage<java.util.Map<java.lang.String,java.lang.Object>>
         *@创建人  yangbin
         *@创建时间  2019/10/1
         */
        public IPage<Map<String, Object>> getCashRecords(Page<Map<String, Object>> page, Map<String, Object> params) {
            /*查询数据*/
            List<Map<String,Object>> cashRecords =  operatorCashRecordMapper.getCashRecords(params,page);
            /*组装 数据为page 类型*/
            page.setRecords(cashRecords);
            return page;
        }

    @Override
    /**
     *@描述   添加提现记录
     *@参数  [operatorCashRecord]
     *@返回值  void
     *@创建人  yangbin
     *@创建时间  2019/10/25
     */
    public void addCashRecord(OperatorCashRecord operatorCashRecord) {
        operatorCashRecordMapper.insert(operatorCashRecord);
    }
}