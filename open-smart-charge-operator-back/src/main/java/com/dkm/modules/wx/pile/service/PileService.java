package com.dkm.modules.wx.pile.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.Response;
import com.dkm.modules.sys.map.model.PlotPile;
import com.dkm.modules.sys.pile.model.SysPileResult;
import com.dkm.modules.wx.pile.model.ChargingPile;
import com.dkm.modules.wx.pile.model.PileResult;
import com.dkm.modules.wx.pile.model.PlotPileTree;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface PileService {

    /**
     * 获取充电桩分页列表
     * @param params 参数
     * @param page 分页信息
     * @return 分页数据
     */
    IPage<SysPileResult> getPage(Map<String,String> params, Page< SysPileResult> page);

    /**
     * 添加充电桩
     * @param chargingPile
     */
    void add(ChargingPile chargingPile);

    /**
    * 修改充电桩
    */
    void edit(ChargingPile chargingPile);

    /**
     * 根据小区代码获取充电桩
     * @param plotId
     * @return
     */
    List<PileResult> getPilesByPlot(int plotId);

    /**
     * 给充电桩设置规则
     * @param Id
     * @param ruleId
     * @param cardRuleId
     */
    Response setPileRule(String Id, String ruleId, String cardRuleId);

    List<PlotPile> getPlotPile();

    List<Map<String, Object>> statisticalPile(Map<String, Object> params);

    List<PlotPileTree> getPileTreeList(Map<String, String> params);

    void ExportExcel(Map<String, String> params, HttpServletResponse httpServletResponse);

    ChargingPile getPile(Integer id);

    /**
      * 功能描述：根据小区id和用户id  查询充电桩信息
      * @param plotId 小区id
      * @param userId 用户id
      * @Return:java.util.List<com.dkm.modules.wx.pile.model.PileResult> 充电桩信息
      * @Author: Guo Liangbo
      * @Date:2019/11/12 14:21
      */
    List<PileResult> getPilesByPlotAndUserId(int plotId, Integer userId);
}
