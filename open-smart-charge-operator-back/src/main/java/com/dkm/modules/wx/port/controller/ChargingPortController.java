
package com.dkm.modules.wx.port.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.Response;
import com.dkm.constant.Constant;

import com.dkm.modules.wx.port.model.ChargingPort;
import com.dkm.modules.wx.port.service.ChargingPortService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName ChargingPortController
 * @Description: 控制器
 * @Author yangbin
 * @Date 2019-09-05 13:52
 * @Version V1.0
 **/
@Api(tags="5 端口管理")
@RestController
@RequestMapping("/chargingport")
public class ChargingPortController extends BaseController {

    private final static Logger logger = LoggerFactory.getLogger(ChargingPortController.class);
    @Autowired
    private ChargingPortService chargingPortService;

    @ApiOperation("添加端口")
    @PostMapping("/addPort")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response addPort(@ApiParam ChargingPort chargingPort) {
        logger.info("添加端口");
        chargingPortService.addPort(chargingPort);
        return Response.ok();
    }


    @ApiOperation("编辑端口")
    @PostMapping("/editPort")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response editPort(@ApiParam ChargingPort chargingPort) {
        logger.info("编辑端口");
        chargingPortService.editPort(chargingPort);
        return Response.ok();
    }


    @ApiOperation("获取端口列表")
    @GetMapping("/getPortList")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response getPortList(@ApiParam("页数") @RequestParam("pageNo")int pageNo,
                                @ApiParam("每页显示数") @RequestParam("pageSize")int pageSize,
                                @ApiParam("充电桩ID") @RequestParam("id")String id,
                                @ApiParam("投放地址") @RequestParam(value = "pkaddress",required = false)String pkaddress) {

        logger.info("获取端口列表");
        /*组装查询参数*/
        Map<String, String> params = new HashMap<>();
        /*充电桩编号*/
        params.put("id", id);
        /*投放地址*/
        params.put("pkaddress", pkaddress); //投放地址
        //获取端口分页后列表
        Page<ChargingPort> page = new Page<>(pageNo,pageSize);
        IPage<ChargingPort> chargingPiles = chargingPortService.getPortList(page, params);
        return Response.ok(chargingPiles);
    }

    @ApiOperation("删除端口")
    @DeleteMapping("delPort")
    @ApiImplicitParam(paramType = "header",name = "token",value = "身份认证Token")
    public Response delPort(@ApiParam("端口ID")@RequestParam("id")Integer id){
        logger.info("删除端口");
        try {
            chargingPortService.delPort(id);
        }catch (Exception e){
            return Response.error(-1, e.getMessage());
        }
        return Response.ok();
    }

}
