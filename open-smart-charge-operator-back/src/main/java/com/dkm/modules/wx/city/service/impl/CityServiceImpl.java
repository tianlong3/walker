        
package com.dkm.modules.wx.city.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dkm.modules.wx.city.mapper.CityMapper;
import com.dkm.modules.wx.city.model.AllAddress;
import com.dkm.modules.wx.city.model.City;
import com.dkm.modules.wx.city.model.Plot;
import com.dkm.modules.wx.city.service.CityService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName CityServiceImpl
 * @Description: 实现类
 * @Author yangbin
 * @Date 2019-09-07 00:03
 * @Version V1.0
 **/
@Service
public class CityServiceImpl implements CityService {

        private static final Logger LOGGER = LoggerFactory.getLogger(CityServiceImpl.class);

        @Autowired
        private CityMapper cityMapper;

        @Override
        public List<City> getCityList(Integer pid) {
                QueryWrapper<City> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("Pid",pid);
                return cityMapper.selectList(queryWrapper);
        }

        @Override
        public List<Plot> getPoltListByRegion(Map<String,Object> params) {
                return cityMapper.getPoltListByRegion(params);
        }

        /**
         * 功能描述：获取所有省市区地址
         * @Return:com.dkm.modules.wx.city.model.AllAddress 结果地址
         * @Author: Guo Liangbo
         * @Date: 2019/11/7 23:08
         */
        @Override
        public AllAddress getAllAddress() {
                List<City> allCity = cityMapper.selectList(new QueryWrapper<>());
                AllAddress allAddress = new AllAddress();
                // 获取省份
                List<City> provinceList = new ArrayList<>();
                HashMap<Integer, String> provinceMap = new HashMap<>();
                // 获取城市
                List<City> cityList = new ArrayList<>();
                HashMap<Integer, String> cityMap = new HashMap<>();
                // 获取乡镇
                List<City> countryList = new ArrayList<>();
                HashMap<Integer, String> countryMap = new HashMap<>();

                //获取省份
                for (City city:allCity) {
                        if(0==city.getPid()){
                                provinceList.add(city);
                                provinceMap.put(city.getId(),city.getName());
                        }
                }
                allAddress.setProvince_list(provinceMap);
                for (City city:allCity) {
                        for (City province:provinceList){
                                if(city.getPid().equals(province.getId())){
                                        cityList.add(city);
                                        cityMap.put(city.getId(),city.getName());
                                }
                        }
                }
                allAddress.setCity_list(cityMap);
                for (City city:allCity) {
                        for (City cityParent:cityList){
                                if(city.getPid().equals(cityParent.getId())){
                                        countryList.add(city);
                                        countryMap.put(city.getId(),city.getName());
                                }
                        }
                }
                allAddress.setCounty_list(countryMap);
                return allAddress;
        }
}