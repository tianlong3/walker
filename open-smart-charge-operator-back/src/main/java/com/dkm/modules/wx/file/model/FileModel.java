
package com.dkm.modules.wx.file.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @ClassName File
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-05 23:09
 * @Version V1.0
 **/

@Getter
@Setter
@TableName("c_file")
public class FileModel {



    /**
     * 文件ID（UUID）
     * 
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 文件名
     * 
     */
    private String fileName;
    /**
     * 文件真实名称
     * 
     */
    private String fileRealName;
    /**
     * 业余区分，也做二级路径
     * 
     */
    private String business;
    /**
     * 上传时间
     * 
     */
    private Date createTime;

    private String fileUrl;
    /**
     * 上传人
     * 
     */
    private String createBy;
}
