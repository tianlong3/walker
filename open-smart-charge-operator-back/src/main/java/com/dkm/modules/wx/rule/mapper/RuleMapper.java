        
package com.dkm.modules.wx.rule.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.modules.wx.pile.model.ChargingPile;
import com.dkm.modules.wx.rule.model.CustomPrice;
import com.dkm.modules.wx.rule.model.Rule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


/**
 * @ClassName RuleMapper
 * @Description: 
 * @Author yangbin
 * @Date 2019-09-20 15:29
 * @Version V1.0
 **/
@Repository
public interface RuleMapper  extends BaseMapper<Rule> {

    Rule getRule(Rule rule);

    List<Rule> getRuleListByUserNm(@org.apache.ibatis.annotations.Param("p")Map<String,Object> params, Page<Rule> page);

    int getRoleUseContById(@Param("id")int id);

    List<Map<String,Object>> getRulePriceListById(int id);

    CustomPrice getCustomPrice(Map<String, Object> parasms);

    /**
      * 功能描述：查询默认规则
      * @param chargingPile 用户id
      * @Return:java.util.List<com.dkm.modules.wx.rule.model.Rule> 默认规则
      * @Author: Guo Liangbo
      * @Date:2019/11/11 23:14
      */
    List<Rule> selectDefaultRuleList(ChargingPile chargingPile);

}

