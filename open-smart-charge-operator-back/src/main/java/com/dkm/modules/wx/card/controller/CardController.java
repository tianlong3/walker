
package com.dkm.modules.wx.card.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dkm.commons.BaseController;
import com.dkm.commons.PageQueryParams;
import com.dkm.commons.Response;
import com.dkm.modules.wx.card.model.Card;
import com.dkm.modules.wx.card.model.RechargeRecods;
import com.dkm.modules.wx.card.model.UseRecods;
import com.dkm.modules.wx.card.service.CardService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName CardController
 * @Description: IC卡控制器
 * @Author yangbin
 * @Date 2019-09-13 12:50
 * @Version V1.0
 **/
@Api(tags = "6 卡管理")
@RestController
@RequestMapping("/card")
public class CardController extends BaseController {

    private final static Logger logger = LoggerFactory.getLogger(CardController.class);
    @Autowired
    private CardService cardService;

    @ApiOperation("新增卡片")
    @ApiResponses({
            @ApiResponse(code = 0, message = "校验成功"),
            @ApiResponse(code = 20001, message = "校验失败"),
            @ApiResponse(code = 40001, message = "参数不合法"),
            @ApiResponse(code = 50001, message = "系统异常"),})
    @PostMapping("/addCard")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response addCard(@Valid Card card) {
        logger.info("新增卡片,编号：" + card.getCardNo());
        /*设置卡所属经销商*/
        card.setOperator(getUserId().toString());
        /*设置卡片的状态为未启用*/
        card.setState("6003");
        return cardService.addCard(card);
    }

    @ApiOperation("挂失卡")
    @GetMapping("/lossCard")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response lossCard(@ApiParam("卡号") @RequestParam("cardNo") String cardNo) {
        logger.info("挂失卡,卡号：" + cardNo);
        return cardService.lossCard(cardNo);
    }

    @ApiOperation(value = "获取卡列表", notes = "状态见 公共代码 parentCode =6 ,6001：正常，6002：挂失，6003：未启用")
    @GetMapping("/getCardList")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<IPage<Card>> getCardList(@ApiParam("卡号") @RequestParam(value = "cardNo", required = false) String cardNo,
                                             @ApiParam("卡状态") @RequestParam(value = "state", required = false) String state,
                                             PageQueryParams pageQueryParams) {
        Page<Card> page = new Page<>(pageQueryParams.getPageNo(), pageQueryParams.getPageSize());
        /*查询卡列表*/
        Map<String, Object> params = new HashMap<>();
        params.put("userId", getUserId());
        params.put("cardNo", cardNo);
        params.put("state", state);
        IPage<Card> cardList = cardService.getCardList(params, page);
        return Response.ok(cardList);
    }

    @ApiOperation("编辑卡")
    @PostMapping("/editCard")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response editCard(Card card) {
        logger.info("编辑卡，卡号：" + card.getCardNo());
        /*设置更新时间*/
        card.setUpdateTime(new Date());
        cardService.updateCard(card);
        return Response.ok();
    }


    @ApiOperation("卡使用记录")
    @GetMapping("/getUseRecods")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response<UseRecods> getUseRecods(@ApiParam("卡号") @RequestParam("cardNo") String cardNo,
                                            @ApiParam("页数") @RequestParam("pageNo") int pageNo,
                                            @ApiParam("每页显示数") @RequestParam("pageSize") int pageSize) {
        logger.info("获取卡使用记录");
        Page<UseRecods> page = new Page<>(pageNo, pageSize);
        IPage<UseRecods> Recods = cardService.getUseRecods(page, cardNo);
        return new Response(Recods);
    }

    @ApiOperation("卡充值记录")
    @GetMapping("/getRechargeRecods")
    @ApiImplicitParam(paramType = "header", name = "token", value = "身份认证Token")
    public Response geRechargeRecods(@ApiParam("卡号") @RequestParam("cardNo") String cardNo,
                                     @ApiParam("页数") @RequestParam("pageNo") int pageNo,
                                     @ApiParam("每页显示数") @RequestParam("pageSize") int pageSize) {
        logger.info("获取卡充值记录");
        Page<RechargeRecods> page = new Page<>(pageNo, pageSize);
        Map<String, Object> params = new HashMap<>();
        params.put("cardNo", cardNo);
        IPage<RechargeRecods> Recods = cardService.geRechargeRecods(params, page);
        return Response.ok(Recods);
    }

}
