package com.dkm.util;

import com.alibaba.fastjson.JSON;
import com.dkm.util.http.HttpClientUtil;
import lombok.Data;

public abstract class BaiDuGeoCodingUtil {

    /**
     * 百度地图 地理编码服务
     * http://lbsyun.baidu.com/index.php?title=webapi/guide/webservice-geocoding
     */
    private static final String URL = "http://api.map.baidu.com/geocoding/v3/";
    private static final String AK = "kHuT1n634XyhUTcOU3oPa2ovhKbOK5NN";

    public static Location getLocation(String address) {
        Location location = null;
        String response = HttpClientUtil.doGet(URL + "?address=" + address + "&output=json&ak=" + AK);
        if (StringUtils.isNotEmpty(response)) {
            BaiduGeoResponse resp = JSON.parseObject(response, BaiduGeoResponse.class);
            if (resp.isSuccess()) {
                location = resp.getResult().getLocation();
            }
        }
        return location;
    }

    @Data
    public static class BaiduGeoResponse {

        private String status;
        private Result result;

        private boolean isSuccess() {
            return StringUtils.equals(status, "0");
        }
    }

    @Data
    public static class Result {
        private Location location;
    }

    @Data
    public static class Location {

        private String lat;
        private String lng;
    }
}
