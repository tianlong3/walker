package com.dkm.util.excel;

import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;

/**
 * @描述 excel导出工具类
 * @创建时间 2019/10/24 21:33
 * @创建人 yangbin
 */
@Component
public class ExportExcelUtil {

    private static  String excelPath;

    @Value("${excel-file-path}")
    public void setExcelPath(String excelPath){
        ExportExcelUtil.excelPath =excelPath;
    }

    public static  void  exportExcel(String templateFileName, String destFileName, Map<String, Object> data, HttpServletResponse res) {

        XLSTransformer transformer = new XLSTransformer();
        InputStream in=null;
        OutputStream out=null;
        File newFile =null;
        try {
            newFile = new File(excelPath+destFileName);
            /*如果文件夹不存在 则创建*/
            if(!newFile.getParentFile().exists()){
                newFile.getParentFile().mkdirs();
            }
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream("excleTemplate/"+templateFileName);
            Workbook workbook=transformer.transformXLS(in, data);
            out=new FileOutputStream(newFile);
            //将内容写入输出流并把缓存的内容全部发出去
            workbook.write(out);
            out.flush();
            DownloadFile(newFile,res);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in!=null){try {in.close();} catch (IOException e) {}}
            if (out!=null){try {out.close();} catch (IOException e) {}}
        }

    }

    public static void DownloadFile(File newFile, HttpServletResponse res) throws IOException {

        ServletOutputStream out = null;
        res.setContentType("multipart/form-data");
        res.setCharacterEncoding("UTF-8");
        res.setContentType("text/html");

        res.setHeader("Content-Disposition", "attachment;fileName=" + newFile.getName());
        FileInputStream inputStream = null;

            inputStream = new FileInputStream(newFile);

            out = res.getOutputStream();
            int b = 0;
            byte[] buffer = new byte[1024];
            while ((b = inputStream.read(buffer)) != -1) {
                // 4.写到输出流(out)中
                out.write(buffer, 0, b);
            }
            inputStream.close();
            if (out!=null){
                out.close();
                newFile.delete();
            }


    }

    public static InputStream getResourcesFileInputStream(String fileName) {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
    }
}
