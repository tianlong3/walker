package com.dkm.util.excel;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class ExcelData {

    private long row;
    private List<String> header;
    private Map<String, List<CellData>> data;
}
