package com.dkm.util.random;

import java.util.Random;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * @Description 随机唯一码
 *
 * @author lizi
 * @date 2019/8/20 上午11:19
 * @Version 1.0
 */
public class RandomPrimaryCode {
	/**
	 * 获取随机唯一码
	 * @param length
	 * @return
	 * @author  wangshoufa 
     * @date 2018年11月29日 下午2:19:26
	 */
	public static String getPrimaryCode(int length) {
		String str = "";
		Random random = new Random();
		for (int i = 0; i < length - 1; i++) {
			String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
			// 输出数字or字母
			if ("char".equals(charOrNum)) {
				// 输出是大写字母还是小写字母
				int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
				str += (char) (random.nextInt(26) + temp);
			} else if ("num".equals(charOrNum)) {
				str += String.valueOf(random.nextInt(10));
			}
		}
		return isNewCode(str, length);
	}

	/**
	 * 
	 * 最后一位校验码确保唯一
	 * @param str
	 * @return
	 * @author wangshoufa 
	 * @date 2018年11月29日
	 *
	 */
	public static String isNewCode(String str, int length) {
		int a[] = {3, 9, 29, 5, 10, 8, 4, 18, 95, 66, 1, 34, 67, 89, 15, 51, 100, 72, 60, 12, 99, 81};
		AtomicIntegerArray arrEXP = new AtomicIntegerArray(a);// 加权因子
		char[] arrValid = { '1', '0', '9', '8', '7', '6', '5', '4', '3', '2', 'a', 'b', 'c', 'y', 'z', 'd', 'e', 'f',
				'g', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'X', 'r', 'h', 'i', 'j', 's', 't', 'u', 'v', 'w', 'x' };// 校验码
		int sum = 0;
		int idx = 0;
		for (int i = 0; i < length - 1; i++) {
			if (((int) (str.charAt(i)) >= 97 && (int) (str.charAt(i)) <= 122)) {
				sum += (((int) (str.charAt(i))) - 96) * arrEXP.get(i);
			} else if (((int) (str.charAt(i)) >= 65 && (int) (str.charAt(i)) <= 90)) {
				sum += (((int) (str.charAt(i))) - 64) * arrEXP.get(i);
			} else {
				sum += (int) (str.charAt(i)) * arrEXP.get(i);
			}
		}
		idx = sum % 37;
		char checkNum = arrValid[idx];
		return str + checkNum;
	}
}
