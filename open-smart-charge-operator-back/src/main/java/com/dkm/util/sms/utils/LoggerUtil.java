package com.dkm.util.sms.utils;

import java.util.Date;

import com.dkm.util.sms.CCPRestSDK;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerUtil {
	private static boolean isLog = true;
	private static Logger logger;
	static {
		if (logger == null) {
			logger = LoggerFactory.getLogger(CCPRestSDK.class);
			
		}
	}
	 
	public static void setLogger(boolean isLog) {
		LoggerUtil.isLog = isLog;
	}
	public static void setLog(Logger logger) {
		LoggerUtil.logger = logger;
	}


	public static void debug(Object msg) {
		if (isLog)
			logger.debug(new Date()+" "+msg);
	}

	public static void info(Object msg) {
		if (isLog)
			logger.info(new Date()+" "+msg);
	}

	public static void warn(Object msg) {
		if (isLog)
			logger.warn(String.valueOf(msg));
	}

	public static void error(Object msg) {
		if (isLog)
			logger.error(String.valueOf(msg));
	}

	  
}
