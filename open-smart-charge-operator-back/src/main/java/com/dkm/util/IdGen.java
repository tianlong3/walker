package com.dkm.util;

import java.util.UUID;

/**
 * @Description 封装各种生成唯一ID算法的工具类
 *
 * @author lizi
 * @date 2019/8/20 上午11:19
 * @Version 1.0
 */
public class IdGen {

    /**
     * 封装JDK自带的UUID, 通过Random数字生成, 中间无-分割.
     */
    public static String uuid() {
        return "_" + UUID.randomUUID().toString().replaceAll("-", "");
    }

}
