package com.dkm.constant;

/**
 * @author lizi
 * @Description 通用错误码
 * @date 2019/8/20 上午11:19
 * @Version 1.0
 */
public enum CommonErrorEnum {
    PARAM_ERROR(100, "param error"), // 请求参数错误
    TOKEN_ERROR(101, "token error"),   // Token无效
    DATA_GET_ERROR(102, "get data failed"),   // 获取接口数据失败
    UNKNOWN_ERROR(103, "认证失败"),   // 未知异常
    NO_AUTHORITY(104, "no authority"), // 无权限

    LOGIN_TIMEOUT(201, "登录超时，请重新登录"),
    LOGIN_FAILED(202, "登录失败，请重新登录"),
    LOGOUT_FAILED(203, "退出失败"),
    UPLOAD_FAILED(204, "上传失败"),
    FILE_NOT_FIND(204, "文件不存在"),;

    private Integer code;

    private String message;

    CommonErrorEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
