package com.dkm.constant;


/**
 * @author lizi
 * @Description 常量类
 * @date 2019/8/20 上午11:19
 * @Version 1.0
 */
public class Constant {

    /**
     * 接口访问成功code码
     */
    public final static Integer SUCCESS_CODE = 200;

    // 默认页数
    public final static Integer DEFAULT_PAGE_NUMBER = 1;
    // 默认每页显示条数
    public final static Integer DEFAULT_PAGE_SIZE = 10;

    /**
     * 用户session信息KEY
     */
    public final static String USER_SESSION_KEY = "userInfo";

    /**
     * 用户信息session信息KEY
     */
    public final static String USERINFO_SESSION_KEY = "user";


    // 默认编码
    public final static String DEFAULT_CHARSET = "UTF-8";

    /**
     * 字符串"."常量定义
     */
    public final static String CUT_PERIOD = ".";

    /**
     * 字符串"."常量定义
     */
    public final static String CUT_SLASH = "/";

    /**
     * 方法名是否包含"login"字符串
     */
    public final static String METHOD_NAME_STARTSWITH_LOGIN = "login";

    /**
     * 用户Token
     */
    public final static String USER_TOKEN = "token";

    public final static String AUTH_HEADER = "Authorization";

    /**
     * token Secret
     */
    public final static String SECRET = "cdzSecret";

    /**
     * token过期时长  2小时
     */
    public final static Long EXPIRATION = 7200L;

    /**
     * 200  访问成功
     * 202  已经接受请求，但未处理完成
     * 500  服务器出错
     * 404  链接无效
     * 400  客户端请求的语法错误，服务器无法理解
     * 401  请求要求用户的身份认证
     * 409  服务器完成客户端的PUT请求是可能返回此代码，服务器处理请求时发生了冲突
     */
    public final static int RESPONSE_STATUS_SUCCESS = 200;
    public final static int RESPONSE_STATUS_ACCEPTED = 202;
    public final static int RESPONSE_STATUS_SERVER_ERROR = 500;
    public final static int RESPONSE_STATUS_INVALID_LINK = 404;
    public final static int RESPONSE_STATUS_BAD_REQUEST_ERROR = 400;
    public final static int RESPONSE_STATUS_UNAUTHORIZED_ERROR = 401;
    public final static int RESPONSE_STATUS_CONFLICT_ERROR = 409;

    /**
     * 系统用戶 状态 1:启用 0:禁用 2:删除
     */
    public final static String XT_USER_NORMAL = "1";
    public final static String XT_USER_FORBIDEN = "0";
    public final static String XT_USER_DELETE = "2";

    /**
     * 用户数据权限
     */
    public final static Integer DATA_PERMISSIONS_ALL = 1;
    public final static Integer DATA_PERMISSIONS_SELF = 2;
    public final static Integer DATA_PERMISSIONS_CUSTOM = 3;

    /**
     * 代理商登录
     */
    public final static Integer LOGIN_TYPE_OPERATOR = 1;
    /**
     * 运营端登录
     */
    public final static Integer LOGIN_TYPE_NOT_OPERATOR = 0;

    /**
     * 用户是代理商
     */
    public final static Integer USER_IS_OPERATOR = 1;
    /**
     * 用户是运营人员
     */
    public final static Integer USER_IS_NOT_OPERATOR = 0;


    /**
     * 用户默认密码
     */
    public final static String USER_DEFAULT_PASSWORD = "admin123456";


    /**
     * 微信错误码 字段
     */
    public static final String WECHAT_ERR_CODE = "errcode";

    /**
     * 计费类型 3001：按小时计费 3002：按度数计费
     */
    public static final String CHARGE_TYPE_HOURS = "3001";
    public static final String CHARGE_TYPE_KWH = "3002";

    /**
     * 默认规则
     */
    public static final Integer RULE_DEFAULT = 1;

    /**
     * 规则类型 卡类型10001  公众号类型 10002
     */
    public final static String RULE_TYPE_CARD = "10001";
    public final static String RULE_TYPE_WX = "10002";

    /**
     * 指令 1 开 0 关
     */
    public static final Integer SWITCH_CLOSE = 0;
    public static final Integer SWITCH_OPEN = 1;

    // 卡片状态 正常
    public static final String CARD_STATE_NORMAL = "6001";
    // 卡片状态 挂失
    public static final String CARD_STATE_LOSE = "6002";
    // 卡片状态 未开启
    public static final String CARD_STATE_NOT_OPEN = "6003";

    // 充值记录类型 收支类型 0 充值
    public static final Integer RECHARGE_RECOD_TYPE_RECHARGE = 0;
    // 充值记录类型1 消费（支出）
    public static final Integer RECHARGE_RECOD_TYPE_CONSUME_ = 1;
    // 充值记录类型2 赠送
    public static final Integer RECHARGE_RECOD_TYPE_GIVE = 2;
    //  充值记录类型3 系统充值
    public static final Integer RECHARGE_RECOD_TYPE_SYSTEM = 3;
    /**
     * 充值记录类型4 退款
     */
    public static final Integer RECHARGE_RECOD_TYPE_REFUND = 4;
    /**
     * 充值记录类型5 退款失败
     */
    public static final Integer RECHARGE_RECOD_TYPE_REFUND_ERROR = 5;

    /**
     * 充值记录类型 转账
     */
    public static final Integer RECHARGE_RECOD_TYPE_TRANSFER_ACCOUNTS = 6;

    /**
     * 充值记录类型 订单退款  此退款，不实际退款
     */
    public static final Integer RECHARGE_RECOD_TYPE_ORDER_REFUND = 7;

    /**
     * 订单待支付
     */
    public static final String ORDER_STATUS_NON_PAY = "1";

    /**
     * 订单已支付
     */
    public static final String ORDER_STATUS_HAS_PAYED = "3";
    /**
     * 订单已退款
     */
    public static final String ORDER_STATUS_HAS_REFUNDED = "4";

    /**
     * 是否收费,2001 收费（默认），2002不收费  取决于充电桩
     */
    public static final String ORDER_IS_FEE = "2001";
    public static final String ORDER_IS_FREE = "2002";

    // 充值记录数据类型 数据类型，1 卡  2 账户
    public static final Integer RECHARGE_RECOD_DATA_TYPE_CARD = 1;
    // 充值记录数据类型
    public static final Integer RECHARGE_RECOD_DATA_TYPE_ACCOUNT = 2;

    // 充电桩收费类型 收费
    public static final String CHARGINGPILE_CPFEETYPE_YES = "2001";
    // 充电桩收费类型 免费
    public static final String CHARGINGPILE_CPFEETYPE_NO = "2002";


    // 充电桩端口 状态
    public static final String PORT_STATE_N = "N";
    public static final String PORT_STATE_Y = "Y";
    public static final String PORT_STATE_F = "F";
    /**
     * 菜单树,顶层节点
     */
    public static final Integer MENT_CODE_TOP = 0;
    /**
     * 菜单类型: 菜单
     */
    public static final Integer MENU_TYPE_MENU = 0;

    /**
     * 菜单类型: 按钮
     */
    public static final Integer MENU_TYPE_BUTTON = 1;
}

