package com.dkm.constant;

/**
 * @Description 文件中心错误码
 *
 * @author lizi
 * @date 2019/8/20 上午11:19
 * @Version 1.0
 */
public enum UserErrorEnum {
    USER_NAME_EXIST(10000, "username already exist"),   // 用户名已经存在
    USER_NAME_NOT_EXIST(10001, "username not exist"),   // 用户名不存在
    EMAIL_ERROR(10100,"邮箱格式错误"),
    PASSWORD_LENGTH_ERROR(10101,"密码长度不对"),
    PASSWORD_ERROR(10102, "密码错误"),
    NOT_SING_IN(-6,"用户未登录或身份异常"),
    SIGN_IN_INPUT_FAIL(-4,"账号或密码错误"),
    DAILI_CANNOT_LOGIN(-5,"代理商不能登录运营端！"),
    USERID_IS_NULL(-11,"用户userid为空");


    private Integer code;

    private String message;

    UserErrorEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
