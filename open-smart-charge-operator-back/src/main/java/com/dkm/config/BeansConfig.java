package com.dkm.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.annotation.Resource;

/**
 * @Description 通用Bean初始化
 *
 * @author lizi
 * @date 2019/8/20 上午11:19
 * @Version 1.0
 */
@Configuration
public class BeansConfig {
	
	@Resource
	private Environment env;
	
	/**
	 * restful服务对象restTemplate创建
	 */
	@Bean
	public RestTemplate restTemplate() {
		SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
		requestFactory.setReadTimeout(15000);
		requestFactory.setConnectTimeout(3000);
		return new RestTemplate(requestFactory);
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver getCommonsMultipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(20971520);
		multipartResolver.setMaxInMemorySize(1048576);
		return multipartResolver;
	}

	@Bean
	public FilterRegistrationBean GlobalParamHandlerFilterBean() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setFilter(new GlobalParamHandlerFilter());
		filterRegistrationBean.addUrlPatterns("/*");
		filterRegistrationBean.addInitParameter("paramName", "paramValue");
		filterRegistrationBean.setName("GlobalParamHandlerFilter");
		filterRegistrationBean.setOrder(3);
		return filterRegistrationBean;
	}

}
