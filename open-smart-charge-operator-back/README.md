### 平台说明
1. 运营端后台接口代码，负责业务维护管理

### 架构说明
1. 项目架构spring-boot，版本：2.1.7.RELEASE


### 项目运行步骤
1. 本地创建数据库：charging_pile
2. 导入数据库文件：doc/smart-charge.sql
4. 本地修改application-dev.properties中的数据库信息为本地数据库
5. redis目前链接请自行安装redis
6. 本地测试，请注释：SecurityAuthInterceptor
7. demo请参照com.dkm.user包，里面包括了完整的用户实体增删改查以及分页，并且已经测试通过
8. 执行Apiv2Application中的main方法直接启动
9. （请自行填写）位置处，填写对应账号信息


### 工程配置文件
1. 开发环境配置文件：application-dev.properties
2. 测试环境配置文件：application-test.properties
3. 生产环境配置文件：application-prod.properties

### 工程结构说明
1. commons：公共包
2. config：项目配置类
3. constant：常量包
4. exception：全局异常控制
5. interceptor：权限拦截器
6. util：工具包
7. 具体业务包，以业务名命名
- 业务包结构如下：controller、mapper、model、service、service.impl