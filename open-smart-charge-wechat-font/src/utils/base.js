/**
 * 接口域名的管理
 */
// 正式配置
/*const base = {

  //sq:'/api',//请求代理需要用到
    sq:'http://wechat.nxptdn.com',
    bd: 'http://operator.nxptdn.com',
    client:'http://e.nxptdn.com',
    appid:'wx9e5e67da26c51a14'
}*/
// 测试配置
const base = {
  // sq:'/api',//请求代理需要用到
  sq:'http://test-wechat.nxptdn.com',
  bd: 'http://test-operator.nxptdn.com',
  client:'http://test.nxptdn.com',
  appid:'wx2d27d24de3fd5b9f'
}
// const base = {
//     sq: '/api',
//     // sq:'http://202.85.220.170:8866/electric-website',   http://test-wechat.nxptdn.com/
//     bd: '/api'
// }


export default base;


