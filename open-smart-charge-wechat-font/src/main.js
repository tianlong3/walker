import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'
import MuseUI from 'muse-ui'
import 'muse-ui/dist/muse-ui.css'
import './assets/font/material-icons.css'
import api from './api/api'
import 'lib-flexible/flexible.js'
import 'vue2-animate/dist/vue2-animate.min.css'
// 在man.js中引入 weixin-jsapi
import wx from "weixin-jsapi";
import Message from 'muse-ui-message'
import './assets/font/iconfont/iconfont.css'
import './assets/font/iconfont/cdziconfont.js'
import { Toast,Dialog} from "vant";
import "vant/lib/toast/style/index";
import Userinfo from './components/global/userinfo';

import Adminuserinfo from './components/global/adminuseinfo';
import axios from 'axios'

import ECharts from 'vue-echarts'
import 'echarts/lib/chart/line'
import 'echarts/lib/component/tooltip'

import Router from 'vue-router'

// import Vconsole from 'vconsole'
// let vConsole=new Vconsole()
// export default vConsole

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}


axios.defaults.withCredentials=true;
Vue.use(Toast);
Vue.use(Dialog);
Toast.setDefaultOptions({ duration: 600 });
Vue.use(Userinfo);
Vue.component('userinfo', Userinfo);
Vue.use(Adminuserinfo);
Vue.component('adminuserinfo', Adminuserinfo);
// 在man.js中注册 weixin-jsapi
Vue.prototype.$wx = wx
Vue.prototype.$api = api // 将api挂载到vue的原型上

Vue.use(Message)
Vue.use(MuseUI)

Vue.component('chart', ECharts)

Vue.config.productionTip = false


new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
